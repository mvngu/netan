###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile
#
# The H value of each object.  The object can be a node, a small cluster of
# nodes, a community, or a whole network.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
infname <- args[1]

# prepare files for reading and writing
incon <- file(infname, open="r")
line <- readLines(incon, n=1)  # first line is header
L <- unlist(strsplit(line, split=":"))
outfname <- paste(infname, ".h", sep="")
outcon <- file(outfname, open="w")
writeLines(paste(L[1], ":H value:", L[2], sep=""), outcon)
# get history of each object and compute its H value
while (length(line <- readLines(incon, n=1, warn=FALSE)) > 0) {
  L <- unlist(strsplit(line, split=":"))
  history <- lapply(unlist(strsplit(L[2], split=",")), as.numeric)
  history <- data.frame(history)
  S <- sum(history)
  # an object without any changes at all has H = 0
  if (S == 0) {
    writeLines(paste(L[1], ":", 0, ":", L[2], sep=""), outcon)
    next
  }
  history <- history / S
  H <- history * log(history)
  H[is.na(H)] <- 0
  H <- -1 * sum(H)
  writeLines(paste(L[1], ":", H, ":", L[2], sep=""), outcon)
}
close(outcon)
close(incon)
