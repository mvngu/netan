###########################################################################
# Copyright (c) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Distribution and sequence of community size.

import argparse
import os

###################################
# helper functions
###################################

def size_distribution(fname):
    """
    Construct a community size distribution from the list of communities
    in the given file.  This distribution is analogous to the degree
    distribution.  That is, for each community size from 1 up to and including
    the maximum size, we count the number of occurrences of this size in
    the provided file.

    INPUT:

    - fname -- name of the file to read from.  This file should contain lists
      of communities, one on each line.  Each line represents one community
      and all the node IDs belonging to a community are listed on one line.
    """
    f = open(fname, "r")
    f.readline()  # ignore the first line, which counts # communities
    # construct the size dictionary where key/value is size/count
    maxsize = 0
    Size = {}
    for line in f:
        C = line.strip().split()
        n = len(C)
        if n not in Size:
            Size.setdefault(n, 1)
        else:
            Size[n] += 1
        if maxsize < n:
            maxsize = n
    f.close()
    # Construct size distribution.  Index is size, value is count.  As lists
    # are indexed from zero, we also include the count for size zero.  The
    # maximum community size is len(S) - 1.
    S = []
    for i in range(maxsize + 1):
        if i in Size:
            S.append(Size[i])
        else:
            S.append(0)
    return S

def size_sequence(fname):
    """
    Construct a community size sequence from the list of communities in the
    given file.  This sequence is analogous to the degree sequence.  That is,
    for each community we compute its size.  The list of sizes for all
    communities is called the size sequence.

    INPUT:

    - fname -- name of the file to read from.  This file should contain lists
      of communities, one on each line.  Each line represents one community
      and all the node IDs belonging to a community are listed on one line.
    """
    f = open(fname, "r")
    f.readline()  # ignore the first line, which counts # communities
    # construct the size sequence
    S = []
    for line in f:
        C = line.strip().split()
        n = len(C)
        S.append(n)
    f.close()
    return sorted(S)

def write_distribution(S, fname, header):
    """
    Write the given community size distribution to file.  The resulting file
    is in CSV format.

    INPUT:

    - S -- a community size distribution implemented as a list, where
      index/value is size/count.  As lists are indexed from zero, S also
      includes the count for size zero.  The maximum community size is
      len(S) - 1.
    - fname -- name of the file to which we write the size distribution.
    - header -- the header of the resulting CSV file.
    """
    f = open(fname, "w")
    f.write(header + "\n")
    for size in range(1, len(S)):
        f.write(str(size) + "," + str(S[size]) + "\n")
    f.close()

def write_sequence(S, fname, header):
    """
    Write the given community size sequence to file.

    INPUT:

    - S -- a community size sequence implemented as a list, where index/value
      is size/count.  Thus community i has size S[i].
    - fname -- name of the file to which we write the size sequence.
    - header -- the header of the output file.
    """
    f = open(fname, "w")
    f.write(header + "\n")
    for size in S:
        f.write(str(size) + "\n")
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Distribution and sequence of community size.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--comfile", metavar="path", required=True,
                    help="path to file with community structures")
parser.add_argument("--distfile", metavar="path", required=True,
                    help="file for writing community size distribution")
parser.add_argument("--seqfile", metavar="path", required=True,
                    help="file for writing community size sequence")
args = parser.parse_args()

# get the command line arguments
comfile = args.comfile
distfile = args.distfile
seqfile = args.seqfile

# sanity checks
if not os.path.isfile(comfile):
    raise ValueError("File not found: %s" % comfile)

# get size distributions and write to file
Dist = size_distribution(comfile)
write_distribution(Dist, distfile, "exact size,count")
# get the size sequence and write to file
Seq = size_sequence(comfile)
write_sequence(Seq, seqfile, "sequence of exact size")
