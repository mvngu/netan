###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script MUST be run from Python.
#
# Structure the history of all communities according to their lifespans.

import argparse
import os

###################################
# helper functions
###################################

def community_history(history, nodelist):
    """
    Read in the history of each community and map each node ID to the
    snapshot/community index.

    INPUT:

    - history -- file with the history of each community.
    - nodelist -- the mapping from snapshot/community index to node ID.
    """
    V = node_list(nodelist)
    C = list()  # history of each community
    f = open(history, "r")
    for line in f:
        D = map(lambda x: V[x], line.strip().split(" "))
        C.append(D)
    f.close()
    return C

def node_list(fname):
    """
    Get the mapping of node ID to snapshot/community index.  Essentially, we
    want a reverse mapping.

    INPUT:

    - fname -- file with a mapping of snapshot/community index to node ID.
    """
    V = dict()
    f = open(fname, "r")
    for line in f:
        dat = line.strip().split(",")
        sci = None
        vid = None
        if len(dat) == 2:
            sci, vid = dat
        elif len(dat) == 3:
            sci, d, vid = dat
            sci = ",".join([sci, d])
        else:
            raise ValueError("bad community data: %s" % line.strip())
        assert vid not in V
        V.setdefault(vid, sci)
    f.close()
    return V

def write_history(D, dirname, n):
    """
    Write to files the history of each community, structured according to
    lifespan.  We are only interested in the full trace of each community,
    i.e. the trace from birth to death, or up to the latest snapshot index.
    The full trace of each community is written on one line.

    INPUT:

    - D -- list of full traces of communities.  Each full trace is given as a
      list.
    - dirname -- path to directory with match data.
    - n -- the maximum lifespan of any community.
    """
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    for i in range(1, n + 1):
        Di = filter(lambda x: len(x) == i, D)  # all traces of a given length
        f = open(os.path.join(dirname, "trace-%d.dat" % i), "w")
        f.write("%d\n" % len(Di))  # number of traces with given length
        for trace in sorted(Di):
            s = "->".join(trace)
            f.write(s + "\n")
        f.flush()
        os.fsync(f.fileno())
        f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Structure the history of all communities according to their lifespans.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--history", metavar="file", required=True,
                    help="the history of each community")
parser.add_argument("--maxell", metavar="int", required=True, type=int,
                    help="maximum lifespan of any community")
parser.add_argument("--tracedir", metavar="path", required=True,
                    help="write the ell-structured history under here")
parser.add_argument("--nodelist", metavar="file", required=True,
                    help="mapping of snapshot/community index to node ID")
args = parser.parse_args()
history = args.history
maxell = int(args.maxell)
tracedir = args.tracedir
nodelist = args.nodelist

assert maxell > 0
C = community_history(history, nodelist)
write_history(C, tracedir, maxell)
