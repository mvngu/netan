###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Rename the nodes in the trace of a dynamic community.  The whole idea is
# to represent an edge list in such a way that igraph can process.

import argparse
import os

###################################
# helper functions
###################################

def read_traces(fname, V, E):
    """
    Get the traces of dynamic communities.

    WARNING: This function modifies its arguments.

    INPUT:

    - fname -- file with traces of dynamic communities.
    - V -- node set.  This is a mapping from nodes in a trace to nonnegative
      integers.  We update the mapping as we go along.
    - E -- set of directed edges.  We update this as we go along.
    """
    f = open(fname, "r")
    f.readline()  # ignore first line, which is no. traces
    for line in f:
        trace = line.strip().split("->")
        if len(trace) == 1:
            u = trace[0]
            if u not in V:
                V.setdefault(u, len(V))
            # Add a self-loop.  This is so we would be able to recover
            # isolated nodes.  Ideally, the edge set E should represent a
            # graph without self-loops not multiple edges.
            E.add((V[u], V[u]))
            continue
        assert len(trace) > 1
        for i in range(len(trace) - 1):
            u = trace[i]
            v = trace[i + 1]
            if u not in V:
                V.setdefault(u, len(V))
            if v not in V:
                V.setdefault(v, len(V))
            E.add((V[u], V[v]))
    f.close()

def rename_nodes(tracedir, ntrace, fout):
    """
    Rename all nodes in traces of dynamic communities.  Write the resulting
    edge list to file.

    INPUT:

    - tracedir -- read the trace files from this directory.
    - ntrace -- how many trace files.  Trace files are numbered from 1.
    - fout -- write results to this file.
    """
    V = dict()  # mapping from nodes to nonnegative integers
    E = set()   # set of directed edges
    for i in range(1, ntrace + 1):
        fname = os.path.join(tracedir, "trace-%d.dat" % i)
        read_traces(fname, V, E)
    f = open(fout, "w")
    for u, v in sorted(list(E)):
        f.write("%d %d\n" % (u, v))
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Rename the nodes in the trace of a dynamic community.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--tracedir", metavar="path", required=True,
                    help="read the trace files from this directory")
parser.add_argument("--ntrace", metavar="integer", required=True, type=int,
                    help="how many trace files, numbered from 1")
parser.add_argument("--output", metavar="path", required=True,
                    help="write the edge list to this file")
args = parser.parse_args()

# get the command line arguments and sanity checks
tracedir = args.tracedir
ntrace = args.ntrace
output = args.output
if ntrace < 1:
    raise ValueError("must provide at least one trace file")

# read in edge list and rename each unique node
rename_nodes(tracedir, ntrace, output)
