###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile outfile

# Normalize a distribution of counts.  We take the cumulative distribution.
# The normal use case is when we have a distribution of counts, say, for
# community size or node degree.  In that case, the input file should follow
# CSV format and must contain a header.  The CSV format is "quant,count",
# where "quant" refers to the quantity of interest and "count" is the number
# of occurrences of that quantity.  In the case of community size
# distribution, the CSV format would be "size,count", where "size" is the
# community size and "count" records the number of communities with that
# particular size.  Having read in the distribution, we then normalize each
# count by the total count and produce a normalized cumulative distribution.

args <- commandArgs(trailingOnly=TRUE)
infile <- args[1]   # file to read from
outfile <- args[2]  # file to which we write

dist <- read.csv(file=infile, header=TRUE, sep=",")
colnames(dist) <- c("size", "count")
N <- sum(dist$count)  # normalization constant
cum <- c()
for (i in 1:length(dist$count)) {
  s <- sum(dist$count[i:length(dist$count)])  # cumulative sum
  cum <- c(cum, s)
}
cumdist <- data.frame(size=dist$size, count=cum / N)
write.csv(cumdist, file=outfile, quote=FALSE, row.names=FALSE)
