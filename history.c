/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* Get the history of all communities.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <igraph.h>
#include <stdio.h>
#include <stdlib.h>

int is_out_neighbour(const igraph_t *G,
                     const igraph_integer_t u,
                     const igraph_integer_t v);
void trace_community(const igraph_t *G,
                     igraph_vector_t *OUTD,
                     const igraph_integer_t s,
                     FILE *f);
void trace_all_communities(const igraph_t *G,
                           igraph_vector_t *IND,
                           igraph_vector_t *OUTD,
                           const char *output);

/* Is v an out-neighbour of u?  In other words, is u a parent of v?
 *
 * INPUT:
 *
 * - G -- a directed graph.
 * - u -- a node in G.
 * - v -- another node in G.
 */
int is_out_neighbour(const igraph_t *G,
                     const igraph_integer_t u,
                     const igraph_integer_t v) {
  int res;
  igraph_vector_t Adj;

  igraph_vector_init(&Adj, 0);
  igraph_neighbors(G, &Adj, u, IGRAPH_OUT);
  res = (int)igraph_vector_binsearch2(&Adj, v);
  igraph_vector_destroy(&Adj);
  return res;
}

/* Trace all communities from the given start node.
 *
 * INPUT:
 *
 * - G -- a digraph representation of community matches.
 * - OUTD -- the sequence of out-degrees of G.  We assume that OUTD[i] is the
 *   out-degree of node i.
 * - s -- the node from which to start tracing communities.  This must be a
 *   node in the given digraph.  We assume that this node has in-degree zero.
 *   We call this the source node.
 * - f -- write traces of all communities to this file.
 */
void trace_community(const igraph_t *G,
                     igraph_vector_t *OUTD,
                     const igraph_integer_t s,
                     FILE *f) {
  int i;
  int j;
  int k;
  int u;
  int v;
  int w;
  igraph_stack_t S;     /* index all those in D to be further explored */
  igraph_vector_t Adj;  /* the neighbours of a node */
  igraph_vector_t T;    /* the trace of a community */

  /* It is possible that s is isolated, i.e. it has one time step.  In this */
  /* case, its in- and out-degrees are both zero. */
  if (VECTOR(*OUTD)[s] == 0) {
    fprintf(f, "%d\n", (int)s);
    return;
  }

  /* From hereon, we assume that s has out-degree > 0.  Do a depth-first */
  /* search from s to get all communities that start from s.  We assume that */
  /* the given graph is directed and we follow edges that go out from s. */
  /* We only record nodes that can be reached from s. */
  igraph_vector_init(&T, 0);
  igraph_stack_init(&S, 0);
  igraph_stack_push(&S, (int)s);
  while (igraph_stack_size(&S) > 0) {
    u = (int)igraph_stack_pop(&S);
    igraph_vector_push_back(&T, u);
    /* record all successors that have exactly one out-neighbour */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    while (VECTOR(*OUTD)[v] == 1) {
      igraph_vector_init(&Adj, 0);
      igraph_neighbors(G, &Adj, v, IGRAPH_OUT);
      igraph_vector_push_back(&T, (int)VECTOR(Adj)[0]);
      i = (int)igraph_vector_size(&T) - 1;
      v = (int)VECTOR(T)[i];
      igraph_vector_destroy(&Adj);
    }
    /* If a node has > 1 out-neighbour, follow all of these out-neighbours. */
    /* Push each out-neighbour onto the stack.  If we don't follow an */
    /* out-neighbour, we won't be able to reach the end point of a */
    /* community. */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    if (VECTOR(*OUTD)[v] > 1) {
      igraph_vector_init(&Adj, 0);
      igraph_neighbors(G, &Adj, v, IGRAPH_OUT);
      for (j = 0; j < (int)igraph_vector_size(&Adj); j++) {
        igraph_stack_push(&S, (int)VECTOR(Adj)[j]);
      }
      igraph_vector_destroy(&Adj);
      continue;
    }
    /* We have reached the end point of a community.  Backtrack to the last */
    /* node in T that has as its out-neighbour the node at the top of the */
    /* stack S.  The purpose of the backtracking is to find the latest node */
    /* from which to trace out another community. */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    if (VECTOR(*OUTD)[v] == 0) {
      /* We have traced out a community.  Write the trace to file. */
      for (k = 0; k < (int)igraph_vector_size(&T) - 1; k++) {
        fprintf(f, "%d ", (int)VECTOR(T)[k]);
      }
      k = (int)igraph_vector_size(&T) - 1;
      fprintf(f, "%d\n", (int)VECTOR(T)[k]);
      /* now backtrack */
      if (igraph_stack_size(&S) > 0) {
        w = (int)igraph_stack_top(&S);
        i = (int)igraph_vector_size(&T) - 1;
        v = (int)VECTOR(T)[i];
        while (!is_out_neighbour(G, v, w)) {
          igraph_vector_pop_back(&T);
          i = (int)igraph_vector_size(&T) - 1;
          v = (int)VECTOR(T)[i];
        }
      }
    }
  }

  igraph_stack_destroy(&S);
  igraph_vector_destroy(&T);
}

/* Trace all dynamic communities using the given digraph.  Each community
 * is traced from its birth to its death, or possibly up to the latest
 * snapshot for which we have data.
 *
 * INPUT:
 *
 * - G -- a digraph representation of community matches.  We use this
 *   digraph representation to trace the evolution of each community.
 * - IND -- the sequence of in-degrees of G.  We assume that IND[i] is the
 *   in-degree of node i.
 * - OUTD -- the sequence of out-degrees of G.  We assume that OUTD[i] is the
 *   out-degree of node i.
 * - output -- write all traces of communities to this file.
 */
void trace_all_communities(const igraph_t *G,
                           igraph_vector_t *IND,
                           igraph_vector_t *OUTD,
                           const char *output){
  FILE *f;
  int i;

  f = fopen(output, "w");  /* overwite the file if it exists */
  fclose(f);
  f = fopen(output, "a");
  for (i = 0; i < (int)igraph_vector_size(IND); i++) {
    /* Get the source node of a community.  This node is the very first */
    /* node in the history of the community.  A source node is defined as */
    /* any node whose in-degree is zero.  Determine all communities starting */
    /* from the given source node. */
    if (VECTOR(*IND)[i] == 0) {
      trace_community(G, OUTD, i, f);
    }
  }
  fclose(f);
}

/* Start from here.  This mainly collects the command line arguments and pass
 * them to the relevant function.
 */
int main(int argc,
         char **argv) {
  FILE *f;
  char *fname;
  igraph_t G;            /* the sampled network; must be directed & simple */
  igraph_vector_t IND;   /* sequence of in-degree */
  igraph_vector_t OUTD;  /* sequence of out-degree */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_str *edgelist = arg_strn(NULL, "edgelist", "file", 0, argc + 2,
                                      "edge list of the trace network");
  struct arg_str *output = arg_strn(NULL, "output", "file", 0, argc + 2,
                               "write all traces of communities to this file");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, edgelist, output, end};
  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Traces of all communities.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((edgelist->count < 1)
      || (output->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Traces of all communities.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* Reconstruct the trace network from its edge list.  The graph is */
  /* a simple digraph, i.e. no self-loops nor multiple edges. */
  asprintf(&fname, "%s", edgelist->sval[0]);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, f, 0, IGRAPH_DIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  /* get all traces of all communities */
  igraph_vector_init(&OUTD, 0);
  igraph_degree(&G, &OUTD, igraph_vss_all(), IGRAPH_OUT, IGRAPH_NO_LOOPS);
  igraph_vector_init(&IND, 0);
  igraph_degree(&G, &IND, igraph_vss_all(), IGRAPH_IN, IGRAPH_NO_LOOPS);
  trace_all_communities(&G, &IND, &OUTD, output->sval[0]);

  igraph_vector_destroy(&IND);
  igraph_vector_destroy(&OUTD);
  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
