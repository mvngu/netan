/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* The line graph of a simple undirected graph.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <igraph.h>
#include <stdlib.h>
#include <string.h>

/* Construct the line graph of a simple undirected graph.  Write the line
 * graph to a file as an adjacency list according to the format:
 *
 * v1 v2,u1 u2,w1 w2,...
 *
 * Here, (v1, v2) is a node and the other nodes are its neighbours.
 */
int main(int argc,
         char **argv) {
  FILE *file;
  char *fname;
  igraph_t G;
  igraph_t lineG;
  igraph_bool_t simple;
  igraph_eit_t eit;
  igraph_vit_t vit;
  igraph_integer_t i;
  igraph_integer_t eid;
  igraph_integer_t vid;
  igraph_integer_t uG;  /* vertex in original graph */
  igraph_integer_t vG;  /* vertex in original graph */
  igraph_integer_t uL;  /* vertex in line graph */
  igraph_integer_t vL;  /* vertex in line graph */
  igraph_vector_t nbr;  /* neighbours of a node */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_file *felist = arg_file0(NULL, "felist", NULL,
                               "file containing edge list, one edge per line");
  struct arg_file *fedge = arg_file0(NULL, "fedge", NULL,
                                   "file for writing edge list of line graph");
  struct arg_file *fadj = arg_file0(NULL, "fadj", NULL,
                              "file for writing adjacency list of line graph");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, felist, fedge, fadj, end};

  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Line graph of a simple undirected graph.\n");
    arg_print_glossary(stdout, argtable, "  %-18s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((felist->count < 1)
      || (fedge->count < 1)
      || (fadj->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Line graph of a simple undirected graph.\n");
    arg_print_glossary(stdout, argtable, "  %-18s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* reconstruct graph from its edge list: simple undirected graph */
  asprintf(&fname, "%s", felist->filename[0]);
  file = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, file, /*nvert*/ 0, IGRAPH_UNDIRECTED);
  fclose(file);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);

  /* Get line graph and write adjacency list to file.  The resulting line */
  /* graph is assumed to be a simple graph. */
  igraph_linegraph(&G, &lineG);
  igraph_is_simple(&lineG, &simple);
  assert(simple);
  igraph_vit_create(&lineG, igraph_vss_all(), &vit);
  asprintf(&fname, "%s", fadj->filename[0]);
  file = fopen(fname, "w");
  while (!IGRAPH_VIT_END(vit)) {
    vid = (igraph_integer_t)IGRAPH_VIT_GET(vit);
    igraph_vector_init(&nbr, 0);
    igraph_neighbors(&lineG, &nbr, vid, IGRAPH_ALL);
    if (igraph_vector_size(&nbr) < 1) {
      igraph_vector_destroy(&nbr);
      IGRAPH_VIT_NEXT(vit);
      continue;
    }
    /* vertex ID of line graph is edge ID of original graph */
    igraph_edge(&G, vid, &uG, &vG);
    fprintf(file, "%ld %ld", (long int)uG, (long int)vG);
    for (i = 0; i < igraph_vector_size(&nbr); i++) {
      vid = (igraph_integer_t)VECTOR(nbr)[i];
      igraph_edge(&G, vid, &uG, &vG);
      fprintf(file, ",%ld %ld", (long int)uG, (long int)vG);
    }
    fprintf(file, "\n");
    igraph_vector_destroy(&nbr);
    IGRAPH_VIT_NEXT(vit);
  }
  fclose(file);
  free(fname);
  igraph_vit_destroy(&vit);

  /* Write edge list to file. */
  igraph_eit_create(&lineG, igraph_ess_all(IGRAPH_EDGEORDER_ID), &eit);
  asprintf(&fname, "%s", fedge->filename[0]);
  file = fopen(fname, "w");
  while (!IGRAPH_EIT_END(eit)) {
    eid = (igraph_integer_t)IGRAPH_EIT_GET(eit);
    /* vertex ID of line graph is edge ID of original graph */
    igraph_edge(&lineG, eid, &uL, &vL);
    igraph_edge(&G, uL, &uG, &vG);
    fprintf(file, "%ld %ld ", (long int)uG, (long int)vG);
    igraph_edge(&G, vL, &uG, &vG);
    fprintf(file, "%ld %ld\n", (long int)uG, (long int)vG);
    IGRAPH_EIT_NEXT(eit);
  }
  fclose(file);
  free(fname);
  igraph_eit_destroy(&eit);

  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
  igraph_destroy(&G);
  igraph_destroy(&lineG);

  return 0;
}
