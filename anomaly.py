###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Locate the region with the greatest changes in a sequence of time ordered
# graphs.

# third party library
from anomaly import change_linegraph_level
from anomaly import change_node_level
from anomaly import write_change_dist_all
from anomaly import write_change_dist_pm
# standard library
import argparse

###################################
# script starts here
###################################

# setup parser for command line options
s = "Locate the region with the greatest changes.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--resolution", metavar="string", required=True,
                    help="resolution level: node, linegraph, community")
parser.add_argument("--edgelistA", metavar="file", required=True,
                    help="file containing edge list, one edge per line")
parser.add_argument("--edgelistB", metavar="file", required=True,
                    help="file containing edge list, one edge per line")
parser.add_argument("--adjlistA", metavar="file", required=False,
                    help="file containing adjacency list, one node per line")
parser.add_argument("--adjlistB", metavar="file", required=False,
                    help="file containing adjacency list, one node per line")
parser.add_argument("--changedist", metavar="file", required=True,
                   help="write change distributions to files with this prefix")
parser.add_argument("--maxchange", metavar="file", required=True,
                    help="write greatest changes to files with this prefix")
args = parser.parse_args()

# get command line arguments & sanity checks
resolution = args.resolution
fedgelistA = args.edgelistA
fedgelistB = args.edgelistB
fadjlistA = args.adjlistA  # for line graph only
fadjlistB = args.adjlistB  # for line graph only
fchangedist = args.changedist
fmaxchange = args.maxchange

if resolution not in ("node", "linegraph", "community"):
    raise ValueError("Unsupported resolution: %s" % resolution)

if resolution == "node":
    change_node_level(fedgelistA, fedgelistB, fchangedist, fmaxchange)
elif resolution == "linegraph":
    if ((len(fadjlistA.strip()) < 1)
        or (len(fadjlistB.strip()) < 1)):
        raise ValueError("Adjacency lists required for line graphs.")
    change_linegraph_level(
        fedgelistA, fedgelistB, fadjlistA, fadjlistB, fchangedist, fmaxchange)
elif resolution == "community":
    raise NotImplementedError(
        "Resolution not yet implemented: %s" % resolution)
