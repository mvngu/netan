###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Scatterplot of lifespan versus H(D_i).

import argparse
import os

###################################
# helper functions
###################################

# this function is stolen from comtype.py
def lifespan(timeline):
    """
    The lifespan of a dynamic community.

    INPUT:

    - timeline -- the timeline of a dynamic community.
    """
    return len(parse_trace(timeline))

# This function is stolen from entropy.py
def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline a dynamic
    community.  Each step community is represented as

        year com

    where "year" refers to the snapshot year and "com" refers to the index
    of the community in the given year.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    return trace.split("->")

###################################
# script starts here
###################################

# setup parser for command line options
# NOTE: Either of the following use cases:
#
# * Supply --infile and --outfile; or
# * Supply --entropydir and --maxlifespan.
s = "Scatterplot of lifespan versus H(D_i).\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--infile", metavar="path",
                    help="path to file to read")
parser.add_argument("--outfile", metavar="path",
                    help="path to file to which we write")
parser.add_argument("--entropydir", metavar="path",
                    help="path to directory with files containing H values")
parser.add_argument("--maxlifespan", metavar="int",
                    help="maximum lifespan of a dynamic community")
args = parser.parse_args()

# get the command line arguments
infile = args.infile
outfile = args.outfile
entropydir = args.entropydir
maxlifespan = args.maxlifespan

if (infile is not None) and (outfile is not None):
    infile = open(infile, "r")
    outfile = open(outfile, "w")
    outfile.write("lifespan,H\n")       # write header for CSV file
    for line in infile:
        H, T = line.strip().split(";")  # get H value and timeline
        ell = lifespan(T)
        outfile.write("%d,%s\n" % (ell, H))
    infile.close()
    outfile.close()
elif (entropydir is not None) and (maxlifespan is not None):
    fname = os.path.join(entropydir, "entropy.csv")
    ofile = open(fname, "w")
    ofile.write("lifespan,H\n")         # write header for CSV file
    for i in range(int(maxlifespan) + 1):
        fname = os.path.join(entropydir, "entropy-%d.dat" % i)
        if not os.path.isfile(fname):
            continue
        f = open(fname, "r")
        for line in f:
            H, _ = line.strip().split(";")
            ofile.write("%d,%s\n" % (i, H))
        f.close()
    ofile.close()
else:
    raise ValueError("Can't figure out what you wanted to do.")
