###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Construct a network of dynamic communities from the traces.  We represent
# each dynamic community as a node.  Two nodes are connected by an edge if
# the corresponding traces share a common element.

import argparse
import os

###################################
# helper functions
###################################

def edge_list(D, fname):
    """
    Construct the edge list of the network and write the edge list to file.

    WARNING: This function modifies its arguments.

    INPUT:

    - D -- mapping from traces to nonnegative integers.
    - fname -- write the edge list to this file.
    """
    T = set(D.keys())
    V = D.keys()
    f = open(fname, "w")
    # write edge list
    for _ in range(len(D) - 1):
        u = set(V[0])
        for i in range(1, len(V)):
            v = set(V[i])
            if len(u.intersection(v)) > 0:
                f.write("%d %d\n" % (D[V[0]], D[V[i]]))
                if V[0] in T:
                    T.remove(V[0])
                if V[i] in T:
                    T.remove(V[i])
        del V[0]
    # write isolated nodes as self-loops
    for u in T:
        f.write("%d %d\n" % (D[u], D[u]))
    f.close()

def read_traces(fname):
    """
    Get the traces of dynamic communities.

    INPUT:

    - fname -- file with traces of dynamic communities.
    """
    D = dict()
    f = open(fname, "r")
    for line in f:
        trace = tuple(line.strip().split())
        if trace not in D:
            D.setdefault(trace, len(D))
    f.close()
    return D

###################################
# script starts here
###################################

# setup parser for command line options
s = "Construct a network of dynamic community.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--ftrace", metavar="path", required=True,
                    help="read the traces from this file")
parser.add_argument("--output", metavar="path", required=True,
                    help="write the edge list to this file")
args = parser.parse_args()

# get the command line arguments and sanity checks
ftrace = args.ftrace
output = args.output

# read in edge list and rename each unique node
edge_list(read_traces(ftrace), output)
