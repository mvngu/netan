#!/usr/bin/env bash

###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

VER="x.y.z"
base=0
new=1

for i in {1..UPPERLIMIT}; do
    if [ ! -f linux-"$VER.$i".tar.lzma ]; then
        base=$(( $i - 1 ))  # the base version
        new=$i              # the new version
        if [ -f linux-"$VER.$base".tar.lzma ]; then
            unlzma linux-"$VER.$base".tar.lzma
            tar -xf linux-"$VER.$base".tar
            mv linux-"$VER.$base" linux-"$VER.$new"
            lzma linux-"$VER.$base".tar
            wget http://www.kernel.org/pub/linux/kernel/v"$VER"/patch"$new".bz2
            bunzip2 patch"$new".bz2
            cd linux-"$VER.$new"
            patch -p1 < ../patch"$new"
            cd ..
            rm patch"$new"
            tar -cf linux-"$VER.$new".tar linux-"$VER.$new"
            lzma linux-"$VER.$new".tar
            rm -rf linux-"$VER.$new"
        else
            echo "Base version linux-$VER.$base.tar.lzma doesn't exist"
        fi
    fi
done
