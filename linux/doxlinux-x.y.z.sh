#!/usr/bin/env bash

###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Get XML format of caller and callee graphs of Linux source code.

VER="x.y.z"
DOXCFG="doxlinux-v$VER.cfg"
DOXLOG="doxygen-v$VER.log"
IN="INPUT                  ="
INDIR="INPUT                  = \/dev\/shm\/mvngu\/linux\/v$VER"
OUT="OUTPUT_DIRECTORY       ="
OUTDIR="OUTPUT_DIRECTORY      = \/dev\/shm\/mvngu\/linux\/xml\/v$VER"
SRCDIR="/dev/shm/mvngu/linux/v$VER"
XMLDIR="/dev/shm/mvngu/linux/xml"
CUR="/dev/shm/mvngu/linux"

mkdir "$XMLDIR"/v"$VER"

# process linux-x.y.z
cd "$CUR"
cat ./doxygen.cfg | sed s/"$OUT"/"$OUTDIR\/linux-$VER"/ | sed s/"$IN"/"$INDIR\/linux-$VER"/ > "$DOXCFG"
echo "v$VER" >> "$DOXLOG"
nice -n 19 ionice -c 2 -n 7 /scratch/mvngu/usr/bin/doxygen "$DOXCFG"
cd "$XMLDIR"/v"$VER"
mv linux-"$VER"/xml ./xml
rm -rf linux-"$VER"
mv xml linux-"$VER"
tar -cf linux-"$VER".tar linux-"$VER"
lzma linux-"$VER".tar
rm -rf linux-"$VER"
rm -rf "$SRCDIR"/linux-"$VER"

# process linux-x.y.z.n
for i in {1..UPPERLIMIT}; do
    cd "$CUR"
    cat ./doxygen.cfg | sed s/"$OUT"/"$OUTDIR\/linux-$VER.$i"/ | sed s/"$IN"/"$INDIR\/linux-$VER.$i"/ > "$DOXCFG"
    echo "v$VER.$i" >> "$DOXLOG"
    nice -n 19 ionice -c 2 -n 7 /scratch/mvngu/usr/bin/doxygen "$DOXCFG"
    cd "$XMLDIR"/v"$VER"
    mv linux-"$VER.$i"/xml ./xml
    rm -rf linux-"$VER.$i"
    mv xml linux-"$VER.$i"
    tar -cf linux-"$VER.$i".tar linux-"$VER.$i"
    lzma linux-"$VER.$i".tar
    rm -rf linux-"$VER.$i"
    rm -rf "$SRCDIR"/linux-"$VER.$i"
done
