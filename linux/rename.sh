#!/usr/bin/env bash

###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

VER="x.y.z"

echo linux-"$VER".tar.bz2
tar -jxf linux-"$VER".tar.bz2
mv linux linux-"$VER"
tar -cf linux-"$VER".tar linux-"$VER"
lzma linux-"$VER".tar
rm -rf linux-"$VER"

for i in {0..UPPERLIMIT}; do
    if [ -f linux-"$VER.$i".tar.bz2 ]; then
        echo linux-"$VER.$i".tar.bz2
        tar -jxf linux-"$VER.$i".tar.bz2
        if [ -d linux ]; then
            mv linux linux-"$VER.$i"
            tar -cf linux-"$VER.$i".tar linux-"$VER.$i"
            lzma linux-"$VER.$i".tar
            rm -rf linux-"$VER.$i"
        elif [ -d linux-"$VER.$i" ]; then
            tar -cf linux-"$VER.$i".tar linux-"$VER.$i"
            lzma linux-"$VER.$i".tar
            rm -rf linux-"$VER.$i"
        else
            echo "Possible bad name for linux-$VER.$i.tar.bz2"
        fi
    fi
done
