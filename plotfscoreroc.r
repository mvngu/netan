###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> ctypepref outpref
#
# Plot the F-score and ROC graph for our prediction method.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
ctypepref <- args[1]   # file prefix of files with data in CSV format
outpref <- args[2]  # file prefix to be used to name the plot files

D1 <- read.csv(file=paste(ctypepref,"-1.dat", sep=""), header=TRUE, sep=",")
D2 <- read.csv(file=paste(ctypepref,"-2.dat", sep=""), header=TRUE, sep=",")
D3 <- read.csv(file=paste(ctypepref,"-3.dat", sep=""), header=TRUE, sep=",")
D4 <- read.csv(file=paste(ctypepref,"-4.dat", sep=""), header=TRUE, sep=",")

# plot the F-score
ymax <- max(max(D1$Fscore), max(D2$Fscore), max(D3$Fscore), max(D4$Fscore))
pdf(paste(outpref,"-fscore.pdf", sep=""))
plot(D1$Hthreshold, D1$Fscore, xlab="", ylab="", type="l", col="black",
     xlim=c(0,3), ylim=c(0,ymax))
par(new=TRUE)
plot(D2$Hthreshold, D2$Fscore, xlab="", ylab="", type="l", col="red",
     xlim=c(0,3), ylim=c(0,ymax))
par(new=TRUE)
plot(D3$Hthreshold, D3$Fscore, xlab="", ylab="", type="l", col="blue",
     xlim=c(0,3), ylim=c(0,ymax))
par(new=TRUE)
plot(D4$Hthreshold, D4$Fscore, type="l", col="green",
     xlim=c(0,3), ylim=c(0,ymax), main="H threshold vs. F-score",
     xlab="H threshold", ylab="F-score")
legend(x="topright", inset=0,
       legend=c("next 1 snapshot", "next 2 snapshots",
         "next 3 snapshots", "next 4 snapshots"),
       col=c("black", "red", "blue", "green"), lwd=5, cex=0.8)
dev.off()

# the ROC graph
xmax <- max(max(D1$FPR), max(D2$FPR), max(D3$FPR), max(D4$FPR))
ymax <- max(max(D1$TPR), max(D2$TPR), max(D3$TPR), max(D4$TPR))
pdf(paste(outpref,"-roc.pdf", sep=""))
plot(D1$FPR, D1$TPR, xlab="", ylab="", type="l", col="black",
     xlim=c(0,xmax), ylim=c(0,ymax))
par(new=TRUE)
abline(0, 1, xlim=c(0,xmax), ylim=c(0,ymax))
par(new=TRUE)
plot(D2$FPR, D2$TPR, xlab="", ylab="", type="l", col="red",
     xlim=c(0,xmax), ylim=c(0,ymax))
par(new=TRUE)
plot(D3$FPR, D3$TPR, xlab="", ylab="", type="l", col="blue",
     xlim=c(0,xmax), ylim=c(0,ymax))
par(new=TRUE)
plot(D4$FPR, D4$TPR, type="l", col="green",
     xlim=c(0,xmax), ylim=c(0,ymax), main="ROC graph",
     xlab="false positive rate", ylab="true positive rate")
legend(x="topright", inset=0,
       legend=c("next 1 snapshot", "next 2 snapshots",
         "next 3 snapshots", "next 4 snapshots"),
       col=c("black", "red", "blue", "green"), lwd=5, cex=0.8)
dev.off()
