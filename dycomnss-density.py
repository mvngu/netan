###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script MUST be run from Sage.
#
# We compute the density of:
#
# * Dynamic communities that interact among each other.
# * Communities that are source of split or destination of merge.
#
# These densities are written to file.

from sage.all_cmdline import *
# Use optparse because Sage 4.8 has Python 2.6.  From Python 2.7 onwards,
# we could use argparse.
import optparse
import os

###################################
# helper functions
###################################

def count_source_sink(G):
    """
    Count the number of nodes that act as source of community split or
    sink of community merge.  Let v be a vertex in a digraph G, where G is
    assumed to be a network derived from community matching.  If the
    out-degree of v is > 1, then v is a source for community split.  If the
    in-degree of v is > 1, then v is a sink for community merge.

    INPUT:

    - G -- a directed graph.
    """
    assert isinstance(G, DiGraph)
    nss = 0  # number of nodes that are source or sink
    for v in G.vertex_iterator():
        if (G.out_degree(v) > 1) or (G.in_degree(v) > 1):
            nss += 1
    return nss

def density_exchange_communities(si, G, fname):
    """
    The density of communities that act as source of split or destination
    for merge.

    INPUT:

    - si -- the sample number.  For each parameter setting, we sampled a
      number of synthetic networks from a population of networks.
    - G -- a digraph representing the evolution of all dynamic communities.
      This contains all the traces of all dynamic communities.
    - fname -- append results to this file.
    """
    # get the properties we are interested in
    nss_observed = count_source_sink(G)
    nnode = G.order()
    # write results to file
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write("sample ID,NSS observed,#communities,density\n")
        f.close()
    # Write the density to the given file.  We do not overwrite the file, if
    # it exists.  Rather, we append our results to the file.
    R = RR(nss_observed)  # nss in the sampled network
    N = RR(nnode)         # possible number of nss, i.e. # nodes in digraph
    density = R / N
    f = open(fname, "a")
    f.write("%d,%d,%d,%.15f\n" % (si, nss_observed, nnode, density))
    f.close()

def density_exchange_dycom(si, G, Dycom, fname):
    """
    The density of dynamic communities that have participated in a community
    split or merge.  In other words, we compute the density of dynamic
    communities that have nodes that act as source of split or destination
    of merge.

    INPUT:

    - si -- the sample number.  For each parameter setting, we sampled a
      number of synthetic networks from a population of networks.
    - G -- a digraph representing the evolution of all dynamic communities.
      This contains all the traces of all dynamic communities.
    - Dycom -- the traces of all dynamic communities.
    - fname -- append results to this file.
    """
    assert isinstance(G, DiGraph)
    NSM = 0  # no. dycoms that have participated in a split or merge
    for D in Dycom:
        for v in D:
            if (G.out_degree(v) > 1) or (G.in_degree(v) > 1):
                NSM += 1
                break
    # write results to file
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write("sample ID,#dycom split merge,#dycom,density\n")
        f.close()
    # Write the density to the given file.  We do not overwrite the file, if
    # it exists.  Rather, we append our results to the file.
    N = len(Dycom)  # no. dycom in all
    density = RR(NSM) / RR(N)
    f = open(fname, "a")
    f.write("%d,%d,%d,%.15f\n" % (si, NSM, N, density))
    f.close()

def edge_set(fname):
    """
    Construct the edge set from the given file.  Each edge is assumed to
    be directed.

    INPUT:

    - fname -- file containing the edge list of a network.
    """
    # Here is the mental model of directed edge list:
    # u : [v1, v2, ...]
    Edge = dict()
    f = open(fname, "r")
    for line in f:
        u, v = line.strip().split()
        if u in Edge:
            Edge[u].append(v)
        else:
            Edge.setdefault(u, [v])
    f.close()
    return Edge

def trace_community(v, G):
    """
    Trace all dynamic communities from the given start node.

    INPUT:

    - v -- the node from which to start tracing dynamic communities.  This
      must be a node in the given digraph.
    - G -- a directed graph representation of community matches.
    """
    # Get all shortest paths starting from v.  This will also give us
    # partial paths, i.e. paths that don't end with leaves.  The result is
    # a dictionary where keys are end points of paths and corresponding
    # values are lists of nodes in the paths from v to the keys.
    D = G.shortest_paths(v)
    # Prune partial paths, leaving only paths from v to leaves.  Such full
    # paths are traces of dynamic communities.
    for u in D.keys():
        if G.out_degree(u) > 0:
            del D[u]
    return D.values()

def trace_dynamic_communities(G):
    """
    Trace all dynamic communities using the given digraph.  Each community
    is traced from its birth to its death, or possibly up to the latest
    snapshot for which we have data.

    INPUT:

    - G -- a digraph representation of community matches.  We use this
      digraph representation to trace the evolution of each dynamic
      community.
    """
    assert isinstance(G, DiGraph)
    # Get all community births.  Each of these is the start of some
    # dynamic community.
    B = [v for v in G if G.in_degree(v) == 0]
    # get full traces of all dynamic communities
    D = list()
    for v in B:
        map(D.append, trace_community(v, G))
    return D

###################################
# script starts here
###################################

# setup parser for command line options
s = "Density of interaction among dynamic communities.\n"
s += "Usage: %prog arg1 arg2 ..."
parser = optparse.OptionParser(usage=s)
parser.add_option("--datadir", metavar="path",
                  help="directory with sample networks")
parser.add_option("--nsample", metavar="integer", type=int,
                  help="how many samples; numbered from 1")
parser.add_option("--outdir", metavar="path",
                  help="write results to files under this directory")
options, _ = parser.parse_args()

# get command line arguments & sanity checks
if ((options.datadir is None)
    or (options.nsample is None)
    or (options.outdir is None)):
    raise optparse.OptionValueError(
        "All options must be used. Use -h for help.")
datadir = options.datadir
nsample = options.nsample
outdir = options.outdir
if nsample < 1:
    raise ValueError("Invalid number of samples")

# process each sample
for i in range(1, nsample + 1):
    # construct digraph and get all traces of all dynamic communities
    fname = os.path.join(datadir, "%d.dat" % i)
    G = DiGraph(edge_set(fname))
    G.allow_loops(False)
    G.allow_multiple_edges(False)
    D = trace_dynamic_communities(G)
    # compute densities and write results to file
    fname = os.path.join(outdir, "exchange-dycom.dat")
    density_exchange_dycom(i, G, D, fname)
    fname = os.path.join(outdir, "exchange-nss.dat")
    density_exchange_communities(i, G, fname)
