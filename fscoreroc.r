###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile
#
# The F-score, ROC points, and accuracy of our prediction method.  Suppose we
# have the following numbers:
#
# * true positive (TP) -- outcome is positive and actual value is positive.
# * false positive (FP) -- outcome is positive and actual value is negative.
# * true negative (TN) -- outcome is negative and actual value is negative.
# * false negative (FN) -- outcome is negative and actual value is positive.
#
# Then the accuracy is defined as
#
# ACC = (TP + TN) / (TP + FP + FN + TN)
#
# the false positive rate (FPR) is defined as
#
# FPR = FP / (FP + TN)
#
# the true positive rate (TPR) is defined as
#
# TPR = TP / (TP + FN)
#
# the precision p is defined as
#
# p = TP / (TP + FP)
#
# the recall r is defined as
#
# r = TP / (TP + FN)
#
# Then F-score is then defined as
#
# F-score = 2rp / (r + p)
#
# and the coordinate (FPR, TPR) is a point in ROC space.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
# Read from and write to this file.  This file should be in CSV format and
# contain data structured as
#
# H threshold,true positive,false positive,true negative,false negative
fname <- args[1]

D <- read.csv(file=fname, header=TRUE, sep=",")
colnames(D) <- c("Hthreshold", "TP", "FP", "TN", "FN")

# F-scores
precision <- D$TP / (D$TP + D$FP)
precision[is.na(precision)] <- 0
recall <- D$TP / (D$TP + D$FN)
recall[is.na(recall)] <- 0
F <- 2 * ((precision * recall) / (precision + recall))
F[is.na(F)] <- 0

# ROC points
TPR <- D$TP / (D$TP + D$FN)  # true positive rate
TPR[is.na(TPR)] <- 0
FPR <- D$FP / (D$FP + D$TN)  # false positive rate
FPR[is.na(FPR)] <- 0

# accuracy
ACC <- (D$TP + D$TN) / (D$TP + D$FP + D$FN + D$TN)
ACC[is.na(ACC)] <- 0

D <- cbind(D, F, TPR, FPR, ACC)
colnames(D) <- c("Hthreshold", "TP", "FP", "TN", "FN",
                 "Fscore", "TPR", "FPR", "ACC")
write.table(D, file=fname, quote=FALSE, sep=",", row.names=FALSE)
