Dynamic communities
===================

Below is a rough outline of how to obtain the trace of each dynamic
community.

#. Extract all communities with a given minimum size.  Use the script
   ``minsize.py`` for this purpose.
#. Match all step communities with a given minimum size, across all
   available snapshots.  This should generate a collection of match
   data.  Use the script ``match.py`` for this.
#. From the match data, get all events in a life-cycle model, but
   exclude the death event.  Use the script ``event.py`` for this with
   option ``--deathonly false``.
#. Obtain the trace of the lifespan of each dynamic community.  Use
   the script ``tracecomm.py`` for this.
#. From the match data and the trace of lifespan, get all deaths.  Use
   the script ``event.py`` for this with option ``--deathonly true``.

See the script ``godycom.sh`` for a rough template on the above outline.
