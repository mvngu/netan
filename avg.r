###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile
#
# Compute the average of a list of numbers.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
fname <- args[1]  # file with a bunch of numbers, one per line

T <- read.csv(file=fname, header=FALSE)
colnames(T) <- c("quantity")
avg <- mean(T$quantity)
write(paste(avg, sep=""), stdout())
