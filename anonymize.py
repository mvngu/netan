###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Anonymize a dataset to be released.

import argparse
import codecs
import os

###################################
# helper functions
###################################

def anonymize(fname):
    """
    Anonymize the given dataset.

    INPUT:

    - fname -- the dataset to anonymize.
    """
    Pub = dict()
    D = dict()
    s = ""
    f = codecs.open(fname, "r", "utf-8")
    # read in the dataset
    for line in f:
        p = line.strip().split(",")
        year = int(p[0])
        author = p[1:]
        if year in Pub:
            Pub[year].append(author)
        else:
            Pub.setdefault(year, [author])
    f.close()
    # map author names to nonnegative integers
    for year in sorted(Pub.keys()):
        for Authors in sorted(Pub[year]):
            A = list()
            for au in sorted(Authors):
                if au not in D:
                    D.setdefault(au, len(D))
                A.append(str(D[au]))
            p = "%d," % year
            p += ",".join(A)
            s += "%s\n" % p
    f = open(fname, "w")
    f.write(s)
    f.flush()
    os.fsync(f.fileno())
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Anonymize a dataset to be released.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fname", metavar="file", required=True,
                    help="a dataset file")
args = parser.parse_args()
fname = args.fname

anonymize(fname)
