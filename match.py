###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Community matches with direct/indirect continuation.  Missing observations
# are allowed, depending on the command line argument.

import argparse
import os

###################################
# script starts here
###################################

# setup parser for command line options
s = "Community matches with direct/indirect continuation.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--smin", metavar="integer", required=True, type=int,
                    help="minimum community size")
parser.add_argument("--theta", metavar="float", required=True, type=float,
                    help="matching threshold")
parser.add_argument("--delta", metavar="integer", required=True, type=int,
                    help="maximum allowable consecutive missing observations")
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="file with all snapshot indices, one per line")
parser.add_argument("--comdir", metavar="path", required=True,
                    help="directory to read community structures")
parser.add_argument("--eventdir", metavar="path", required=True,
                    help="directory to read/write event data")
args = parser.parse_args()

# get command line arguments & sanity checks
smin = args.smin
theta = args.theta
delta = args.delta
fsindex = args.fsindex
comdir = args.comdir
eventdir = args.eventdir
assert smin > 0
assert theta >= 0.0
assert delta >= 0

dirname = os.path.join(eventdir, "smin-%d" % smin,
                       "theta-%s_delta-%d" % (str(theta), delta))
if not os.path.isdir(dirname):
    os.makedirs(dirname)
os.system("../../netan/match --fsindex %s --matchdir %s --comdir %s --theta %s --delta %s" %
          (fsindex, dirname, comdir, theta, delta))
