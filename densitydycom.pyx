###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This is a C extension for Python.
#
# Modelling the evolution of interaction density among dynamic communities.

import math
import random

###################################
# helper functions
###################################

cdef int add_new_node(dict V, int t, int n, double lmbd):
    """
    Construct a new node for the given time step.

    WARNING: This function modifies its arguments.

    INPUT:

    - V -- set of nodes.
    - t -- current time step.
    - n -- the running count of the number of nodes so far.
    - lmbd -- parameter for the exponential distribution, lambda.  We use the
      exponential distribution to model the lifespan of each dynamic community.
    """
    cdef int age
    cdef int ell
    cdef tuple node
    age = 1
    ell = int(math.ceil(random.expovariate(lmbd)))  # lifespan
    assert ell > 0
    node = (n, t, ell, age)
    label = "%d,%d" % (n, t)
    V.setdefault(label, node)
    return n + 1  # number of nodes so far

cdef tuple bipartite_network(dict V, int t, list E):
    """
    From the given set of front communities, generate a preliminary set of
    step communities.  Together, these two sets of nodes form a bipartite
    network.  We also update the edge set as we go along.

    WARNING: This function modifies its arguments.

    INPUT:

    - V -- set of front communities.  This is represented as a dictionary.
    - t -- current time step.
    - E -- the running edge set for the network of dynamic communities.
    """
    cdef dict Vs
    cdef dict Vt
    cdef int index
    cdef int timestep
    cdef int lifespan
    cdef int age
    Vs = dict()  # front communities
    Vt = dict()  # step communities
    for k in V:
        index, timestep, lifespan, age = V[k]
        if age == lifespan:
            continue
        assert age < lifespan
        assert t == timestep + 1
        # set of front communities
        Vs.setdefault(k, V[k])
        # set of step communities
        node = (index, t, lifespan, age + 1)
        label = "%d,%d" % (index, t)
        Vt.setdefault(label, node)
        # update the running edge set; only directed edge
        E.append((k, label))
    return Vs, Vt

def build_dycom_network(int niter, double lmbd, double prob, int ncom,
                        outfile):
    """
    Generate a network to represent the evolution of interaction among
    dynamic communities.

    INPUT:

    - niter -- number of iterations, also the number of time steps.
    - lmbd -- parameter for the exponential distribution, lambda.  We use the
      exponential distribution to model the lifespan of each dynamic community.
    - prob -- probability of merge or split.
    - ncom -- initial number of step communities.  We treat each step
      community as a node.
    - outfile -- write the edge list to this file.
    """
    cdef int n
    cdef int t
    cdef set Node
    cdef list Edge
    Node = set()                # the set of vertices
    Edge = list()               # the edge list
    Vt = init_pool(ncom, lmbd)  # communities at time step t = 0
    n = len(Vt)                 # running count of #nodes
    Node = Node.union(set(Vt.keys()))
    for t in range(1, niter):
        Vs, Vt = bipartite_network(Vt, t, Edge)
        n = add_new_node(Vt, t, n, lmbd)
        random_split(Vs, Vt, Edge, prob)
        random_merge(Vs, Vt, Edge, prob)
        Node = Node.union(set(Vt.keys()))
    # write edge list to file
    # first add all the directed edges
    f = open(outfile, "w")
    for u, v in sorted(Edge):
        f.write("%s %s\n" % (u, v))
        if u in Node:
            Node.remove(u)
        if v in Node:
            Node.remove(v)
    # Now add all the isolated nodes.  We treat each isolated node as a
    # self-loop.  However, when we read in the resulting edge list, we should
    # remove all self-loops.  That way, we would be able to recover
    # isolated nodes.
    for v in sorted(list(Node)):
        f.write("%s %s\n" % (v, v))
    f.close()

cdef dict init_pool(int ncom, double lmbd):
    """
    Initialize the pool of nodes.  That is, generate a pool of nodes at
    time step t = 0.  Each node is assigned a lifespan drawn from an
    exponential distribution with the given parameter lmbd.

    INPUT:

    - ncom -- initial number of step communities.  We treat each step
      community as a node.
    - lmbd -- parameter for the exponential distribution, lambda.  We use the
      exponential distribution to model the lifespan of each dynamic community.
    """
    cdef dict V
    cdef int t
    cdef int age
    cdef int i
    cdef int ell
    V = dict()  # initial pool of nodes
    t = 0       # time step
    age = 1     # how many time steps has this node appeared in
    for i in range(ncom):
        ell = int(math.ceil(random.expovariate(lmbd)))  # lifespan
        assert ell > 0
        node = (i, t, ell, age)
        label = "%d,%d" % (i, t)
        V.setdefault(label, node)
    return V

cdef void random_merge(dict Vs, dict Vt, list E, double p):
    """
    Create a number of random merge events.  For each node v in Vt, we draw a
    probability q uniformly at random from [0,1].  If q < p, then we choose
    two nodds u and w uniformly at random from Vs.  The node v is connected
    to each of u and w.  This is the merge step.

    WARNING: This function modifies its arguments.

    INPUT:

    - Vs -- set of front communities.  For the purposes of abstraction, these
      are just nodes.
    - Vt -- set of step communities.  These are just nodes.
    - E -- the running edge set for the network of dynamic communities.
    - p -- probability of merge.
    """
    cdef double q
    for v in Vt:
        q = random.random()
        if q < p:
            u, w = random.sample(Vs, 2)
            assert u != w
            E.append((u, v))
            E.append((w, v))

cdef void random_split(dict Vs, dict Vt, list E, double p):
    """
    Create a number of random split events.  For each node v in Vs, we draw a
    probability q uniformly at random from [0,1].  If q < p, then we choose
    two nodds u and w uniformly at random from Vt.  The node v is connected
    to each of u and w.  This is the split step.

    WARNING: This function modifies its arguments.

    INPUT:

    - Vs -- set of front communities.  For the purposes of abstraction, these
      are just nodes.
    - Vt -- set of step communities.  These are just nodes.
    - E -- the running edge set for the network of dynamic communities.
    - p -- probability of split.
    """
    cdef double q
    for v in Vs:
        q = random.random()
        if q < p:
            u, w = random.sample(Vt, 2)
            assert u != w
            E.append((v, u))
            E.append((v, w))
