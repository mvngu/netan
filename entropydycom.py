###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Entropy of a dynamic community with respect to a given type of change.
# Here we consider two types of changes:
#
# * The history of members leaving a dynamic community.
# * The history of unique new members joining a dynamic community.

import argparse
import math
import os

###################################
# helper functions
###################################

def change_community_level(fsindex, comdir, tracedir, outdir):
    """
    Change at the level of dynamic communities.  Note that we exclude all
    newborn communities, those communities with a lifespan of one.

    INPUT:

    - fsindex -- a file listing all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    - comdir -- directory where communities are stored.
    - tracedir -- directory where community traces are stored.
    - outdir -- directory where we will write data on anomaly.
    """
    Sindex = read_sindex(fsindex)
    Com = read_community(Sindex, comdir)
    T = list()
    for ell in range(2, len(Sindex) + 1):
        read_trace(tracedir, ell, T)
    fname = os.path.join(outdir, "plus-unique.seq")
    fplus = open(fname, "w")
    fplus.write("entropy\n")
    fname = os.path.join(outdir, "minus.seq")
    fminus = open(fname, "w")
    fminus.write("entropy\n")
    for trace in T:
        History = history_new_members(Com, trace)
        # if len(History) == 0:
        #     continue
        assert len(History) > 0
        H = entropy(History)
        fplus.write("%.15f\n" % H)
        History = history_members_depart(Com, trace)
        assert len(History) > 0
        H = entropy(History)
        fminus.write("%.15f\n" % H)
    fplus.close()
    fminus.close()

def entropy(History):
    """
    The entropy of the history of some type of changes.  The entropy is
    scaled in proportion to the number of leading zeros in the history of
    changes.

    INPUT:

    - History -- a list giving the sequence of changes with respect to some
      event.  For example, suppose we are interested in the number of new
      nodes joining a dynamic community at snapshot i.  Then History[i]
      represents the required count in snapshot i.
    """
    S = float(math.fsum(History))
    if S == 0.0:
        return 0.0
    assert S > 0.0
    P = [e / S for e in History]
    H = list()
    for p in P:
        if p == 0.0:
            H.append(0.0)
        else:
            H.append(p * math.log(p))  # natural logarithm
    H = -1.0 * math.fsum(H)
    if H == 0.0:
        return 0.0
    ell = len(History)
    x = leading_zeros(History)
    return H * math.exp(-x / ell)

def history_members_depart(Com, trace):
    """
    The history of members departing the given dynamic community D.  Let i and
    j be consecutive snapshots, where i < j.  Let N_i and N_j be the set of
    nodes at snapshots i and j, respectively.  All the nodes from N_i that
    leave N_j can be obtained by the set difference N_i - N_j.  That is, we
    consider all nodes that are in N_i but not in N_j.

    INPUT:

    - Com -- a dictionary of all communities in all snapshots.  We use the key
      "y i" to refer to community i in snapshot y.
    - trace -- the trace of a dynamic community.  The trace is represented as
      (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk).  Each 2-tuple
      is considered for departing members.
    """
    # the history of members leaving a dynamic community
    History = list()
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # get the required changes
        A = Com[a]
        B = Com[b]
        d = len(A.difference(B))
        History.append(d)
    return History

def history_new_members(Com, trace):
    """
    The history of new members in a given dynamic community D.  Let i and j be
    consecutive snapshots, where i < j.  Let N_i and N_j be the set of nodes
    at snapshots i and j, respectively.  All the new nodes that join the
    network at snapshot j can be obtained by the set difference N_j - N_i.
    That is, we consider all nodes that are in N_j but not in N_i.

    INPUT:

    - Com -- a dictionary of all communities in all snapshots.  We use the key
      "y i" to refer to community i in snapshot y.
    - trace -- the trace of a dynamic community.  The trace is represented as
      (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk).  Each 2-tuple
      is considered for new members.
    """
    History = list()  # history of new members
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # get the required changes
        A = Com[a]
        B = Com[b]
        History.append(len(B.difference(A)))
    return History

def leading_zeros(History):
    """
    The number of leading zeros in the given history of changes.  A leading
    zero means zero change since the last snapshot in which the change was
    observed.  Suppose we represent the history of a particular change by
    the sequence

    c_1, c_2, ..., c_{n-1}, c_n

    If there exists a largest k, with 1 < k < n, such that c_k > 0 and
    c_{k+1} = c_{k+2} = ... = c_n = 0, then we say that the c_{k+i} (i > 0) are
    the leading zeros of the history of changes.
    """
    i = len(History) - 1
    nzero = 0
    while History[i] == 0.0:
        nzero += 1
        i -= 1
        if i < 0:
            break
    assert nzero <= len(History)
    return nzero

def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline of a dynamic
    community.  Each step community is represented as

        si com

    where "si" refers to the snapshot index and "com" refers to the index
    of the community in the given snapshot.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    T = trace.split("->")
    # transform the pattern "si1 com1,n" into "si1 com1"
    for i in range(len(T)):
        if "," in T[i]:
            sicom, _ = T[i].split(",")
            T[i] = sicom
    return T

def read_community(Sindex, comdir):
    """
    Read in all the communities in each network snapshot.  For a network
    snapshot y, each community in the snapshot is assigned a unique
    nonnegative integer, starting from zero.  Let i be the unique label
    assigned to some community.  We use the key "y i" to refer to community i
    in snapshot y.

    INPUT:

    - Sindex -- a list of all the snapshot indices.
    - comdir -- directory where communities are stored.
    """
    # Dictionary of all communities.  Each key is a string of the form "y i".
    # The value corresponding to the key is the set of all nodes belonging to
    # community "y i".
    D = dict()
    for si in Sindex:
        i = 0
        fname = os.path.join(comdir, "comm-%d.dat" % si)
        if not os.path.isfile(fname):
            continue
        f = open(fname, "r")
        ncom = int(f.readline().strip())  # first line is number of communities
        if ncom < 1:
            f.close()
            continue
        # get the set of nodes belonging to community "y i"
        for line in f:
            S = line.strip().split()
            Node = set(map(int, S))
            assert len(S) == len(Node)
            key = "%d %d" % (si, i)
            D.setdefault(key, Node)
            i += 1
        f.close()
    return D

def read_sindex(fname):
    """
    Get all the snapshot indices from the given file.

    INPUT:

    - fname -- file with all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    """
    Sindex = []
    f = open(fname, "r")
    for line in f:
        Sindex.append(int(line.strip()))
    f.close()
    return Sindex

def read_trace(tracedir, ell, T):
    """
    Read in the trace of all dynamic communities with the given lifespan.
    The trace of a dynamic community is represented as:

        y1 n1->y2 n2->y3 n3-> ... ->yk nk

    After reading in the trace, we represent it as:

        (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk)

    WARNING:  This function modifies its arguments.

    INPUT:

    - tracedir -- directory where community traces are stored.
    - ell -- the lifespan of a dynamic community.  Assumed to be > 1.
    - T -- a list with traces of dynamic communities.  The traces will be
      added to this list.
    """
    if ell < 2:
        raise ValueError("Lifespan must be > 1")
    fname = os.path.join(tracedir, "trace-%d.dat" % ell)
    f = open(fname, "r")
    ncom = int(f.readline().strip())  # how many dynamic communities
    if ncom > 0:
        for line in f:
            n = len(T)
            T.append(list())
            trace = parse_trace(line.strip())
            for i in range(len(trace) - 1):
                T[n].append((trace[i], trace[i + 1]))
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Anomaly at the level of dynamic communities.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="listing of all snapshot indices")
parser.add_argument("--comdir", metavar="path", required=True,
                    help="directory where communities are stored")
parser.add_argument("--tracedir", metavar="path", required=True,
                    help="directory where community traces are stored")
parser.add_argument("--outdir", metavar="path", required=True,
                    help="directory where we will write the entropies")
args = parser.parse_args()
# get command line arguments & sanity checks
fsindex = args.fsindex
# This should be the directory where we can find all the communities that
# were used in the matching process.  It is not necessarily the same as the
# directory storing all the communities extracted from all snapshots.
comdir = args.comdir
tracedir = args.tracedir
outdir = args.outdir

if not os.path.isdir(outdir):
    os.makedirs(outdir)
change_community_level(fsindex, comdir, tracedir, outdir)
