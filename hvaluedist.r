###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile-no-extension

# Plot of the given cumulative distribution on log-log axes.

args <- commandArgs(trailingOnly=TRUE)
infile <- paste(args[1],".csv", sep="")

dist <- read.csv(file=infile, header=TRUE, sep=",")
png(paste(args[1],".png", sep=""))
plot(dist, log="xy")
dev.off()
