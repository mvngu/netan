###########################################################################
# Copyright (c) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Insert missing p-values.  We use the tool plfit to test the hypothesis that
# a sequence follows a power-law.  Sometimes plfit is unable to handle a
# given sequence and it would abort without providing any p-values at all.
# For any sequences that plfit cannot handle, we assume that the sequence
# does not follow a power-law and set the p-value to zero.

import argparse

###################################
# script starts here
###################################

# setup parser for command line options
s = "Insert missing p-values.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fname", metavar="file", required=True,
                    help="file containing the p-values")
args = parser.parse_args()

# get the command line arguments
fname = args.fname

Pval = dict()
# All the possible combinations of parameter values for which we want to
# test the hypothesis of power-law.
for smin in range(1, 21):
    for theta in (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0):
        for delta in range(4):
            s = "%d,%s,%d" % (smin, str(theta), delta)
            Pval.setdefault(s, False)  # we don't have p-values yet
# read in all the parameter values for which we have p-values
f = open(fname, "r")
for line in f:
    dat = line.strip().split(",")
    s = ",".join(dat[:-1])
    p = dat[-1]
    Pval[s] = p
f.close()
# now insert missing p-values
f = open(fname, "w")
f.write("smin,theta,delta,pvalue\n")
for smin in range(1, 21):
    for theta in (0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0):
        for delta in range(4):
            s = "%d,%s,%d" % (smin, str(theta), delta)
            if Pval[s] == False:
                f.write("%s,%s\n" % (s, "0.00000"))
            else:
                f.write("%s,%s\n" % (s, Pval[s]))
f.close()
