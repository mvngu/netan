#!/usr/bin/env python

import os

def comsize(year, comid, comdir):
    """
    The size of the community in the given year and having the given ID.

    INPUT:

    - year -- the snapshot year in which the community can be found.
    - comid -- the ID of the community.
    - comdir -- the path under which the list of communities are stored.
    """
    yr = int(year)
    fname = os.path.join(comdir, "comm-%d.dat" % yr)
    if not os.path.isfile(fname):
        raise IOError("File not found: %s" % fname)
    f = open(fname, "r")
    ncom = f.readline().strip()  # first line is # communities in given year
    ncom = int(ncom)
    cid = int(comid)
    if cid >= ncom:
        f.close()
        raise ValueError("Community not found: %d %d" % (yr, cid))
    i = -1
    size = None
    for line in f:
        i += 1
        if i == cid:
            Com = line.strip().split()
            size = len(Com)
            break
    f.close()
    if size is None:
        raise ValueError("Expected the size of a community")
    return size

def newborn(size, ell, comdir, tracedir):
    """
    The ID of a newborn dynamic community with the given size and eventual
    lifespan.  Return None if no such newborn is found.

    INPUT:

    - size -- the size of the newborn dynamic community.
    - ell -- the eventual lifespan of the dynamic community.
    - comdir -- path under which the list of communities are stored.
    - tracedir -- path under which the history of each dynamic community is
      stored.
    - maxell -- the maximum lifespan of any dynamic community.
    """
    fname = os.path.join(tracedir, "trace-%d.dat" % ell)
    if not os.path.isfile(fname):
        raise IOError("File not found: %s" % fname)
    f = open(fname, "r")
    f.readline()  # ignore first line, which is # dynamic communities
    yr = None
    cid = None
    did = -1  # ID of dynamic community in given year, with given lifespan
    for line in f:
        did += 1
        nwbrn = parse_trace(line.strip())[0]
        year, comid = nwbrn.split()
        s = comsize(year, comid, comdir)
        if size == s:
            yr = int(year)
            cid = int(comid)
            break
    f.close()
    if (yr is None) or (cid is None):
        return None
    return (did, yr, cid)

# This function is stolen from entropy.py
def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline a dynamic
    community.  Each step community is represented as

        year com

    where "year" refers to the snapshot year and "com" refers to the index
    of the community in the given year.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    return trace.split("->")

def time_series(did, lifespan, size, year, entropydir):
    """
    Represent the sequence of Jaccard coefficients of a dynamic community
    in the form of a time series.

    INPUT:

    - did -- the ID of the dynamic community.  This ID is specific to the list
      of dynamic communities with a given lifespan.
    - lifespan -- the lifespan of the dynamic community.
    - year -- the year in which the dynamic community is born.
    - size -- the size of the newborn dynamic community
    - entropydir -- the path under which files with records of Jaccard
      coefficients are stored.
    """
    fname = os.path.join(entropydir, "entropy-%d.dat" % lifespan)
    f = open(fname, "r")
    i = -1
    for line in f:
        i += 1
        if did == i:
            H, S = line.strip().split(";")
            print "%d;%d;%d;%d;%s" % (did, lifespan, size, year, H)
            S = S.split(",")
            Y = range(year + 1, year + len(S) + 1)
            assert(len(S) == len(Y))
            fname = os.path.join("jaccard-%d.dat" % size)
            ofile = open(fname, "w")
            ofile.write("year,Jaccard coeff\n")
            for yr, jac in zip(Y, S):
                ofile.write("%d,%s\n" % (yr, jac))
            ofile.close()
            break
    f.close()

comdir = "../gp2011/comblondel/event/smin-1/"
entropydir = "../gp2011/comblondel/event/smin-1/theta-0.3_delta-3/entropy/"
tracedir = "../gp2011/comblondel/event/smin-1/theta-0.3_delta-3/trace/"

# for size in range(201, 301):
#     for ell in range(2, 62):
#         nwbrn = newborn(size, ell, comdir, tracedir)
#         if nwbrn is None:
#             continue
#         did, yr, cid = nwbrn
#         fname = os.path.join(entropydir, "entropy-%d.dat" % ell)
#         f = open(fname, "r")
#         i = -1
#         for line in f:
#             i += 1
#             if did == i:
#                 H, S = line.strip().split(";")
#                 print "%d;%d;%d;%d %d" % (did, ell, size, yr, cid)
#                 break
#         f.close()

# Given the ID of a newborn, represent its sequence of Jaccard coefficients
# as a time series.
Did = [0, 63, 29, 5, 88, 37, 26]
Ell = [54, 7, 10, 14, 5, 11, 7]
Size = [1, 10, 20, 38, 68, 93, 108]
Year = [1958, 2005, 2002, 1998, 2007, 2001, 2005]
for d, e, s, y in zip(Did, Ell, Size, Year):
    time_series(d, e, s, y, entropydir)
