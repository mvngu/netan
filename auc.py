###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# The area under the ROC curve (AUC) for a given setting of s_min, theta,
# and delta.  We use trapezoidal integration to compute the AUC from a given
# collection of ROC points.

import argparse
import sys

###################################
# script starts here
###################################

# setup parser for command line options
s = "Area under the ROC curve.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--infile", metavar="file", required=True,
                    help="file containing confusion matrix and FPR and TPR")
args = parser.parse_args()

# get command line arguments
infile = args.infile

# read in the true positive rates and the false positive rates
f = open(infile, "r")
TPR = list()
FPR = list()
f.readline()  # ignore the first line, which is header
for line in f:
    Hthreshold, TP, FP, TN, FN, Fscore, tpr, fpr, ACC = line.strip().split(",")
    TPR.append(float(tpr))
    FPR.append(float(fpr))
f.close()
P = sorted(zip(FPR, TPR))
# use trapezoidal integration to compute the AUC
area = 0.0
for i in range(1, len(P)):
    xa, ya = P[i - 1]
    xb, yb = P[i]
    assert xa <= xb
    area += (xb - xa) * (yb + ya)
area *= 0.5
print("%.15f\n" % area)
sys.stdout.flush()
