###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# The history of change of each pair of nodes.

import argparse
import os

###################################
# helper functions
###################################

def add_neighbour(fsindex, anomdir):
    """
    The history of new neighbours for each pair of nodes.  If
    s_0, s_1, ..., s_n are the snapshot indices of a network, then we only
    consider changes from snapshot s_1 onward.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    - anomdir -- path with files on anomaly at the level of pairs of nodes.
      This is also the directory under which we write our results.
    """
    History = dict()
    SIndex = snapshot_index(fsindex)
    for i in SIndex[1:]:
        fname = os.path.join(anomdir, "snapshot-%s" % i,
                             "change-%s-plus.dat" % i)
        f = open(fname, "r")
        f.readline()  # ignore first line, which is file header
        for line in f:
            nodepair, tchange, _ = line.strip().split(":")
            nodepair = tuple(map(int, nodepair.split()))
            if nodepair not in History:
                History.setdefault(nodepair, list())
            History[nodepair].append(tchange)
        f.close()
    fname = os.path.join(anomdir, "change-plus.dat")
    f = open(fname, "w")
    f.write("node pair:history of new neighbours\n")
    for nodepair in sorted(History.keys()):
        u, v = nodepair
        s = "%d %d:" % (u, v)
        s += ",".join(History[nodepair])
        f.write("%s\n" % s)
    f.close()

def remove_neighbour(fsindex, anomdir):
    """
    The history of removal of neighbours for pair of nodes.  If
    s_0, s_1, ..., s_n are the snapshot indices of a network, then we only
    consider changes from snapshot s_1 onward.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    - anomdir -- path with files on anomaly at the level of pairs of nodes.
      This is also the directory under which we write our results.
    """
    History = dict()
    SIndex = snapshot_index(fsindex)
    for i in SIndex[1:]:
        fname = os.path.join(anomdir, "snapshot-%s" % i,
                             "change-%s-minus.dat" % i)
        f = open(fname, "r")
        f.readline()  # ignore first line, which is file header
        for line in f:
            nodepair, tchange, _ = line.strip().split(":")
            nodepair = tuple(map(int, nodepair.split()))
            if nodepair not in History:
                History.setdefault(nodepair, list())
            History[nodepair].append(tchange)
        f.close()
    fname = os.path.join(anomdir, "change-minus.dat")
    f = open(fname, "w")
    f.write("node:history of removal of neighbours\n")
    for nodepair in sorted(History.keys()):
        u, v = nodepair
        s = "%d %d:" % (u, v)
        s += ",".join(History[nodepair])
        f.write("%s\n" % s)
    f.close()

def snapshot_index(fsindex):
    """
    All the snapshot indices of a network.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    """
    SIndex = list()
    f = open(fsindex, "r")
    for line in f:
        SIndex.append(line.strip())
    f.close()
    return SIndex

def total_change(fsindex, anomdir):
    """
    The history of total changes for each pair of nodes.  If
    s_0, s_1, ..., s_n are the snapshot indices of a network, then we only
    consider changes from snapshot s_1 onward.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    - anomdir -- path with files on anomaly at the level of pairs of nodes.
      This is also the directory under which we write our results.
    """
    History = dict()
    SIndex = snapshot_index(fsindex)
    for i in SIndex[1:]:
        fname = os.path.join(anomdir, "snapshot-%s" % i,
                             "change-%s-all.dat" % i)
        f = open(fname, "r")
        f.readline()  # ignore first line, which is file header
        for line in f:
            nodepair, tchange = line.strip().split(",")
            nodepair = tuple(map(int, nodepair.split()))
            if nodepair not in History:
                History.setdefault(nodepair, list())
            History[nodepair].append(tchange)
        f.close()
    fname = os.path.join(anomdir, "change-all.dat")
    f = open(fname, "w")
    f.write("node pair:history of change\n")
    for nodepair in sorted(History.keys()):
        u, v = nodepair
        s = "%d %d:" % (u, v)
        s += ",".join(History[nodepair])
        f.write("%s\n" % s)
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "The history of change of each pair of nodes.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="file containing all snapshot indices")
parser.add_argument("--anomdir", metavar="path", required=True,
                  help="path with files on anomaly at the level of node pairs")
args = parser.parse_args()

# get the command line arguments
fsindex = args.fsindex
anomdir = args.anomdir

total_change(fsindex, anomdir)
add_neighbour(fsindex, anomdir)
remove_neighbour(fsindex, anomdir)
