###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This is a C extension for Python.
#
# Modelling the evolution of interaction density among dynamic communities.

# third party library
from densitydycom import build_dycom_network
# standard library
import argparse

###################################
# script starts here
###################################

# setup parser for command line options
s = "Evolution of interaction density among dynamic communities.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--niter", metavar="integer", required=True, type=int,
                    help="number of iterations, also the number of time steps")
parser.add_argument("--lmbd", metavar="float", required=True, type=float,
                    help="parameter for the exponential distribution, lambda")
parser.add_argument("--prob", metavar="float", required=True, type=float,
                    help="probability of merge or split")
parser.add_argument("--ncom", metavar="integer", required=True, type=int,
                    help="initial number of step communities")
parser.add_argument("--outfile", metavar="path", required=True,
                    help="write the edge list to this file")
args = parser.parse_args()

# get command line arguments & sanity checks
niter = args.niter
lmbd = args.lmbd
prob = args.prob
ncom = args.ncom
outfile = args.outfile
if niter < 1:
    raise ValueError("Number of time steps must be > 0")
if lmbd < 0.0:
    raise ValueError("Parameter lambda must be nonnegative")
if prob < 0.0:
    raise ValueError("Probability must be >= 0.0")
if ncom < 1:
    raise ValueError("Initial pool of step communities must be > 0")

build_dycom_network(niter, lmbd, prob, ncom, outfile)
