/**************************************************************************
 * Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <ctype.h>
#include <igraph.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>  /* access, F_OK */

/* The degree distribution and degree sequence of a graph.  Let G be the graph
 * under consideration and assume that G is simple.  Let m be the maximum
 * degree of G.  For each i <- 0, 1, ..., m we count how many vertices in V(G)
 * have degree i.  This sequence is called the degree distribution of G.  Each
 * count in the distribution can be normalized by the total degree of G.  Let
 * D be a vector storing the degree distribution, where D[i] counts the number
 * of vertices having degree i.  Then the sum sum_i i*D[i] is equivalent to
 * the total degree of G, which is in turn equivalent to twice the number of
 * edges (remember that G is assumed to be a simple undirected graph).  In
 * contrast, the degree sequence of G is simply a listing of the degree of
 * each node in G.
 *
 * Usage: <program-name> file
 *
 * Only two command line arguments are expected, including the argument that
 * specifies the program name.  The above call will compute the degree
 * distribution/sequence for the given file, assumed to contain the edge list.
 */
int main(int argc,
         char **argv) {
  FILE *file;
  char *fname;
  igraph_integer_t a, b, e;  /* generic variables */
  igraph_integer_t c;        /* count # occurrences of a degree */
  igraph_integer_t i, k;     /* generic indices */
  igraph_integer_t m;        /* maximum degree of graph */
  igraph_t G;                /* network graph */
  igraph_vector_t deg;       /* degree sequence */
  igraph_vector_t dist;      /* degree distribution */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_file *elistf = arg_file0(NULL, "elistf", NULL,
                               "file containing edge list, one edge per line");
  struct arg_file *distf = arg_file0(NULL, "distf", NULL,
                             "file to which we write the degree distribution");
  struct arg_file *seqf = arg_file0(NULL, "seqf", NULL,
                                 "file to which we write the degree sequence");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, elistf, distf, seqf, end};

  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("The degree distribution and sequence.\n");
    arg_print_glossary(stdout, argtable, "  %-25s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((elistf->count < 1) || (distf->count < 1) || (seqf->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("The degree distribution and sequence.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* Construct a graph G from its edge list.  Write the degree of each */
  /* vertex to a file. */
  asprintf(&fname, "%s", elistf->filename[0]);
  /* given file doesn't exist */
  if (access(fname, F_OK) != 0) {
    printf("Error: file not found %s\n", fname);
    fflush(stdout);
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* reconstruct graph from its edge list; assume simple graph */
  file = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, file, /*nvert*/ 0, IGRAPH_UNDIRECTED);
  fclose(file);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  /* degree sequence of graph */
  igraph_vector_init(&deg, 0);
  igraph_degree(&G, &deg, igraph_vss_all(), IGRAPH_ALL,
                /*ignore self-loops*/ 0);

  /* Degree distribution of the graph G.  The value D[i] counts the */
  /* number of vertices in G having degree i.  If m is the maximum */
  /* degree of G, then the vector D has length m + 1 because we also */
  /* consider vertices with degree zero. */
  igraph_maxdegree(&G, &m, igraph_vss_all(), IGRAPH_ALL,
                   /*ignore self-loops*/ 0);
  igraph_vector_init(&dist, m + 1);  /* each element set to zero initially */
  for (i = 0; i < igraph_vector_size(&deg); i++) {
    k = (igraph_integer_t)VECTOR(deg)[i];
    VECTOR(dist)[k] += 1;
  }

  /* sanity check: sum of vector dist must equal # vertices in G */
  printf("%li\n", (long int)igraph_vector_sum(&dist));
  printf("%li\n", (long int)igraph_vcount(&G));
  /* sanity check: must have */
  /* sum_i i*dist[i] == sum_i deg[i] == 2 * #edges */
  a = 0;
  for (i = 0; i < igraph_vector_size(&dist); i++) {
    a += i * (igraph_integer_t)VECTOR(dist)[i];
  }
  b = (igraph_integer_t)igraph_vector_sum(&deg);
  e = 2 * (igraph_integer_t)igraph_ecount(&G);
  printf("%li\n", (long int)a);
  printf("%li\n", (long int)b);
  printf("%li\n", (long int)e);
  fflush(stdout);

  /* write degree sequence to file */
  igraph_vector_sort(&deg);
  asprintf(&fname, "%s", seqf->filename[0]);
  file = fopen(fname, "w");
  fprintf(file, "degree sequence\n");
  for (i = 0; i < igraph_vector_size(&deg); i++) {
    fprintf(file, "%li\n", (long int)VECTOR(deg)[i]);
  }
  fclose(file);
  free(fname);

  /* write degree distribution to file */
  asprintf(&fname, "%s", distf->filename[0]);
  file = fopen(fname, "w");
  fprintf(file, "degree,count\n");
  for (i = 0; i < igraph_vector_size(&dist); i++) {
    c = VECTOR(dist)[i];
    fprintf(file, "%li,%li\n", (long int)i, (long int)c);
  }
  fclose(file);
  free(fname);

  /* clean up */
  igraph_destroy(&G);
  igraph_vector_destroy(&deg);
  igraph_vector_destroy(&dist);

  return 0;
}
