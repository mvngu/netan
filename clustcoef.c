/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* Evolution of the clustering coefficients of a network.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <ctype.h>
#include <igraph.h>
#include <stdio.h>

void snapshot_index(const char *infile,
                    igraph_vector_t *sindex);
void write_header(const char *fname);
void write_results(const char *fname,
                   const igraph_integer_t sindex,
                   const igraph_real_t localcoef,
                   const igraph_real_t globalcoef);

/* Get all the snapshot indices from the given file.  The file is assumed to
 * contain snapshot indices, one per line.  All the snapshot indices are
 * assumed to be in temporal order, from the birth of the network to its
 * last observation.
 *
 * - infile -- file containing all the snapshot years.
 * - sindex -- vector of snapshot indices.  The result will be stored here.
 *   This should be initialized beforehand.
 */
void snapshot_index(const char *infile,
                    igraph_vector_t *sindex) {
  FILE *f;
  long int si;  /* a snapshot index */
  int c;        /* a character */

  f = fopen(infile, "r");

  /* skip all white spaces */
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);

  /* read all snapshot indices, one per line */
  while (!feof(f)) {
    fscanf(f, "%li", &si);
    igraph_vector_push_back(sindex, (igraph_integer_t)si);
    /* skip all white spaces */
    do {
      c = getc(f);
    } while (isspace(c));
    ungetc(c, f);
  }

  fclose(f);
}

/* Write header for the results file.
 *
 * - fname -- the name of the file to which we write the header.
 */
void write_header(const char *fname) {
  FILE *file;

  file = fopen(fname, "w");
  fprintf(file, "snapshot index,avg local clust coef,global clust coef\n");
  fclose(file);
}

/* Write results to a file.  Results are written according to the following
 * CSV file format:
 *
 * snapshot index,avg local clust. coefficient,global clustering coefficient
 *
 * - fname -- the file to which we write results.
 * - sindex -- the current snapshot index.
 * - localcoef -- the average local clustering coefficient.
 * - globalcoef -- the global clustering coefficient.
 */
void write_results(const char *fname,
                   const igraph_integer_t sindex,
                   const igraph_real_t localcoef,
                   const igraph_real_t globalcoef) {
  FILE *file;

  file = fopen(fname, "a");
  fprintf(file, "%li,%.10lf,%.10lf\n",
          (long int)sindex, (double)localcoef, (double)globalcoef);
  fclose(file);
}

/* Evolution of the clustering coefficients of a network.  We consider two
 * types of clustering coefficients:
 *
 * - the average local clustering coefficient, which is the average local
 *   transitivity.  This is the version of clustering coefficient considered
 *   in (Watts & Strogatz 1998).
 * - the global clustering coefficient, which is a single number that
 *   quantifies the clustering of a network.  This is more commonly known as
 *   the transitivity of a graph, as defined in (Wasserman & Faust 1994).
 */
int main(int argc,
         char **argv) {
  FILE *infile;            /* file to read from */
  char *infname;
  igraph_vector_t sindex;  /* all the snapshot indices */
  igraph_t G;              /* a graph */
  igraph_integer_t i;      /* generic index */
  igraph_real_t clocal;    /* average local clustering coefficient */
  igraph_real_t cglobal;   /* global clustering coefficient */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_file *findex = arg_file0(NULL, "file", NULL,
                         "file containing all snapshot indices, one per line");
  struct arg_str *edgelistdir = arg_strn(NULL, "edgelistdir", "path", 0,
                                    argc + 2,
                                    "directory from which to read edge lists");
  struct arg_file *fstats = arg_file0(NULL, "outfile", NULL,
                                      "file to which we write statistics");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, findex, edgelistdir, fstats, end};

  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Evolution of the clustering coefficients of a network.\n");
    arg_print_glossary(stdout, argtable, "  %-25s %s\n");
    /* deallocate each non-null entry in argtable[] */
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((findex->count < 1)
      || (edgelistdir->count < 1)
      || (fstats->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Evolution of the clustering coefficients of a network.\n");
    arg_print_glossary(stdout, argtable, "  %-25s %s\n");
    /* deallocate each non-null entry in argtable[] */
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  write_header(fstats->filename[0]);
  igraph_vector_init(&sindex, 0);
  snapshot_index(findex->filename[0], &sindex);

  /* iterate over each snapshot and get clustering coefficients */
  for (i = 0; i < igraph_vector_size(&sindex); i++) {
    printf("%li\n", (long int)VECTOR(sindex)[i]);
    fflush(stdout);
    /* Construct the snapshot graph from its edge list.  The graph is */
    /* a simple undirected graph, i.e. no self-loops nor multiple edges. */
    asprintf(&infname, "%s/edgelist-%li.dat", edgelistdir->sval[0],
             (long int)VECTOR(sindex)[i]);
    infile = fopen(infname, "r");
    igraph_read_graph_edgelist(&G, infile, 0, IGRAPH_UNDIRECTED);
    fclose(infile);
    free(infname);
    igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                    /*edge_comb*/ 0);

    /* Average local clustering coefficient following */
    /* (Watts & Strogatz 1998).  For a vertex with degree < 2, we set its */
    /* average local clustering coefficient to zero. */
    igraph_transitivity_avglocal_undirected(&G, &clocal,
                                            IGRAPH_TRANSITIVITY_ZERO);
    /* The global clustering coefficient as defined in */
    /* (Wasserman & Faust 1994).  If a graph has no connected triples */
    /* (i.e. triangles), we set its global clustering coefficient to zero. */
    igraph_transitivity_undirected(&G, &cglobal, IGRAPH_TRANSITIVITY_ZERO);

    /* output results to a file */
    write_results(fstats->filename[0], VECTOR(sindex)[i], clocal, cglobal);

    /* clean up */
    igraph_destroy(&G);
  }

  igraph_vector_destroy(&sindex);
  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return EXIT_SUCCESS;
}
