###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Convert data in a CSV file to coordinates suitable for LaTeX'ing using
# pgfplots.

import argparse

# setup parser for command line options
s = "Convert from CSV to coordinates.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--csvfile", metavar="path", required=True,
                    help="path to file with data in CSV format")
parser.add_argument("--coordfile", metavar="path", required=True,
                    help="file for writing coordinates")
args = parser.parse_args()

# get the command line arguments
csvfile = args.csvfile
coordfile = args.coordfile

infile = open(csvfile, "r")
outfile = open(coordfile, "w")
infile.readline()  # ignore first line, which is header
for line in infile:
    x, y = line.strip().split(",")
    outfile.write("(%s, %s)\n" % (x, y))
infile.close()
outfile.close()
