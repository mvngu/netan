#!/usr/bin/env bash

###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Trace dynamic communities.

Python=/scratch/mvngu/usr/bin/python
Sage=/dev/shm/mvngu/sage-4.8/sage
delta=3    # no. allowable missing observations
theta=0.3  # match threshold
gamma=0.1  # growth threshold
kappa=0.1  # contraction threshold
sigma=0.1  # laggard threshold

# Extract all communities with a given minimum size.
for com in comblondel comraghavan; do
    for smin in {1..50}; do
        nice -n 19 ionice -c 2 -n 7 "$Python" ../../netan/minsize.py \
            --smin "$smin" \
            --year ../yearly/year.dat \
            --comdir ../"$com"/ \
            --outdir ../"$com"/event \
            2>&1 | tee -a ../log.minsize
    done
done

# Match all step communities with a given minimum size, across all available
# snapshots. This should generate a collection of match data.
for com in comblondel comraghavan; do
    for smin in {1..50}; do
        nice -n 19 ionice -c 2 -n 7 "$Python" ../../netan/match.py \
            --smin "$smin" \
            --theta "$theta" \
            --delta "$delta" \
            --year ../yearly/year.dat \
            --eventdir ../"$com"/event \
            2>&1 | tee -a ../log.match
    done
done

# From the match data, get all events in a life-cycle model, but exclude the
# death event.
for com in comblondel comraghavan; do
    for smin in {1..50}; do
        nice -n 19 ionice -c 2 -n 7 "$Python" ../../netan/event.py \
            --deathonly false \
            --smin "$smin" \
            --theta "$theta" \
            --delta "$delta" \
            --gamma "$gamma" \
            --kappa "$kappa" \
            --year ../yearly/year.dat \
            --eventdir ../"$com"/event \
            2>&1 | tee -a ../log.keyevent
    done
done

# Obtain the trace of the lifespan of each dynamic community.
for com in comblondel comraghavan; do
    for smin in {1..50}; do
        nice -n 19 ionice -c 2 -n 7 "$Sage" -python ../../netan/tracecomm.py \
            --smin "$smin" \
            --theta "$theta" \
            --delta "$delta" \
            --year ../yearly/year.dat \
            --eventdir ../"$com"/event \
            2>&1 | tee -a ../log.tracecomm
    done
done

# From the match data and the trace of lifespan, get all deaths.
for com in comblondel comraghavan; do
    for smin in {1..50}; do
        nice -n 19 ionice -c 2 -n 7 "$Python" ../../netan/event.py \
            --deathonly true \
            --smin "$smin" \
            --theta "$theta" \
            --delta "$delta" \
            --gamma "$gamma" \
            --kappa "$kappa" \
            --year ../yearly/year.dat \
            --eventdir ../"$com"/event \
            2>&1 | tee -a ../log.keyevent
    done
done

# Compute the H-value of each dynamic community.
for com in comblondel comraghavan; do
    for smin in {1..50}; do
        prefix=../"$com"/event/smin-"$smin"/theta-"$theta"_delta-"$delta"
        mkdir -p "$prefix"/entropy
        nice -n 19 ionice -c 2 -n 7 "$Sage" -python ../../netan/entropy.py \
            --year ../yearly/year.dat \
            --matchdir "$prefix" \
            --tracedir "$prefix"/trace \
            --outdir "$prefix"/entropy \
            2>&1 | tee -a ../log.entropy
    done
done

# Detect special types of dynamic communities.
for com in comblondel comraghavan; do
    for smin in {1..50}; do
        prefix=../"$com"/event/smin-"$smin"/theta-"$theta"_delta-"$delta"
        mkdir -p "$prefix"/comtype
        mkdir -p "$prefix"/pchange
        nice -n 19 ionice -c 2 -n 7 "$Sage" -python ../../netan/comtype.py \
            --year ../yearly/year.dat \
            --matchdir "$prefix" \
            --tracedir "$prefix"/trace \
            --delta "$delta" \
            --sigma "$sigma" \
            --comtypedir "$prefix"/comtype \
            --pchangedir "$prefix"/pchange \
            2>&1 | tee -a ../log.comtype
    done
done
