###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Predict that nodes will join or leave.  We are essentially doing a binary
# classification, where the results are either classified as positive or
# negative.  The two types of results can induce four possible types of
# outcomes.
#
# * true positive -- the outcome is positive and the actual value is positive.
# * false positive -- the outcome is positive and the actual value is negative.
# * true negative -- the outcome is negative and the actual value is negative.
# * false negative -- the outcome is negative and the actual value is positive.
#
# If si is the snapshot of interest, then we will only consider all temporal
# groups that exist in that snapshot.

import argparse
import math
import os

###################################
# helper functions
###################################

def change_community_level(fsindex, nnext, lastsi, hval, onlyHthreshold,
                           comdir, tracedir, outdir):
    """
    Change at the level of dynamic communities.

    INPUT:

    - fsindex -- a file listing all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    - nnext -- how far ahead do we want to predict.
    - lastsi -- the last snapshot index to consider.  This is the latest we
      will consider.  We do not consider any data beyond this snapshot index.
      All temporal groups to be used for prediction must have a snapshot in
      the given snapshot index.
    - hval -- the H value to be used as a threshold for prediction.
    - onlyHthreshold -- boolean; True if we want to only use the threshold on
      the H value in making a prediction; False otherwise.
    - comdir -- directory where communities are stored.
    - tracedir -- directory where community traces are stored.
    - outdir -- directory where we will write data on anomaly.
    """
    Hmin = float(hval)
    Sindex = read_sindex(fsindex)
    Com = read_community(Sindex, comdir)
    T = list()
    for ell in range(2, len(Sindex) + 1):
        read_trace(tracedir, ell, T)
    # confusion matrix for predicting that nodes will leave a temporal group
    MinTruePositive = 0
    MinTrueNegative = 0
    MinFalsePositive = 0
    MinFalseNegative = 0
    # confusion matrix for predicting that nodes will join a temporal group
    PlsTruePositive = 0
    PlsTrueNegative = 0
    PlsFalsePositive = 0
    PlsFalseNegative = 0
    dthreshold = 0
    ntgroup = 0
    for i in range(len(T)):
        trace = T[i]
        # We only consider temporal groups with snapshots in the given
        # snapshot index.
        if not has_snapshot(lastsi, trace):
            continue
        ntgroup += 1

        # predict that members will join
        History, delta, deltanext = history_new_members(
            Com, trace, lastsi, nnext)
        H = entropy(History)
        prediction = False  # default; predict that nodes will not join
        actual = False      # default; actually, no nodes will join
        # make a prediction
        if H >= Hmin:
            if onlyHthreshold:
                prediction = True  # predict that nodes will join
            elif delta > dthreshold:
                prediction = True  # predict that nodes will join
        # verify our prediction
        if deltanext > dthreshold:
            actual = True          # nodes actually will join
        # update the confusion matrix
        if prediction:
            if actual:
                PlsTruePositive += 1
            else:
                PlsFalsePositive += 1
        else:
            if actual:
                PlsFalseNegative += 1
            else:
                PlsTrueNegative += 1

        # predict that members will leave
        History, delta, deltanext = history_members_depart(
            Com, trace, lastsi, nnext)
        H = entropy(History)
        prediction = False  # default; predict that nodes will not leave
        actual = False      # default; actually, no nodes will leave
        # make a prediction
        if H >= Hmin:
            if delta > dthreshold:
                prediction = True  # predict that nodes will leave
        # verify our prediction
        if deltanext > dthreshold:
            actual = True          # nodes actually will leave
        # update the confusion matrix
        if prediction:
            if actual:
                MinTruePositive += 1
            else:
                MinFalsePositive += 1
        else:
            if actual:
                MinFalseNegative += 1
            else:
                MinTrueNegative += 1
    s = PlsTruePositive + PlsFalsePositive + PlsFalseNegative + PlsTrueNegative
    assert ntgroup == s
    s = MinTruePositive + MinFalsePositive + MinFalseNegative + MinTrueNegative
    assert ntgroup == s

    # Write results to files.  If a file exists, just append results to it.
    # The file format is as follows:
    #
    # * the value of the H threshold
    # * the number of true positives
    # * the number of false positives
    # * the number of true negatives
    # * the number of false negatives
    header = "H threshold,true positive,false positive,true negative,false negative\n"
    # members depart
    fname = os.path.join(outdir, "minus-%d.dat" % nnext)
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write(header)
        f.close()
    f = open(fname, "a")
    f.write("%s,%d,%d,%d,%d\n" % (
            hval, MinTruePositive, MinFalsePositive, MinTrueNegative,
            MinFalseNegative))
    f.close()
    # members joining
    fname = os.path.join(outdir, "plus-%d.dat" % nnext)
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write(header)
        f.close()
    f = open(fname, "a")
    f.write("%s,%d,%d,%d,%d\n" % (
            hval, PlsTruePositive, PlsFalsePositive, PlsTrueNegative,
            PlsFalseNegative))
    f.close()

def entropy(History):
    """
    The entropy of the history of some type of changes.

    INPUT:

    - History -- a list giving the sequence of changes with respect to some
      event.  For example, suppose we are interested in the number of new
      nodes joining a dynamic community.  Then History[i] represents the
      required count in snapshot i.
    """
    S = float(math.fsum(History))
    if S == 0.0:
        return 0.0
    assert S > 0.0
    P = [e / S for e in History]
    H = list()
    for p in P:
        if p == 0.0:
            H.append(0.0)
        else:
            H.append(p * math.log(p))  # natural logarithm
    H = -1.0 * math.fsum(H)
    if H == 0.0:
        H = 0.0
    return H

def has_snapshot(si, trace):
    """
    Whether a temporal group has a snapshot in the given snapshot index.

    INPUT:

    - si -- a snapshot index.
    - trace -- the trace of a temporal group.  The trace is represented as
      (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk).  We use the
      notation "y n" to refer to community n in snapshot index y.
    """
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        if (sia == si) or (sib == si):
            return True
    return False

def history_members_depart(Com, trace, lastsi, nnext):
    """
    The history of members departing the given dynamic community D.  Let i and
    j be consecutive snapshots, where i < j.  Let N_i and N_j be the set of
    nodes at snapshots i and j, respectively.  All the nodes from N_i that
    leave N_j can be obtained by the set difference N_i - N_j.  That is, we
    consider all nodes that are in N_i but not in N_j.

    INPUT:

    - Com -- a dictionary of all communities in all snapshots.  We use the key
      "y i" to refer to community i in snapshot y.
    - trace -- the trace of a dynamic community.  The trace is represented as
      (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk).  Each 2-tuple
      is considered for departing members.
    - lastsi -- the last snapshot index to consider.  This is the latest we
      will consider.  We do not consider any data beyond this snapshot index.
      We assume that the given temporal group has a snapshot in this
      snapshot index.
    - nnext -- how far ahead do we want to predict.
    """
    # the history to be used for prediction
    History = list()
    m = 0  # no. snapshots with missing observations
    d = 0  # how many nodes leave the dynamic community
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index?
        if sia >= lastsi:
            break
        assert sia < lastsi
        assert sib <= lastsi
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        d = len(A.difference(B))
        History.append(d)
        if (sia == sib) and (sia + m == lastsi):
            break
    delta = int(d)

    # the history to be used for verification
    # the temporal group dies off before nnext snapshots ahead
    if not has_snapshot(lastsi + nnext, trace):
        return History, delta, 0
    # the temporal group is still alive nnext snapshots ahead
    m = 0  # no. snapshots with missing observations
    d = 0  # how many nodes leave the dynamic community
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index + nnext?
        if sia >= lastsi + nnext:
            break
        assert sia < lastsi + nnext
        assert sib <= lastsi + nnext
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        d = len(A.difference(B))
        if (sia == sib) and (sia + m == lastsi + nnext):
            break
    deltanext = int(d)

    return History, delta, deltanext

def history_new_members(Com, trace, lastsi, nnext):
    """
    The history of new members in a given dynamic community D.  Let i and j be
    consecutive snapshots, where i < j.  Let N_i and N_j be the set of nodes
    at snapshots i and j, respectively.  All the new nodes that join the
    network at snapshot j can be obtained by the set difference N_j - N_i.
    That is, we consider all nodes that are in N_j but not in N_i.

    INPUT:

    - Com -- a dictionary of all communities in all snapshots.  We use the key
      "y i" to refer to community i in snapshot y.
    - trace -- the trace of a dynamic community.  The trace is represented as
      (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk).  Each 2-tuple
      is considered for new members.
    - lastsi -- the last snapshot index to consider.  This is the latest we
      will consider.  We do not consider any data beyond this snapshot index.
      We assume that the given temporal group has a snapshot in this
      snapshot index.
    - nnext -- how far ahead do we want to predict.
    """
    # the history to be used for prediction
    # History of members joining.  We have no history if the temporal group
    # is born on the snapshot index we want.
    History = list()
    m = 0  # no. snapshots with missing observations
    d = 0  # how many nodes join the dynamic community
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index?
        if sia >= lastsi:
            break
        assert sia < lastsi
        assert sib <= lastsi
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        d = len(B.difference(A))
        History.append(d)
        if (sia == sib) and (sia + m == lastsi):
            break
    delta = int(d)

    # the history to be used for verification
    # the temporal group dies off before nnext snapshots ahead
    if not has_snapshot(lastsi + nnext, trace):
        return History, delta, 0
    # the temporal group is still alive nnext snapshots ahead
    m = 0  # no. snapshots with missing observations
    d = 0  # how many nodes join the temporal group
    for a, b in trace:
        sia, _ = map(int, a.split())
        sib, _ = map(int, b.split())
        # have we processed changes for the latest snapshot index + nnext?
        if sia >= lastsi + nnext:
            break
        assert sia < lastsi + nnext
        assert sib <= lastsi + nnext
        # are we missing observations?
        if sia == sib:
            m += 1
        else:
            m = 0
        # get the required changes
        A = Com[a]
        B = Com[b]
        d = len(B.difference(A))
        if (sia == sib) and (sia + m == lastsi + nnext):
            break
    deltanext = int(d)

    return History, delta, deltanext

def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline of a dynamic
    community.  Each step community is represented as

        si com

    where "si" refers to the snapshot index and "com" refers to the index
    of the community in the given snapshot.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    T = trace.split("->")
    # transform the pattern "si1 com1,n" into "si1 com1"
    for i in range(len(T)):
        if "," in T[i]:
            sicom, _ = T[i].split(",")
            T[i] = sicom
    return T

def read_community(Sindex, comdir):
    """
    Read in all the communities in each network snapshot.  For a network
    snapshot y, each community in the snapshot is assigned a unique
    nonnegative integer, starting from zero.  Let i be the unique label
    assigned to some community.  We use the key "y i" to refer to community i
    in snapshot y.

    INPUT:

    - Sindex -- a list of all the snapshot indices.
    - comdir -- directory where communities are stored.
    """
    # Dictionary of all communities.  Each key is a string of the form "y i".
    # The value corresponding to the key is the set of all nodes belonging to
    # community "y i".
    D = dict()
    for si in Sindex:
        i = 0
        fname = os.path.join(comdir, "comm-%d.dat" % si)
        if not os.path.isfile(fname):
            continue
        f = open(fname, "r")
        ncom = int(f.readline().strip())  # first line is number of communities
        if ncom < 1:
            f.close()
            continue
        # get the set of nodes belonging to community "y i"
        for line in f:
            S = line.strip().split()
            Node = set(map(int, S))
            assert len(S) == len(Node)
            key = "%d %d" % (si, i)
            D.setdefault(key, Node)
            i += 1
        f.close()
    return D

def read_sindex(fname):
    """
    Get all the snapshot indices from the given file.

    INPUT:

    - fname -- file with all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    """
    Sindex = []
    f = open(fname, "r")
    for line in f:
        Sindex.append(int(line.strip()))
    f.close()
    return Sindex

def read_trace(tracedir, ell, T):
    """
    Read in the trace of all dynamic communities with the given lifespan.
    The trace of a dynamic community is represented as:

        y1 n1->y2 n2->y3 n3-> ... ->yk nk

    After reading in the trace, we represent it as:

        (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk)

    WARNING:  This function modifies its arguments.

    INPUT:

    - tracedir -- directory where community traces are stored.
    - ell -- the lifespan of a dynamic community.  Assumed to be > 1.
    - T -- a list with traces of dynamic communities.  The traces will be
      added to this list.
    """
    if ell < 2:
        raise ValueError("Lifespan must be > 1")
    fname = os.path.join(tracedir, "trace-%d.dat" % ell)
    f = open(fname, "r")
    ncom = int(f.readline().strip())  # how many dynamic communities
    if ncom > 0:
        for line in f:
            n = len(T)
            T.append(list())
            trace = parse_trace(line.strip())
            for i in range(len(trace) - 1):
                T[n].append((trace[i], trace[i + 1]))
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Predict that nodes will join or leave.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="listing of all snapshot indices")
parser.add_argument("--nnext", metavar="integer", required=True, type=int,
                    help="how far ahead we want to predict")
parser.add_argument("--lastsi", metavar="integer", required=True, type=int,
                    help="the last snapshot index to consider")
parser.add_argument("--h", metavar="string", required=True,
                    help="the H threshold for prediction")
parser.add_argument("--onlyHthreshold", metavar="boolean", required=True,
             help="whether to only use the H threshold in making a prediction")
parser.add_argument("--comdir", metavar="path", required=True,
                    help="directory where communities are stored")
parser.add_argument("--tracedir", metavar="path", required=True,
                    help="directory where community traces are stored")
parser.add_argument("--outdir", metavar="path", required=True,
                    help="directory where we will write data on anomaly")
args = parser.parse_args()
# get command line arguments & sanity checks
fsindex = args.fsindex
# How far ahead we want to predict.  This might be one snapshot ahead, two
# snapshots ahead, and so on.
nnext = args.nnext
# The last snapshot index to consider.  This is the latest we will consider.
# We do not consider any data beyond this snapshot index.
lastsi = args.lastsi
# the H value will be used to predict what would happen in the next snapshot
hval = args.h
onlyHthreshold = args.onlyHthreshold
# This should be the directory where we can find all the communities that
# were used in the matching process.  It is not necessarily the same as the
# directory storing all the communities extracted from all snapshots.
comdir = args.comdir
tracedir = args.tracedir
outdir = args.outdir

if not os.path.isdir(outdir):
    os.makedirs(outdir)
if onlyHthreshold in ("True", "true"):
    onlyHthreshold = True
elif onlyHthreshold in ("False", "false"):
    onlyHthreshold = False
else:
    raise ValueError("onlyHthreshold must be a boolean")
change_community_level(fsindex, nnext, lastsi, hval, onlyHthreshold, comdir,
                       tracedir, outdir)
