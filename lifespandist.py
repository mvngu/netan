###########################################################################
# Copyright (c) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

import os
import sys

# NOTE: This script could be run from Python.
#
# README
# Distribution and sequence of lifespan of dynamic community.  Use the script
# below to obtain lifespan distribution and sequence of all trace records.
#
## #!/usr/bin/env bash
##
## theta=0.3
## for ed in editors noeditors; do
##     for comm in BlondelEtAl2008 ClausetEtAl2004 RaghavanEtAl2007; do
## 	for size in {1..10}; do
## 	    for delta in {0..3}; do
## 		for len in {1..62}; do
## 		    echo "$path $size $theta $delta $len" 2>&1 | tee -a go.log
## 		    path="$ed"/snapshot/comm_"$comm"/event
## 		    python commlen_dist.py "$path" "$size" "$theta" "$delta" "$len" 2>&1 | tee -a go.log
## 		done
## 	    done
## 	done
##     done
## done


###################################
# helper functions
###################################

def length_distribution(dirname, maxlen):
    """
    Construct a dynamic community length distribution from the trace files
    under the given directory.  This distribution is analogous to the degree
    distribution.  That is, for each dynamic community length from 1 up to and
    including the given maximum length, we count the number of occurrences of
    this length in the trace files.

    INPUT:

    - dirname -- name of the directory under which traces of dynamic
      communities are stored.
    - maxlen -- the maximum length of any dynamic community.
    """
    # Construct length distribution.  Index is length, value is count.  As
    # lists are indexed from zero, we also include the count for length zero.
    # The maximum dynamic community length is len(L) - 1.
    L = []
    L.append(0)
    for i in range(1, maxlen + 1):
        fname = os.path.join(dirname, "trace-%d.dat" % i)
        f = open(fname, "r")
        L.append(int(f.readline().strip()))  # first line stores count
        f.close()
    return L

def length_sequence(dirname, maxlen):
    """
    Construct a dynamic community length sequence from the trace files
    under the given directory.  This sequence is analogous to the degree
    sequence.

    INPUT:

    - dirname -- name of the directory under which traces of dynamic
      communities are stored.
    - maxlen -- the maximum length of any dynamic community.
    """
    # Construct length sequence.  Index is ID of dynamic community; value is
    # the length of that dynamic community.  In fact, the IDs of dynamic
    # communities are not important for our purpose.  We only want the
    # length sequence and disregard the IDs.
    L = []
    for i in range(1, maxlen + 1):
        fname = os.path.join(dirname, "trace-%d.dat" % i)
        f = open(fname, "r")
        ncomm = int(f.readline().strip())  # first line stores count
        f.close()
        # i is length
        # append as many i as there are communities of this length
        for _ in range(ncomm):
            L.append(i)
    return L

def usage():
    """
    Print the usage information for this script.
    """
    s = "Usage: %s path smin theta delta maxlen\n" % sys.argv[0]
    s += "  - path -- path to where files with traces of dynamic communities\n"
    s += "    live.  Results are also written to files under this path.\n"
    s += "  - smin -- the minimum community size.\n"
    s += "  - theta -- the similarity threshold.\n"
    s += "  - delta -- maximum allowable number of missing observations.\n"
    s += "  - maxlen -- the maximum length of any dynamic community."
    print(s)
    sys.stdout.flush()

def write_distribution(D, fname, header):
    """
    Write the given dynamic community length distribution to file.  The
    resulting file is a CSV file.

    INPUT:

    - D -- a dynamic community length distribution implemented as a list,
      where index/value is length/count.  As lists are indexed from zero, D
      also includes the count for length zero.  The maximum length is
      len(D) - 1.
    - fname -- name of the file to which we write the length distribution.
    - header -- the header of the resulting CSV file.
    """
    f = open(fname, "w")
    f.write(header + "\n")
    for length in range(1, len(D)):
        f.write(str(length) + "," + str(D[length]) + "\n")
    f.close()

def write_sequence(S, fname, header=None):
    """
    Write the given dynamic community length sequence to file.  The
    resulting file is a CSV file.

    INPUT:

    - S -- a dynamic community length sequence implemented as a list, where
      S[i] is the length of some dynamic community.
    - fname -- name of the file to which we write the length sequence.
    - header -- (default: None) the header of the resulting CSV file.
    """
    f = open(fname, "w")
    if header is not None:
        f.write(header + "\n")
    for length in S:
        f.write(str(length) + "\n")
    f.close()

###################################
# script starts here
###################################

# sanity checks
narg = 6
if len(sys.argv) != narg:
    usage()
    exit(1)
prefix = sys.argv[1]
if not os.path.isdir(prefix):
    usage()
    exit(1)
smin = sys.argv[2]
theta = sys.argv[3]
delta = sys.argv[4]
maxlen = sys.argv[5]
dirname = os.path.join(prefix, "smin-%s" % smin,
                     "theta-%s_delta-%s" % (theta, delta), "trace")
if not os.path.isdir(dirname):
    print("Directory not found: %s" % dirname)
    exit(1)

# get length distributions and write to file
Dist = length_distribution(dirname, int(maxlen))
fname = os.path.join(dirname, "lifespan-dist.csv")
write_distribution(Dist, fname, "exact length,count")

# get length sequence and write to file
Seq = length_sequence(dirname, int(maxlen))
fname = os.path.join(dirname, "lifespan-seq.dat")
write_sequence(Seq, fname)
