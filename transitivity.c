/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* The clustering coefficient of the network of dynamic communities.  We
 * consider the following type of clustering coefficient.
 *
 * - The clustering coefficient in (Watts & Strogatz 1998).  This is also
 *   called the average local transitivity.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <igraph.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void clustering_cefficient(const int id,
                           const igraph_t *G,
                           FILE *f);

/* The average clustering coefficient of (Watts & Strogatz 1998).  If a node
 * has < 2 neighbours, then we set its clustering coefficient to zero.
 *
 * INPUT:
 *
 * - id -- the ID of the sampled network.
 * - G -- a graph representation of community matches.  This should be an
 *   undirected graph.
 * - f -- append results to this file.
 */
void clustering_cefficient(const int id,
                           const igraph_t *G,
                           FILE *f) {
  igraph_real_t ws;  /* avg clustering coefficient of Watts & Strogatz */

  igraph_transitivity_avglocal_undirected(G, &ws, IGRAPH_TRANSITIVITY_ZERO);
  fprintf(f, "%i,%.15lf\n", id, ws);
}

/* The clustering coefficients of the network of dynamic communities.
 */
int main(int argc,
         char **argv) {
  FILE *f;
  char *fname;
  int n;
  igraph_t G;

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_str *datadir = arg_strn(NULL, "datadir", "path", 0, argc + 2,
                                     "directory with sampled networks");
  struct arg_str *sampleid = arg_strn(NULL, "sampleid", "integer", 0, argc + 2,
                                      "the sample ID");
  struct arg_str *outdir = arg_strn(NULL, "outdir", "path", 0, argc + 2,
                                "write results to files under this directory");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, datadir, sampleid, outdir, end};
  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Clustering coefficients of the network of dynamic communities.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((datadir->count < 1)
      || (sampleid->count < 1)
      || (outdir->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Clustering coefficients of the network of dynamic communities.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* Construct the network from its edge list.  The graph is assumed to be */
  /* undirected and simple, i.e. no self-loops nor multiple edges. */
  n = atoi(sampleid->sval[0]);
  asprintf(&fname, "%s/dycomnet-edgelist-%i.dat", datadir->sval[0], n);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, f, 0, IGRAPH_UNDIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  /* local and global clustering coefficients */
  asprintf(&fname, "%s/transitivity.dat", outdir->sval[0]);
  f = fopen(fname, "a");
  clustering_cefficient(n, &G, f);
  fclose(f);
  free(fname);

  igraph_destroy(&G);
  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
