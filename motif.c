/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* Find all motifs of a given size.  We only consider motifs of sizes 3 and
 * 4 here.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <igraph.h>
#include <stdlib.h>
#include <string.h>

/* WARNING:  Global variable.  I know I shouldn't do this.  But I really
 * need a global variable in order to keep track of the motif profile and to
 * record a representative from each isomorphism class.
 */
igraph_vector_t mprofile;

/* function prototypes
 */
igraph_bool_t motif_profile(const igraph_t *graph,
                            igraph_vector_t *vids,
                            int ic,
                            void* extra);

/* Update the motif profile as we find each motif.  This function is meant to
 * be used as a callback function.
 *
 * WARNING:  This function uses a global variable.
 */
igraph_bool_t motif_profile(const igraph_t *graph,
                            igraph_vector_t *vids,
                            int ic,  /* isomorphism class */
                            void* extra) {
  igraph_integer_t i;
  igraph_integer_t k;
  igraph_integer_t eid;
  igraph_integer_t u;
  igraph_integer_t v;
  /* record a representative from each isomorphism class */
  /* Record the subgraph induced by the nodes in the representative. */
  if (VECTOR(mprofile)[ic] == 0) {
    printf("%d", ic);
    for (i = 0; i < igraph_vector_size(vids) - 1; i++) {
      for (k = i + 1; k < igraph_vector_size(vids); k++) {
        u = (igraph_integer_t)VECTOR(*vids)[i];
        v = (igraph_integer_t)VECTOR(*vids)[k];
        igraph_get_eid(graph, &eid, u, v, IGRAPH_UNDIRECTED, /*error=*/ 0);
        if (eid >= 0) {
          printf(",%15ld %15ld", (long int)u, (long int)v);
        }
      }
    }
    printf("\n");
  }
  /* update motif profile */
  VECTOR(mprofile)[ic] += 1L;

  return 0;
}

/* Find all motifs of a given size.  Currently, we only support motifs of
 * sizes 3 and 4.  The algorithm for motif detection is from:
 *
 * S. Wernicke and F. Rasche. FANMOD: a tool for fast network motif
 * detection. Bioinformatics, 22:1152--1153, 2006.
 *
 * WARNING:  This function uses a global variable.
 */
int main(int argc,
         char **argv) {
  FILE *file;
  char *fname;
  int i;
  int n;
  igraph_t G;
  igraph_vector_t cp;  /* cut-off probabilities */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_file *felist = arg_file0(NULL, "felist", NULL,
                               "file containing edge list, one edge per line");
  struct arg_str *size = arg_strn(NULL, "size", "string", 0, argc + 2,
                                  "size of motif; only support 3 and 4");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, felist, size, end};

  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Find all motifs of a given size.\n");
    arg_print_glossary(stdout, argtable, "  %-25s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((felist->count < 1)
      || (size->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Find all motifs of a given size.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: must use a supported motif size */
  if ((strcmp(size->sval[0], "3") != 0)
      && (strcmp(size->sval[0], "4") != 0)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Find all motifs of a given size.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* reconstruct graph from its edge list: simple undirected graph */
  asprintf(&fname, "%s", felist->filename[0]);
  file = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, file, /*nvert*/ 0, IGRAPH_UNDIRECTED);
  fclose(file);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);

  n = atoi(size->sval[0]);  /* motif size */
  if (n == 3) {
    /* 4 isomorphism classes for motifs on 3 nodes */
    igraph_vector_init(&mprofile, 0);  /* global variable */
    for (i = 0; i < 4; i++) {
      igraph_vector_push_back(&mprofile, 0L);
    }
  } else if (n == 4) {
    /* 11 isomorphism classes for motifs on 4 nodes */
    igraph_vector_init(&mprofile, 0);  /* global variable */
    for (i = 0; i < 11; i++) {
      igraph_vector_push_back(&mprofile, 0L);
    }
  }
  igraph_vector_init_real(&cp, 8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
  igraph_motifs_randesu_callback(&G, n, &cp, &motif_profile, 0);

  /* print motif profile */
  for (i = 0; i < igraph_vector_size(&mprofile); i++) {
    printf("%20ld ", (long int)VECTOR(mprofile)[i]);
  }
  printf("\n");

  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
  igraph_destroy(&G);
  igraph_vector_destroy(&cp);
  igraph_vector_destroy(&mprofile);  /* global variable */

  return 0;
}
