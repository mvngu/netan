###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Expand each dynamic community.  For each snapshot of a dynamic community, we
# list all nodes in that snapshot.

import argparse
import os

###################################
# helper functions
###################################

def clean_snapshot(dycomsnapshot):
    """
    Clean the given ID for a snapshot of a dynamic community.  The snapshot
    ID is assumed to follow the format:

        yyyy i,[n]

    where [n] denotes an optional n.

    INPUT:

    - dycomsnapshot -- an ID of a snapshot of a dynamic community.
    """
    # get network snapshot and community IDs
    snapshotid, comid = dycomsnapshot.split()
    if "," in comid:
        comid, _ = comid.split(",")
    return int(snapshotid), int(comid)

def community_nodes(comdir, snapshotid, comid):
    """
    All nodes belonging to a community with the given snapshot and community
    IDs.

    INPUT:

    - comdir -- path to directory with community files.
    - snapshotid -- the ID of the network snapshot.
    - comid -- the ID of the community.
    """
    fname = os.path.join(comdir, "comm-%d.dat" % snapshotid)
    f = open(fname, "r")
    f.readline()  # ignore first line, which counts number of communities
    i = 0
    comnodes = None
    for line in f:
        if i == comid:
            comnodes = line.strip()
            break
        i += 1
    f.close()
    return comnodes

def expand_all_dycom(tracedir, maxlifespan, comdir, outdir):
    """
    Expand each dynamic community.  For each snapshot of a dynamic community,
    we list all nodes in that snapshot.

    INPUT:

    - tracedir -- path to directory with trace files.
    - maxlifespan -- maximum lifespan of a dynamic community.
    - comdir -- path to directory with community files.
    - outdir -- write results to files under this directory.
    """
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    # get the trace of each dynamic community and expand it
    comid = 0  # ID for each dynamic community
    for ell in range(1, maxlifespan + 1):
        fname = os.path.join(tracedir, "trace-%d.dat" % ell)
        f = open(fname, "r")
        # Ignore the first line, which counts the number of dynamic
        # communities with the given lifespan.
        f.readline()
        for line in f:
            trace = parse_trace(line.strip())
            expand_dycom(trace, comdir, outdir, comid)
            comid += 1
        f.close()

def expand_dycom(trace, comdir, outdir, comid):
    """
    Expand a dynamic community.  For each snapshot of the dynamic community,
    we list all nodes in that snapshot.

    INPUT:

    - trace -- the trace of a dynamic community.
    - comdir -- path to directory with community files.
    - outdir -- write results to files under this directory.
    - comid -- the ID assigned to the dynamic community.  This is the running
      ID for files.
    """
    fname = os.path.join(outdir, "dycom-%d.dat" % comid)
    f = open(fname, "w")
    # file header; the lifespan of the dynamic community
    f.write("%d\n" % len(trace))
    for dycomsnapshot in trace:
        snapshotid, cid = clean_snapshot(dycomsnapshot)
        comnodes = community_nodes(comdir, snapshotid, cid)
        f.write("%d %s\n" % (snapshotid, comnodes))
    f.close()

# This function is stolen from entropy.py
def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline of a dynamic
    community.  Each step community is represented as

        year com

    where "year" refers to the snapshot year and "com" refers to the index
    of the community in the given year.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    return trace.split("->")

###################################
# script starts here
###################################

# setup parser for command line options
s = "Expand each dynamic community.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--tracedir", metavar="path", required=True,
                    help="path to directory with trace files")
parser.add_argument("--maxlifespan", metavar="int", required=True,
                    help="maximum lifespan of a dynamic community")
parser.add_argument("--comdir", metavar="path", required=True,
                    help="path to directory with community files")
# parser.add_argument("--startindex", metavar="int", required=True,
#                     help="starting snapshot index")
# parser.add_argument("--endindex", metavar="int", required=True,
#                     help="last snapshot index")
parser.add_argument("--outdir", metavar="path", required=True,
                    help="write results to files under this directory")
args = parser.parse_args()

# get the command line arguments
tracedir = args.tracedir
maxlifespan = int(args.maxlifespan)
comdir = args.comdir
# startindex = int(args.startindex)
# endindex = int(args.endindex)
outdir = args.outdir

expand_all_dycom(tracedir, maxlifespan, comdir, outdir)
