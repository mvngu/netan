###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile outfile
#
# Plot the cumulative distribution of H values.
# We consider the complementary cumulative distribution or tail distribution.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
infile <- args[1]   # file with cumulative distribution of H values
outfile <- args[2]  # write the plot to this file

D <- read.csv(file=infile, header=TRUE, sep=",")
colnames(D) <- c("Hvalue", "cumfreq")
pdf(outfile)
# Plot the cumulative distribution, but ignore the case when H = 0.
plot(D[2:length(D$Hvalue),], xlab="H value", ylab="cumulative frequency")
dev.off()
