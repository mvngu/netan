/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* The degree of a dynamic community.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <igraph.h>

int is_out_neighbour(const igraph_t *G,
                     const igraph_integer_t u,
                     const igraph_integer_t v);
void degree(const igraph_t *G,
            igraph_vector_t *IND,
            igraph_vector_t *OUTD,
            const igraph_integer_t s,
            FILE *fdegree);
void dycom_degree(const igraph_t *G,
                  igraph_vector_t *IND,
                  igraph_vector_t *OUTD,
                  const char *outdir);

/* Is v an out-neighbour of u?  In other words, is u a parent of v?
 *
 * INPUT:
 *
 * - G -- a directed graph.
 * - u -- a node in G.
 * - v -- another node in G.
 */
int is_out_neighbour(const igraph_t *G,
                     const igraph_integer_t u,
                     const igraph_integer_t v) {
  int res;
  igraph_vector_t Adj;

  igraph_vector_init(&Adj, 0);
  igraph_neighbors(G, &Adj, u, IGRAPH_OUT);
  res = (int)igraph_vector_binsearch2(&Adj, v);
  igraph_vector_destroy(&Adj);
  return res;
}

/* Trace all dynamic communities from the given start node.
 *
 * INPUT:
 *
 * - G -- a digraph representation of community matches.
 * - IND -- the sequence of in-degrees of G.  We assume that IND[i] is the
 *   in-degree of node i.
 * - OUTD -- the sequence of out-degrees of G.  We assume that D[i] is the
 *   out-degree of node i.
 * - s -- the node from which to start tracing dynamic communities.  This
 *   must be a node in the given digraph.  We assume that this node has
 *   in-degree zero.  We call this the source node.
 * - fdegree -- write to this file the sequences of in-, out-, and total
 *   degrees.
 */
void degree(const igraph_t *G,
            igraph_vector_t *IND,
            igraph_vector_t *OUTD,
            const igraph_integer_t s,
            FILE *fdegree) {
  int i;
  int j;
  int k;
  int indeg;            /* the in-degree */
  int nmerge;           /* the number of merge points */
  int outdeg;           /* the out-degree */
  int nsplit;           /* the number of split points */
  int u;
  int v;
  int w;
  igraph_stack_t S;     /* index all those in D to be further explored */
  igraph_vector_t Adj;  /* the neighbours of a node */
  igraph_vector_t T;    /* the trace of a dynamic community */

  /* It is possible that s is isolated, i.e. it has one time step.  In this */
  /* case, its in- and out-degrees are both zero. */
  if (VECTOR(*OUTD)[s] == 0) {
    /* The file format is "degree". */
    fprintf(fdegree, "0\n");
    return;
  }

  /* From hereon, we assume that s has out-degree > 0.  Do a depth-first */
  /* search from s to get all dynamic communities that start from s.  We */
  /* assume that the given graph is directed and we follow edges that go out */
  /* from s.  We only record nodes that can be reached from s. */
  igraph_vector_init(&T, 0);
  igraph_stack_init(&S, 0);
  igraph_stack_push(&S, (int)s);
  while (igraph_stack_size(&S) > 0) {
    u = (int)igraph_stack_pop(&S);
    igraph_vector_push_back(&T, u);
    /* record all successors that have exactly one out-neighbour */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    while (VECTOR(*OUTD)[v] == 1) {
      igraph_vector_init(&Adj, 0);
      igraph_neighbors(G, &Adj, v, IGRAPH_OUT);
      igraph_vector_push_back(&T, (int)VECTOR(Adj)[0]);
      i = (int)igraph_vector_size(&T) - 1;
      v = (int)VECTOR(T)[i];
      igraph_vector_destroy(&Adj);
    }

    /* If a node has > 1 out-neighbour, follow all of these out-neighbours. */
    /* To keep track of which one we are yet to visit, we push these */
    /* out-neighbours onto the stack.  If we don't follow an out-neighbour, */
    /* we won't be able to reach the end point of a dynamic community. */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    if (VECTOR(*OUTD)[v] > 1) {
      igraph_vector_init(&Adj, 0);
      igraph_neighbors(G, &Adj, v, IGRAPH_OUT);
      for (j = 0; j < (int)igraph_vector_size(&Adj); j++) {
        igraph_stack_push(&S, (int)VECTOR(Adj)[j]);
      }
      igraph_vector_destroy(&Adj);
      continue;
    }

    /* We have reached the end point of a dynamic community.  Backtrack to */
    /* the last node in T that has as its out-neighbour the node at the top */
    /* of the stack S.  The purpose of the backtracking is to find the  */
    /* latest node from which to trace out another dynamic community. */
    i = (int)igraph_vector_size(&T) - 1;
    v = (int)VECTOR(T)[i];
    if (VECTOR(*OUTD)[v] == 0) {
      /* We have found another dynamic community.  Get the in-degree of this */
      /* dynamic community.  The in-degree is the number of outside edges */
      /* that point to this dynamic community.  Let u be a snapshot of a */
      /* dynamic community.  If u is a point of merge, then one edge from */
      /* the dynamic community is pointing to u and the remaining edges are */
      /* outside edges as these emanate from other dynamic communities.  Let */
      /* v_1, ..., v_k be all snapshots of the dynamic community, each of */
      /* which is a point of merge.  If indeg(v_i) is the in-degree of v_i, */
      /* then the in-degree of the dynamic community is given by */
      /* (\sum_{i=1}^k indeg(v_i)) - k.  Also, we get the out-degree of this */
      /* dynamic community.  The out-degree is the number of edges that go */
      /* outside of this dynamic community.  Let u be a snapshot of a */
      /* dynamic community.  If u is a point of split, then one edge from */
      /* the dynamic community is pointing to the next snapshot while the */
      /* remaining edges are pointing outside to other dynamic communities. */
      /* Let v_1, ..., v_k be all snapshots of the dynamic community, each */
      /* of which is a point of split.  If outdeg(v_i) is the out-degree of */
      /* v_i, then the out-degree of the dynamic community is given by */
      /* (\sum_{i=1}^k outdeg(v_i)) - k. */
      indeg = 0;   /* in-degree of the dynamic community */
      nmerge = 0;  /* number of merge points */
      outdeg = 0;  /* out-degree of the dynamic community */
      nsplit = 0;  /* number of merge points */
      for (k = 0; k < (int)igraph_vector_size(&T); k++) {
        v = (int)VECTOR(T)[k];
        if (VECTOR(*IND)[v] > 1) {
          indeg += (int)VECTOR(*IND)[v];
          nmerge++;
        }
        if (VECTOR(*OUTD)[v] > 1) {
          outdeg += (int)VECTOR(*OUTD)[v];
          nsplit++;
        }
      }
      indeg = indeg - nmerge;
      outdeg = outdeg - nsplit;
      /* The file format is "degree". */
      fprintf(fdegree, "%i\n", indeg + outdeg);

      /* now backtrack */
      if (igraph_stack_size(&S) > 0) {
        w = (int)igraph_stack_top(&S);
        i = (int)igraph_vector_size(&T) - 1;
        v = (int)VECTOR(T)[i];
        while (!is_out_neighbour(G, v, w)) {
          igraph_vector_pop_back(&T);
          i = (int)igraph_vector_size(&T) - 1;
          v = (int)VECTOR(T)[i];
        }
      }
    }
  }
  igraph_stack_destroy(&S);
  igraph_vector_destroy(&T);
}

/* Trace all dynamic communities using the given digraph.  Each community
 * is traced from its birth to its death, or possibly up to the latest
 * snapshot for which we have data.
 *
 * INPUT:
 *
 * - G -- a digraph representation of community matches.  We use this
 *   digraph representation to trace the evolution of each dynamic
 *   community.
 * - IND -- the sequence of in-degrees of G.  We assume that IND[i] is the
 *   in-degree of node i.
 * - OUTD -- the sequence of out-degrees of G.  We assume that OUTD[i] is the
 *   out-degree of node i.
 * - outdir -- write results to files under this directory.
 */
void dycom_degree(const igraph_t *G,
                  igraph_vector_t *IND,
                  igraph_vector_t *OUTD,
                  const char *outdir) {
  FILE *fdegree;
  char *fname;
  int i;

  asprintf(&fname, "%s/degree.dat", outdir);
  fdegree = fopen(fname, "w");  /* overwite if it exists */
  fprintf(fdegree, "degree\n");
  fclose(fdegree);
  fdegree = fopen(fname, "a");
  free(fname);
  for (i = 0; i < (int)igraph_vector_size(IND); i++) {
    /* Get the source node of a dynamic community.  This node is the */
    /* very first node in the history of the dynamic community.  A source */
    /* node is defined as any node whose in-degree is zero.  Determine all */
    /* dynamic communities starting from the given source node. */
    if (VECTOR(*IND)[i] == 0) {
      degree(G, IND, OUTD, i, fdegree);
    }
  }
  fclose(fdegree);
}

/* The degree of each dynamic community.
 */
int main(int argc,
         char **argv) {
  FILE *f;
  char *fname;
  igraph_t G;             /* the sampled network; must be directed & simple */
  igraph_vector_t IND;    /* sequence of in-degree */
  igraph_vector_t OUTD;   /* sequence of out-degree */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_str *edgelist = arg_strn(NULL, "edgelist", "path", 0, argc + 2,
                                      "read the edgelist from this file");
  struct arg_str *outdir = arg_strn(NULL, "outdir", "path", 0, argc + 2,
                                "write results to files under this directory");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, edgelist, outdir, end};
  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("The degree of each dynamic community.\n");
    arg_print_glossary(stdout, argtable, "  %-17s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((edgelist->count < 1)
      || (outdir->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("The degree of each dynamic community.\n");
    arg_print_glossary(stdout, argtable, "  %-17s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* Construct the graph from its edge list.  The graph is a simple digraph, */
  /* i.e. no self-loops nor multiple edges. */
  asprintf(&fname, "%s", edgelist->sval[0]);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, f, 0, IGRAPH_DIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  /* get the degree of each dynamic community */
  igraph_vector_init(&OUTD, 0);
  igraph_degree(&G, &OUTD, igraph_vss_all(), IGRAPH_OUT, IGRAPH_NO_LOOPS);
  igraph_vector_init(&IND, 0);
  igraph_degree(&G, &IND, igraph_vss_all(), IGRAPH_IN, IGRAPH_NO_LOOPS);
  dycom_degree(&G, &IND, &OUTD, outdir->sval[0]);

  igraph_destroy(&G);
  igraph_vector_destroy(&IND);
  igraph_vector_destroy(&OUTD);
  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
