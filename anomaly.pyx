###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This is a C extension for Python.  You should not use this directly.
# Write a Python script to import specific functions from this module.
#
# Locate the region with the greatest changes in a sequence of time ordered
# graphs.

import gc
from itertools import izip
import os

cdef adjacency_list(edgeset):
    """
    The adjacency list of each node in the given edge set.  Each element of
    the set is an edge represented as a 2-tuple.  If the given edge set is
    an edge set of a line graph, then each element of the 2-tuple is again a
    2-tuple.

    INPUT:

    - edgeset -- an edge set of a graph or line graph.
    """
    cdef long mx  # the maximum degree
    Adj = {}      # a dictionary of dictionaries
    mx = 0
    for u, v in edgeset:
        # update adjacency list
        if u in Adj:
            Adj[u].setdefault(v, True)
        else:
            Adj.setdefault(u, {v:True})
        if v in Adj:
            Adj[v].setdefault(u, True)
        else:
            Adj.setdefault(v, {u:True})
        # track the maximum degree
        if len(Adj[u]) > mx:
            mx = len(Adj[u])
        if len(Adj[v]) > mx:
            mx = len(Adj[v])
    return Adj, mx

cdef adjacency_list_linegraph(fname):
    """
    The adjacency list of each node in the given edge set.  Each element of
    the set is an edge represented as a 2-tuple.  As the given edge set is
    an edge set of a line graph, then each element of the 2-tuple is again a
    2-tuple.

    INPUT:

    - fname -- file containing an edge set of a line graph.
    """
    cdef int mx  # the maximum degree
    cdef int u1
    cdef int u2
    cdef int v1
    cdef int v2
    Adj = dict()  # a dictionary of dictionaries
    mx = 0
    f = open(fname, "r")
    for line in f:
        u1str, u2str, v1str, v2str = line.strip().split()
        u1 = int(u1str)
        u2 = int(u2str)
        v1 = int(v1str)
        v2 = int(v2str)
        # don't consider self-loops in line graph
        if tuple(sorted([u1, u2])) == tuple(sorted([v1, v2])):
            continue
        # don't consider self-loops in original graph
        if (u1 == u2) or (v1 == v2):
            continue
        # update adjacency list
        u, v = tuple(sorted([(u1, u2), (v1, v2)]))
        u = tuple(sorted(list(u)))
        v = tuple(sorted(list(v)))
        if u in Adj:
            Adj[u].setdefault(v, True)
        else:
            Adj.setdefault(u, {v:True})
        if v in Adj:
            Adj[v].setdefault(u, True)
        else:
            Adj.setdefault(v, {u:True})
        # track the maximum degree
        if len(Adj[u]) > mx:
            mx = len(Adj[u])
        if len(Adj[v]) > mx:
            mx = len(Adj[v])
        u1str = None
        u2str = None
        v1str = None
        v2str = None
    f.close()
    f = None
    return Adj, mx

def change_linegraph_level(fnameEdgeA, fnameEdgeB, fnameAdjA, fnameAdjB,
                           fchangedist, fmaxchange):
    """
    Locate the region of greatest changes.  Here, we consider the level of
    line graph.  A line graph is represented as an edge list according to the
    format:

        u1 u2 v1 v2

    Here, the vertices v1, v2, u1, and u2 are nodes in the original graph.
    The edges (v1, v2) and (u1, u2) are nodes in the line graph.  Thus
    v1 v2 u1 u2 represents an edge in the line graph.  We assume that the
    original graph is simple.  Therefore, we ignore any edges that satisfy
    the following cases:

      * (u1, u2) == (v1, v2) --- This implies multiple edges between
        u1 and v1 in the original graph.
      * u1 == u2 or v1 == v2 --- This implies that in the original graph,
        there is a self-loop at u1 or at v1.

    INPUT:

    - fnameEdgeA -- an edge list of a line graph.
    - fnameEdgeB -- an edge list of another line graph.
    - fnameAdjA -- an adjacency list of a line graph.
    - fnameAdjB -- an adjacency list of another line graph.
    - fchangedist -- write change distributions to files with this prefix.
    - fmaxchange -- write greatest changes to files with this prefix.
    """
    cdef unsigned short pls = 1    # plus
    cdef unsigned short mns = 0    # minus
    cdef unsigned int maxdegminus  # greatest number of neighbours deleted
    cdef unsigned int maxdegplus   # greatest number of new neighbours
    fsymdif = os.path.join(        # temporary file for symmetric difference
        os.path.dirname(fnameEdgeA), "symdif.dat")

    # The total change, irrespective of addition or deletion of edges.  Note
    # that the information in AdjTotal can also be inferred from the
    # plus-minus table.
    symmetric_difference(fnameEdgeA, fnameEdgeB, fsymdif)
    AdjTotal, maxdeg = adjacency_list_linegraph(fsymdif)
    VTotal = dict()  # all nodes with greatest degree
    for u in AdjTotal:
        if len(AdjTotal[u]) == maxdeg:
            VTotal.setdefault(u, maxdeg)
    write_change_dist_all(
        fmaxchange + "-all.dat", VTotal, pmtable=False, linegraph=True)
    os.remove(fsymdif)
    fsymdif = None
    AdjTotal = None
    VTotal = None
    maxdeg = None
    gc.collect()

    # The plus table records the total number of addition of edges incident on
    # each node.  The "plus" refers to addition.
    PlusTable = dict()
    # The total number of addition of edges.  Let A be the set of all
    # neighbours of v in AdjB that are not in AdjA.  Then members of A are new
    # neighbours of v in the transformation from AdjA to AdjB.  The
    # cardinality of A therefore counts the number of new edges incident on v.
    maxdegplus = 0  # greatest number of new neighbours
    Plus = []
    AdjA, _ = adjacency_list_linegraph(fnameEdgeA)
    f = open(fnameAdjB, "r")
    for line in f:
        adj = line.strip().split(",")
        a, b = adj[0].split()
        v = tuple(sorted([int(a), int(b)]))
        Plus = []
        if v in AdjA:
            for VV in adj[1:]:
                a, b = VV.split()
                u = tuple(sorted([int(a), int(b)]))
                if u not in AdjA[v]:
                    Plus.append(u)
        else:
            for VV in adj[1:]:
                a, b = VV.split()
                u = tuple(sorted([int(a), int(b)]))
                Plus.append(u)
        PlusTable.setdefault(v, {pls: sorted(Plus)})
        if len(Plus) > maxdegplus:
            maxdegplus = len(Plus)
        adj = None
    f.close()
    Plus = None
    gc.collect()
    # Ensure consistency of the plus table.  That is, each node must have a
    # separate record for its plus.
    for v in AdjA:
        if v not in PlusTable:
            PlusTable.setdefault(v, {pls: {}})
        else:
            if pls not in PlusTable[v]:
                PlusTable[v].setdefault(pls, {})
    AdjA = None
    gc.collect()
    AdjB, _ = adjacency_list_linegraph(fnameEdgeB)
    for v in AdjB:
        if v not in PlusTable:
            PlusTable.setdefault(v, {pls: {}})
        else:
            if pls not in PlusTable[v]:
                PlusTable[v].setdefault(pls, {})
    AdjB = None
    gc.collect()
    # All nodes with greatest number of new neighbours.
    VPlus = dict()  # all nodes with greatest number of new neighbours
    for v in PlusTable:
        if len(PlusTable[v][pls]) == maxdegplus:
            VPlus.setdefault(v, PlusTable[v][pls])
    # output to file
    write_change_dist_pm(
        fmaxchange + "-plus.dat", VPlus, pmtable=False, plus=False,
        linegraph=True)
    write_change_dist_pm(
        fchangedist + "-plus.dat", PlusTable, pmtable=True, plus=True,
        linegraph=True)
    VPlus = None
    PlusTable = None
    gc.collect()

    # The minus table records the total number of deletion of edges incident on
    # each node.  The "minus" refers to deletion.
    MinusTable = dict()
    # The total number of deletion of edges.  Let D be the set of all
    # neighbours of v in AdjA that are not in AdjB.  Then members of D are
    # neighbours that are deleted in the transformation from AdjA to AdjB.
    # The cardinality of D therefore counts the number of edges incident on
    # v that are deleted in AdjB.
    maxdegminus = 0  # greatest number of neighbours deleted
    Minus = []
    AdjB, _ = adjacency_list_linegraph(fnameEdgeB)
    f = open(fnameAdjA, "r")
    for line in f:
        adj = line.strip().split(",")
        a, b = adj[0].split()
        v = tuple(sorted([int(a), int(b)]))
        Minus = []
        if v in AdjB:
            for VV in adj[1:]:
                a, b = VV.split()
                u = tuple(sorted([int(a), int(b)]))
                if u not in AdjB[v]:
                    Minus.append(u)
        else:
            for VV in adj[1:]:
                a, b = VV.split()
                u = tuple(sorted([int(a), int(b)]))
                Minus.append(u)
        MinusTable.setdefault(v, {mns: sorted(Minus)})
        if len(Minus) > maxdegminus:
            maxdegminus = len(Minus)
        adj = None
    f.close()
    Minus = None
    AdjB = None
    gc.collect()
    # Ensure consistency of the minus table.  That is, each node must have a
    # separate record for its minus.
    AdjA, _ = adjacency_list_linegraph(fnameEdgeA)
    for v in AdjA:
        if v not in MinusTable:
            MinusTable.setdefault(v, {mns: {}})
        else:
            if mns not in MinusTable[v]:
                MinusTable[v].setdefault(mns, {})
    AdjA = None
    gc.collect()
    AdjB, _ = adjacency_list_linegraph(fnameEdgeB)
    for v in AdjB:
        if v not in MinusTable:
            MinusTable.setdefault(v, {mns: {}})
        else:
            if mns not in MinusTable[v]:
                MinusTable[v].setdefault(mns, {})
    AdjB = None
    gc.collect()
    # All nodes with greatest number of neighbours deleted.
    VMinus = dict()  # all nodes with greatest number of neighbours deleted
    for v in MinusTable:
        if len(MinusTable[v][mns]) == maxdegminus:
            VMinus.setdefault(v, MinusTable[v][mns])
    # output to file
    write_change_dist_pm(
        fmaxchange + "-minus.dat", VMinus, pmtable=False, plus=False,
        linegraph=True)
    write_change_dist_pm(
        fchangedist + "-minus.dat", MinusTable, pmtable=True, plus=False,
        linegraph=True)
    VMinus = None
    MinusTable = None
    gc.collect()

    # total changes, disregarding addition or deletion of edges
    fplus = open(fchangedist + "-plus.dat", "r")
    fminus = open(fchangedist + "-minus.dat", "r")
    fplus.readline()   # ignore first line, which is file header
    fminus.readline()  # ignore first line, which is file header
    f = open(fchangedist + "-all.dat", "w")
    f.write("node,total change\n")  # file header
    for linep, linem in izip(fplus, fminus):
        nodep, countp, _ = linep.strip().split(":")
        nodem, countm, _ = linem.strip().split(":")
        assert nodep == nodem
        countp = int(countp)
        countm = int(countm)
        f.write("%s,%d\n" % (nodep, countp + countm))
    fplus.close()
    fminus.close()
    f.close()

def change_node_level(fnameA, fnameB, fchangedist, fmaxchange):
    """
    Locate the region of greatest changes.  Here, we consider the node level.

    INPUT:

    - fnameA -- the first edge list.
    - fnameB -- the second edge list.
    """
    cdef unsigned short pls = 1  # plus
    cdef unsigned short mns = 0  # minus

    # The total change, irrespective of addition or deletion of edges.  Note
    # that the information in AdjTotal can also be inferred from the
    # plus-minus table.
    EdgeA, IsolateA = edge_set(fnameA, False)
    EdgeB, IsolateB = edge_set(fnameB, False)
    SymDif = EdgeA.symmetric_difference(EdgeB)
    AdjTotal, maxdeg = adjacency_list(SymDif)
    VTotal = {}  # all nodes with greatest degree
    for u in AdjTotal:
        if len(AdjTotal[u]) == maxdeg:
            VTotal.setdefault(u, maxdeg)

    # The plus-minus table records the total number of addition and deletion
    # of edges incident on each node.  The "plus" refers to addition, whereas
    # the "minus" means deletion.
    PlusMinusTable = {}
    AdjA, _ = adjacency_list(EdgeA)
    AdjB, _ = adjacency_list(EdgeB)
    EdgeA = None
    EdgeB = None
    SymDif = None
    AdjTotal = None
    maxdeg = None
    gc.collect()

    # The total number of addition of edges.  Let A be the set of all
    # neighbours of v in AdjB that are not in AdjA.  Then members of A are new
    # neighbours of v in the transformation from AdjA to AdjB.  The
    # cardinality of A therefore counts the number of new edges incident on v.
    maxdegplus = 0  # greatest number of new neighbours
    for v in AdjB:
        Plus = []
        if v in AdjA:
            for u in AdjB[v]:
                if u not in AdjA[v]:
                    Plus.append(u)
        else:
            Plus = AdjB[v].keys()
        PlusMinusTable.setdefault(v, {pls: sorted(Plus)})
        if len(Plus) > maxdegplus:
            maxdegplus = len(Plus)
        Plus = None

    # The total number of deletion of edges.  Let D be the set of all
    # neighbours of v in AdjA that are not in AdjB.  Then members of D are
    # neighbours that are deleted in the transformation from AdjA to AdjB.
    # The cardinality of D therefore counts the number of edges incident on
    # v that are deleted in AdjB.
    maxdegminus = 0  # greatest number of neighbours deleted
    for v in AdjA:
        Minus = []
        if v in AdjB:
            for u in AdjA[v]:
                if u not in AdjB[v]:
                    Minus.append(u)
        else:
            Minus = AdjA[v].keys()
        if v in PlusMinusTable:
            PlusMinusTable[v].setdefault(mns, sorted(Minus))
        else:
            PlusMinusTable.setdefault(v, {mns: sorted(Minus)})
        if len(Minus) > maxdegminus:
            maxdegminus = len(Minus)
        Minus = None
    AdjA = None
    AdjB = None
    gc.collect()

    # Ensure consistency of the plus-minus table.  That is, each node must
    # have a separate record for its plus and minus.
    Isolate = IsolateA.union(IsolateB)
    for v in Isolate:
        if v not in PlusMinusTable:
            PlusMinusTable.setdefault(v, {pls:{}, mns:{}})
    for v in PlusMinusTable:
        if pls not in PlusMinusTable[v]:
            PlusMinusTable[v].setdefault(pls, {})
        if mns not in PlusMinusTable[v]:
            PlusMinusTable[v].setdefault(mns, {})
    Isolate = None
    IsolateA = None
    IsolateB = None
    gc.collect()

    # All nodes with greatest number of new neighbours and greatest number of
    # neighbours deleted.
    VPlus = {}   # all nodes with greatest number of new neighbours
    VMinus = {}  # all nodes with greatest number of neighbours deleted
    for v in PlusMinusTable:
        if len(PlusMinusTable[v][pls]) == maxdegplus:
            VPlus.setdefault(v, PlusMinusTable[v][pls])
        if len(PlusMinusTable[v][mns]) == maxdegminus:
            VMinus.setdefault(v, PlusMinusTable[v][mns])

    write_change_dist_all(
        fmaxchange + "-all.dat", VTotal, pmtable=False, linegraph=False)
    write_change_dist_all(
        fchangedist + "-all.dat", PlusMinusTable, pmtable=True,
        linegraph=False)
    write_change_dist_pm(
        fchangedist + "-plus.dat", PlusMinusTable, pmtable=True, plus=True,
        linegraph=False)
    write_change_dist_pm(
        fchangedist + "-minus.dat", PlusMinusTable, pmtable=True, plus=False,
        linegraph=False)
    write_change_dist_pm(
        fmaxchange + "-plus.dat", VPlus, pmtable=False, plus=False,
        linegraph=False)
    write_change_dist_pm(
        fmaxchange + "-minus.dat", VMinus, pmtable=False, plus=False,
        linegraph=False)

cdef edge_set(fname, linegraph):
    """
    Get the edge set from the given file.  The given file can contain the
    edge set of a line graph.  A line graph is represented as an edge list
    according to the format:

        u1 u2 v1 v2

    Here, the vertices v1, v2, u1, and u2 are nodes in the original graph.
    The edges (v1, v2) and (u1, u2) are nodes in the line graph.  Thus
    v1 v2 u1 u2 represents an edge in the line graph.  We assume that the
    original graph is simple.  Therefore, if the edge u1 u2 v1 v2 in the line
    graph satisfies any of the following conditions, we would ignore the edge.

      * (u1, u2) == (v1, v2) --- This implies multiple edges between
        u1 and v1 in the original graph.
      * u1 == u2 or v1 == v2 --- This implies that in the original graph,
        there is a self-loop at u1 or at v1.

    INPUT:

    - fname -- the file containing an edge list, one edge per line.  The file
      is assumed to contain the edge set of a simple undirected graph.
    - linegraph -- boolean; whether we are reading the edge set of a line
      graph.
    """
    cdef int u1
    cdef int u2
    cdef int v1
    cdef int v2
    cdef int u
    cdef int v
    Edge = None     # all edges
    Isolate = None  # all isolated nodes
    f = open(fname, "r")
    if linegraph:
        Edge = dict()
        for line in f:
            u1str, u2str, v1str, v2str = line.strip().split()
            u1 = int(u1str)
            u2 = int(u2str)
            v1 = int(v1str)
            v2 = int(v2str)
            # don't consider self-loops in line graph
            if tuple(sorted([u1, u2])) == tuple(sorted([v1, v2])):
                continue
            # don't consider self-loops in original graph
            if (u1 == u2) or (v1 == v2):
                continue
            e = tuple(sorted([(u1, u2), (v1, v2)]))
            Edge.setdefault(e, True)
            e = None
            u1str = None
            u2str = None
            v1str = None
            v2str = None
    else:
        Edge = set()
        Isolate = set()
        for line in f:
            ustr, vstr = line.strip().split()
            if ustr == vstr:  # don't consider self-loops
                Isolate.add(int(ustr))
                continue
            u = int(ustr)
            v = int(vstr)
            e = tuple(sorted([u, v]))
            Edge.add(e)
    f.close()
    f = None
    if linegraph:
        return Edge
    return (Edge, Isolate)

cdef set_difference(fsymdifname, Edge, fname):
    """
    The set difference between the given edge set and the edge set contained
    in the given file.  You only need to use this function for the edge sets
    of line graphs.  The main reason is that a line graph usually has a much
    larger number of edges than the original graph.  Reading in the edge sets
    of two line graphs could take up gigabytes of RAM.  This function is
    designed to avoid such huge memory usage.

    WARNING: This function modifies its arguments.

    INPUT:

    - fsymdifname -- name of file to which we write the symmetric difference.
    - Edge -- an edge set to compare with.  This should be a dictionary for
      fast look-up.
    - fname -- a file containing another edge set.
    """
    cdef int u1
    cdef int u2
    cdef int v1
    cdef int v2
    f = open(fname, "r")
    fsymdif = open(fsymdifname, "a")
    for line in f:
        u1str, u2str, v1str, v2str = line.strip().split()
        u1 = int(u1str)
        u2 = int(u2str)
        v1 = int(v1str)
        v2 = int(v2str)
        # don't consider self-loops in line graph
        if tuple(sorted([u1, u2])) == tuple(sorted([v1, v2])):
            continue
        # don't consider self-loops in original graph
        if (u1 == u2) or (v1 == v2):
            continue
        e = tuple(sorted([(u1, u2), (v1, v2)]))
        if e not in Edge:
            u, v = e
            fsymdif.write("%d %d %d %d\n" % (u[0], u[1], v[0], v[1]))
            u = None
            v = None
        e = None
        u1str = None
        u2str = None
        v1str = None
        v2str = None
    f.close()
    fsymdif.close()
    f = None
    fsymdif = None

cdef symmetric_difference(fnameA, fnameB, fsymdifname):
    """
    The symmetric difference of two edge sets.  You only need to use this
    function for the edge sets of line graphs.  The main reason is that a line
    graph usually has a much larger number of edges than the original graph.
    Reading in the edge sets of two line graphs could take up gigabytes of RAM.
    This function is designed to avoid such huge memory usage.

    INPUT:

    - fnameA -- file containing an edge set.
    - fnameB -- file containing another edge set.
    - fsymdifname -- name of file to which we write the symmetric difference.

    OUTPUT:

    File containing the required symmetric difference.
    """
    f = open(fsymdifname, "w")
    f.close()
    Edge = edge_set(fnameA, True)
    set_difference(fsymdifname, Edge, fnameB)
    Edge = None
    gc.collect()
    Edge = edge_set(fnameB, True)
    set_difference(fsymdifname, Edge, fnameA)
    Edge = None
    f = None
    gc.collect()

def write_change_dist_all(fname, Dist, pmtable=(True, False),
                          linegraph=(True, False)):
    """
    Write the change distribution to the given file.

    INPUT:

    - fname -- write the change distribution to this file.
    - Dist -- the change distribution.  This is a dictionary where the key is
      the node ID and the value is either the degree of the node or the node's
      adjacency list.
    - pmtable -- boolean; whether Dist is a plus-minus table.
    - linegraph -- boolean; whether the given change distribution relates to
      a line graph.
    """
    cdef unsigned short pls = 1  # plus
    cdef unsigned short mns = 0  # minus
    f = open(fname, "w")
    f.write("node,total change\n")  # file header
    if pmtable:
        if linegraph:
            for v in sorted(Dist.keys()):
                s = "%d %d" % (v[0], v[1])
                f.write("%s,%d\n" % (s, Dist[v]))
        else:
            for v in sorted(Dist.keys()):
                plus = len(Dist[v][pls])
                minus = len(Dist[v][mns])
                total = plus + minus
                s = "%d" % v
                f.write("%s,%d\n" % (s, total))
    else:
        for v in sorted(Dist.keys()):
            s = None
            if linegraph:
                s = "%d %d" % (v[0], v[1])
            else:
                s = "%d" % v
            f.write("%s,%d\n" % (s, Dist[v]))
    f.close()

def write_change_dist_pm(fname, Dist, pmtable=(True, False),
                         plus=(True, False), linegraph=(True, False)):
    """
    Write the change distribution to the given file.

    INPUT:

    - fname -- write the change distribution to this file.
    - Dist -- the change distribution.  This is a dictionary where the key is
      the node ID and the value is either the degree of the node or the node's
      adjacency list.
    - pmtable -- boolean; whether Dist is a plus-minus table.
    - plus -- boolean; whether we consider addition or deletion of edges.
    - linegraph -- boolean; whether the given change distribution relates to
      a line graph.
    """
    cdef unsigned short pls = 1  # plus
    cdef unsigned short mns = 0  # minus
    f = open(fname, "w")
    f.write("node:total neighbours:list of neighbours\n")  # file header
    if pmtable:
        for v in sorted(Dist.keys()):
            adj = None
            if plus:
                adj = sorted(Dist[v][pls])
            else:
                adj = sorted(Dist[v][mns])
            # if len(adj) < 1:
            #     continue
            s = None
            if linegraph:
                s = "%d %d" % (v[0], v[1])
                n = len(adj)
                t = ",".join(map(lambda x: "%d %d" % (x[0], x[1]), adj))
                s = ":".join([s, str(n), t])
            else:
                s = "%d" % v
                n = len(adj)
                t = ",".join(map(str, adj))
                s = ":".join([s, str(n), t])
            # follows the format:
            # node:#neighbours:list of neighbours
            f.write("%s\n" % s)
    else:
        for v in sorted(Dist.keys()):
            adj = sorted(Dist[v])
            s = None
            if linegraph:
                s = "%d %d" % (v[0], v[1])
                n = len(adj)
                t = ",".join(map(lambda x: "%d %d" % (x[0], x[1]), adj))
                s = ":".join([s, str(n), t])
            else:
                s = "%d" % v
                n = len(adj)
                t = ",".join(map(str, adj))
                s = ":".join([s, str(n), t])
            # follows the format:
            # node:#neighbours:list of neighbours
            f.write("%s\n" % s)
    f.close()
