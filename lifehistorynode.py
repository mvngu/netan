###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# The history of change of each node.

import argparse
import os

###################################
# helper functions
###################################

def add_neighbour(fsindex, anomdir):
    """
    The history of new neighbours for each node.  If s_0, s_1, ..., s_n are
    the snapshot indices of a network, then we only consider changes from
    snapshot s_1 onward.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    - anomdir -- path with files on anomaly at the node level.  This is also
      the directory under which we write our results.
    """
    History = dict()
    SIndex = snapshot_index(fsindex)
    for i in SIndex[1:]:
        fname = os.path.join(anomdir, "snapshot-%s" % i,
                             "change-%s-plus.dat" % i)
        f = open(fname, "r")
        f.readline()  # ignore first line, which is file header
        for line in f:
            node, tchange, _ = line.strip().split(":")
            node = int(node)
            if node not in History:
                History.setdefault(node, list())
            History[node].append(tchange)
        f.close()
    fname = os.path.join(anomdir, "change-plus.dat")
    f = open(fname, "w")
    f.write("node:history of new neighbours\n")
    for node in sorted(History.keys()):
        s = "%d:" % node
        s += ",".join(History[node])
        f.write("%s\n" % s)
    f.close()

def remove_neighbour(fsindex, anomdir):
    """
    The history of removal of neighbours for each node.  If s_0, s_1, ..., s_n
    are the snapshot indices of a network, then we only consider changes from
    snapshot s_1 onward.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    - anomdir -- path with files on anomaly at the node level.  This is also
      the directory under which we write our results.
    """
    History = dict()
    SIndex = snapshot_index(fsindex)
    for i in SIndex[1:]:
        fname = os.path.join(anomdir, "snapshot-%s" % i,
                             "change-%s-minus.dat" % i)
        f = open(fname, "r")
        f.readline()  # ignore first line, which is file header
        for line in f:
            node, tchange, _ = line.strip().split(":")
            node = int(node)
            if node not in History:
                History.setdefault(node, list())
            History[node].append(tchange)
        f.close()
    fname = os.path.join(anomdir, "change-minus.dat")
    f = open(fname, "w")
    f.write("node:history of removal of neighbours\n")
    for node in sorted(History.keys()):
        s = "%d:" % node
        s += ",".join(History[node])
        f.write("%s\n" % s)
    f.close()

def snapshot_index(fsindex):
    """
    All the snapshot indices of a network.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    """
    SIndex = list()
    f = open(fsindex, "r")
    for line in f:
        SIndex.append(line.strip())
    f.close()
    return SIndex

def total_change(fsindex, anomdir):
    """
    The history of total changes for each node.  If s_0, s_1, ..., s_n are
    the snapshot indices of a network, then we only consider changes from
    snapshot s_1 onward.

    INPUT:

    - fsindex -- file containing all snapshot indices.  The indices are
      assumed to be sorted in temporal order.
    - anomdir -- path with files on anomaly at the node level.  This is also
      the directory under which we write our results.
    """
    History = dict()
    SIndex = snapshot_index(fsindex)
    for i in SIndex[1:]:
        fname = os.path.join(anomdir, "snapshot-%s" % i,
                             "change-%s-all.dat" % i)
        f = open(fname, "r")
        f.readline()  # ignore first line, which is file header
        for line in f:
            node, tchange = line.strip().split(",")
            node = int(node)
            if node not in History:
                History.setdefault(node, list())
            History[node].append(tchange)
        f.close()
    fname = os.path.join(anomdir, "change-all.dat")
    f = open(fname, "w")
    f.write("node:history of change\n")
    for node in sorted(History.keys()):
        s = "%d:" % node
        s += ",".join(History[node])
        f.write("%s\n" % s)
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "The history of change of each node.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="file containing all snapshot indices")
parser.add_argument("--anomdir", metavar="path", required=True,
                    help="path with files on anomaly at the node level")
args = parser.parse_args()

# get the command line arguments
fsindex = args.fsindex
anomdir = args.anomdir

total_change(fsindex, anomdir)
add_neighbour(fsindex, anomdir)
remove_neighbour(fsindex, anomdir)
