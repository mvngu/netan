###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Clean files containing a series of network motif profiles.

import argparse

###################################
# helper functions
###################################

def clean_file(fname):
    """
    Clean the series of motif profiles in the given file.  The file is assumed
    to contain a series of network motif profiles, each arranged in the
    following format:

        isomorphism class1,u1 v1, ..., un vn
        ...
        isomorphism classk,w1 x2, ..., wn xn
        count class1 ... count classk

    Note that (ui vi) is an edge in the isomorphism class.  A listing of all
    these edges allows us to reconstruct a representive graph from the
    given isomorphism class.

    INPUT:

    - fname -- a file containing a profile of network motifs.
    """
    f = open(fname, "r")
    IsoClassSeries = []
    ProfileSeries = []
    IC = {}
    Pr = None
    for line in f:
        dat = line.strip()
        # another motif profile in the series
        # two motif profiles in the series are separated by a blank lines
        if len(dat) < 1:
            IsoClassSeries.append(IC)
            ProfileSeries.append(Pr)
            IC = {}
            Pr = None
            continue
        # an isomorphism class
        if "," in dat:
            dat = dat.split(",")
            clsnum = dat[0]
            rep = []
            for edge in dat[1:]:
                u, v = edge.split()
                u = int(u)
                v = int(v)
                rep.append(tuple(sorted([u, v])))
            rep = sorted(rep)
            IC.setdefault(int(clsnum), rep)
        # motif profile
        else:
            Pr = dat.strip().split()
    f.close()
    return IsoClassSeries, ProfileSeries

def write_motifs(fname, IsoClassSeries, ProfileSeries):
    """
    Write to the given file all isomorphism classes of network motifs with
    a given size.  We also write the motif profile.

    INPUT:

    - fname -- a file to which we write results.
    - IsoClass -- all the isomorphism classes of network motifs with a given
      size.
    - Profile -- the motif profile for all motifs with a given size.
    """
    f = open(fname, "w")
    nclass = 0
    for IsoClass, Profile in zip(IsoClassSeries, ProfileSeries):
        Class = sorted(IsoClass.keys())
        for i in Class:
            s = ""
            for u, v in IsoClass[i]:
                s = ",".join([s, "%d %d" % (u, v)])
            f.write("%d%s\n" % (i, s))
        p = " ".join(Profile)
        f.write("%s\n" % p)
        nclass += 1
        if nclass < len(IsoClassSeries):
            f.write("\n")
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Clean files containing network motif profiles.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--infile", metavar="file", required=True,
                    help="file to clean")
args = parser.parse_args()

# get command line arguments & sanity checks
infile = args.infile

IsoClassSeries, ProfileSeries = clean_file(infile)
write_motifs(infile, IsoClassSeries, ProfileSeries)
