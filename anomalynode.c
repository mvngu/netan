/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* Locate the region with the greatest changes in a sequence of time ordered */
/* graphs. */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <ctype.h>
#include <igraph.h>
#include <libcalg.h>
#include <stdio.h>

void change_minus(const igraph_t *G,
                  const igraph_t *H,
                  const char *fchangedist,
                  const char *fmaxchange,
                  igraph_vector_t *MinusCount);
void change_plus(const igraph_t *G,
                 const igraph_t *H,
                 const char *fchangedist,
                 const char *fmaxchange,
                 igraph_vector_t *PlusCount);
void change_total(const igraph_vector_t *MinusCount,
                  const igraph_vector_t *PlusCount,
                  const char *fprefix,
                  igraph_integer_t *max);
void change_total_max(const igraph_vector_t *MinusCount,
                      const igraph_vector_t *PlusCount,
                      const char *fprefix,
                      const igraph_integer_t max);
/* void greatest_degree(Set *Edge, */
/*                      igraph_vector_t *VTotal, */
/*                      igraph_integer_t *maxdeg); */
/* void symmetric_difference(const igraph_t *G, */
/*                           const igraph_t *H, */
/*                           Set *Symdif); */
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_t *G,
                            const igraph_t *H);

/* The total number of deletion of edges.  Let D be the set of all
 * neighbours of v in AdjA that are not in AdjB.  Then members of D are
 * neighbours that are deleted in the transformation from AdjA to AdjB.
 * The cardinality of D therefore counts the number of edges incident on
 * v that are deleted in AdjB.
 *
 * INPUT:
 *
 * - G -- a simple undirected graph.  This graph is assumed to be a snapshot of
 *   some network at time t.
 * - H -- another simple undirected graph.  This graph is assumed to be a
 *   snapshot of some network at time t + 1.
 * - fchangedist -- write change distributions to files with this prefix.
 * - fmaxchange -- write greatest changes to files with this prefix.
 * - MinusCount -- count the number of deleted neighbours for each node.
 *   This should be initialized beforehand.  The results will be stored here.
 */
void change_minus(const igraph_t *G,
                  const igraph_t *H,
                  const char *fchangedist,
                  const char *fmaxchange,
                  igraph_vector_t *MinusCount) {
  char *fname;
  FILE *f;
  igraph_integer_t i;
  igraph_integer_t k;
  igraph_integer_t maxdegminus;  /* greatest number of neighbours deleted */
  igraph_integer_t node;         /* vertex ID */
  igraph_integer_t u;
  igraph_integer_t v;
  igraph_vector_t *N;            /* all deleted neighbours of some node */
  igraph_vector_t nbrG;
  igraph_vector_t nbrH;
  /* All nodes with greatest number of neighbours deleted. */
  igraph_vector_t VMinus;
  /* The minus table records the total number of deletion of edges incident */
  /* on each node.  The "minus" refers to deletion.  Index of MinusTable is */
  /* node index. The element MinusTable[i] refers to a vector containing all */
  /* deleted neighbours of node i. */
  igraph_vector_ptr_t MinusTable;
  igraph_vit_t vit;

  igraph_vector_ptr_init(&MinusTable, 0);
  vector_of_vectors_init(&MinusTable, G, H);
  IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(&MinusTable, igraph_vector_destroy);
  assert((igraph_integer_t)igraph_vector_size(MinusCount)
         == (igraph_integer_t)igraph_vector_ptr_size(&MinusTable));

  maxdegminus = 0;
  node = 0;
  igraph_vit_create(G, igraph_vss_all(), &vit);
  /* iterate in ascending order of node ID */
  while (!IGRAPH_VIT_END(vit)) {
    v = (igraph_integer_t)IGRAPH_VIT_GET(vit);
    N = (igraph_vector_t *)VECTOR(MinusTable)[v];
    assert(node == v);
    /* neighbours of v in G; assumed to be sorted by node IDs */
    igraph_vector_init(&nbrG, 0);
    igraph_neighbors(G, &nbrG, v, IGRAPH_ALL);
    /* H has node v */
    if (v < igraph_vcount(H)) {
      /* neighbours of v in H; assumed to be sorted by node IDs */
      igraph_vector_init(&nbrH, 0);
      igraph_neighbors(H, &nbrH, v, IGRAPH_ALL);
      for (i = 0; i < igraph_vector_size(&nbrG); i++) {
        u = (igraph_integer_t)VECTOR(nbrG)[i];
        /* In graph H, node u is not a neighbour of v.  Thus in graph H, */
        /* an edge between u and v has been deleted. */
        if (!igraph_vector_binsearch2(&nbrH, u)) {
          igraph_vector_push_back(N, u);
        }
      }
      igraph_vector_destroy(&nbrH);
    }
    /* node v is not in H */
    else {
      /* All the deleted neighbours of v are those neighbours of v in the */
      /* graph G. */
      for (i = 0; i < igraph_vector_size(&nbrG); i++) {
        u = (igraph_integer_t)VECTOR(nbrG)[i];
        igraph_vector_push_back(N, u);
      }
    }
    /* the running greatest number of neighbours deleted */
    if ((igraph_integer_t)igraph_vector_size(N) > maxdegminus) {
      maxdegminus = (igraph_integer_t)igraph_vector_size(N);
    }
    igraph_vector_destroy(&nbrG);
    VECTOR(*MinusCount)[node] = (igraph_integer_t)igraph_vector_size(N);
    node++;
    IGRAPH_VIT_NEXT(vit);
  }
  igraph_vit_destroy(&vit);

  /* all nodes with greatest number of neighbours deleted */
  igraph_vector_init(&VMinus, 0);
  for (i = 0; i < igraph_vector_ptr_size(&MinusTable); i++) {
    N = (igraph_vector_t *)VECTOR(MinusTable)[i];
    if ((igraph_integer_t)igraph_vector_size(N) == maxdegminus) {
      igraph_vector_push_back(&VMinus, i);
    }
  }
  assert(igraph_vector_size(&VMinus) > 0);
  /* output to file */
  asprintf(&fname, "%s-minus.dat", fmaxchange);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_size(&VMinus); i++) {
    v = (igraph_integer_t)VECTOR(VMinus)[i];
    N = (igraph_vector_t *)VECTOR(MinusTable)[v];
    fprintf(f, "%ld:%ld:", (long int)v, (long int)maxdegminus);
    if (igraph_vector_size(N) < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 1); k++) {
      fprintf(f, "%ld,", (long int)VECTOR(*N)[k]);
    }
    k = igraph_vector_size(N) - 1;
    fprintf(f, "%ld\n", (long int)VECTOR(*N)[k]);
  }
  fclose(f);
  free(fname);
  igraph_vector_destroy(&VMinus);

  /* output the minus table to file */
  asprintf(&fname, "%s-minus.dat", fchangedist);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_ptr_size(&MinusTable); i++) {
    N = (igraph_vector_t *)VECTOR(MinusTable)[i];
    fprintf(f, "%ld:%ld:", (long int)i, (long int)igraph_vector_size(N));
    if (igraph_vector_size(N) < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 1); k++) {
      fprintf(f, "%ld,", (long int)VECTOR(*N)[k]);
    }
    k = igraph_vector_size(N) - 1;
    fprintf(f, "%ld\n", (long int)VECTOR(*N)[k]);
  }
  fclose(f);
  free(fname);
  igraph_vector_ptr_destroy_all(&MinusTable);
}

/* The total number of addition of edges.  Let A be the set of all neighbours
 * of v in AdjB that are not in AdjA.  Then members of A are new neighbours of
 * v in the transformation from AdjA to AdjB.  The cardinality of A therefore
 * counts the number of new edges incident on v.
 *
 * INPUT:
 *
 * - G -- a simple undirected graph.  This graph is assumed to be a snapshot of
 *   some network at time t.
 * - H -- another simple undirected graph.  This graph is assumed to be a
 *   snapshot of some network at time t + 1.
 * - fchangedist -- write change distributions to files with this prefix.
 * - fmaxchange -- write greatest changes to files with this prefix.
 * - PlusCount -- count the number of new neighbours for each node.  This
 *   should be initialized beforehand.  The results will be stored here.
 */
void change_plus(const igraph_t *G,
                 const igraph_t *H,
                 const char *fchangedist,
                 const char *fmaxchange,
                 igraph_vector_t *PlusCount) {
  char *fname;
  FILE *f;
  igraph_integer_t i;
  igraph_integer_t k;
  igraph_integer_t maxdegplus;  /* greatest number of new neighbours */
  igraph_integer_t node;         /* vertex ID */
  igraph_integer_t u;
  igraph_integer_t v;
  igraph_vector_t *N;           /* all new neighbours of some node */
  igraph_vector_t nbrG;
  igraph_vector_t nbrH;
  /* All nodes with greatest number of new neighbours. */
  igraph_vector_t VPlus;
  /* The plus table records the total number of addition of edges incident */
  /* on each node.  The "plus" refers to addition.  Index of PlusTable is */
  /* node index. The element PlusTable[i] refers to a vector containing all */
  /* new neighbours of node i. */
  igraph_vector_ptr_t PlusTable;
  igraph_vit_t vit;

  igraph_vector_ptr_init(&PlusTable, 0);
  vector_of_vectors_init(&PlusTable, G, H);
  IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(&PlusTable, igraph_vector_destroy);
  assert((igraph_integer_t)igraph_vector_size(PlusCount)
         == (igraph_integer_t)igraph_vector_ptr_size(&PlusTable));

  maxdegplus = 0;
  node = 0;
  igraph_vit_create(H, igraph_vss_all(), &vit);
  /* iterate in ascending order of node ID */
  while (!IGRAPH_VIT_END(vit)) {
    v = (igraph_integer_t)IGRAPH_VIT_GET(vit);
    N = (igraph_vector_t *)VECTOR(PlusTable)[v];
    assert(node == v);
    /* neighbours of v in H; assumed to be sorted by node IDs */
    igraph_vector_init(&nbrH, 0);
    igraph_neighbors(H, &nbrH, v, IGRAPH_ALL);
    /* G has node v */
    if (v < igraph_vcount(G)) {
      /* neighbours of v in G; assumed to be sorted by node IDs */
      igraph_vector_init(&nbrG, 0);
      igraph_neighbors(G, &nbrG, v, IGRAPH_ALL);
      for (i = 0; i < igraph_vector_size(&nbrH); i++) {
        u = (igraph_integer_t)VECTOR(nbrH)[i];
        /* In graph G, node u is not a neighbour of v.  Thus in graph H, */
        /* node u is a new neighbour of v. */
        if (!igraph_vector_binsearch2(&nbrG, u)) {
          igraph_vector_push_back(N, u);
        }
      }
      igraph_vector_destroy(&nbrG);
    }
    /* node v is not in G */
    else {
      /* All the new neighbours of v are those neighbours of v in the */
      /* graph H. */
      for (i = 0; i < igraph_vector_size(&nbrH); i++) {
        u = (igraph_integer_t)VECTOR(nbrH)[i];
        igraph_vector_push_back(N, u);
      }
    }
    /* the running greatest number of new neighbours */
    if ((igraph_integer_t)igraph_vector_size(N) > maxdegplus) {
      maxdegplus = (igraph_integer_t)igraph_vector_size(N);
    }
    igraph_vector_destroy(&nbrH);
    VECTOR(*PlusCount)[node] = (igraph_integer_t)igraph_vector_size(N);
    node++;
    IGRAPH_VIT_NEXT(vit);
  }
  igraph_vit_destroy(&vit);

  /* all nodes with greatest number of new neighbours */
  igraph_vector_init(&VPlus, 0);
  for (i = 0; i < igraph_vector_ptr_size(&PlusTable); i++) {
    N = (igraph_vector_t *)VECTOR(PlusTable)[i];
    if ((igraph_integer_t)igraph_vector_size(N) == maxdegplus) {
      igraph_vector_push_back(&VPlus, i);
    }
  }
  assert(igraph_vector_size(&VPlus) > 0);
  /* output to file */
  asprintf(&fname, "%s-plus.dat", fmaxchange);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_size(&VPlus); i++) {
    v = (igraph_integer_t)VECTOR(VPlus)[i];
    N = (igraph_vector_t *)VECTOR(PlusTable)[v];
    fprintf(f, "%ld:%ld:", (long int)v, (long int)maxdegplus);
    if (igraph_vector_size(N) < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 1); k++) {
      fprintf(f, "%ld,", (long int)VECTOR(*N)[k]);
    }
    k = igraph_vector_size(N) - 1;
    fprintf(f, "%ld\n", (long int)VECTOR(*N)[k]);
  }
  fclose(f);
  free(fname);
  igraph_vector_destroy(&VPlus);

  /* output the plus table to file */
  asprintf(&fname, "%s-plus.dat", fchangedist);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_ptr_size(&PlusTable); i++) {
    N = (igraph_vector_t *)VECTOR(PlusTable)[i];
    fprintf(f, "%ld:%ld:", (long int)i, (long int)igraph_vector_size(N));
    if (igraph_vector_size(N) < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 1); k++) {
      fprintf(f, "%ld,", (long int)VECTOR(*N)[k]);
    }
    k = igraph_vector_size(N) - 1;
    fprintf(f, "%ld\n", (long int)VECTOR(*N)[k]);
  }
  fclose(f);
  free(fname);
  igraph_vector_ptr_destroy_all(&PlusTable);
}

/* Write all changes to file.
 *
 * INPUT:
 *
 * - MinusCount -- all the counts of number of deleted neighbours for each
 *   node.
 * - PlusCount -- all the counts of number of new neighbours for each node.
 * - fprefix -- write changes to a file with this prefix.
 * - max -- the maximum number of changes will be stored here.
 */
void change_total(const igraph_vector_t *MinusCount,
                  const igraph_vector_t *PlusCount,
                  const char *fprefix,
                  igraph_integer_t *max) {
  char *fname;
  FILE *f;
  igraph_integer_t count;
  igraph_integer_t countm;
  igraph_integer_t countp;
  igraph_integer_t i;

  assert(igraph_vector_size(MinusCount) == igraph_vector_size(PlusCount));
  asprintf(&fname, "%s-all.dat", fprefix);
  f = fopen(fname, "w");
  fprintf(f, "node,total change\n");
  count = 0;
  *max = 0;
  for (i = 0; i < igraph_vector_size(MinusCount); i++) {
    countm = (igraph_integer_t)VECTOR(*MinusCount)[i];
    countp = (igraph_integer_t)VECTOR(*PlusCount)[i];
    count = countm + countp;
    fprintf(f, "%ld,%ld\n", (long int)i, (long int)count);
    if (count > *max) {
      *max = count;
    }
  }
  fclose(f);
  free(fname);
}

/* Locate the region of greatest total changes.
 *
 * INPUT:
 *
 * - MinusCount -- all the counts of number of deleted neighbours for each
 *   node.
 * - PlusCount -- all the counts of number of new neighbours for each node.
 * - fprefix -- write greatest changes to a file with this prefix.
 * - max -- the greatest total change.
 */
void change_total_max(const igraph_vector_t *MinusCount,
                      const igraph_vector_t *PlusCount,
                      const char *fprefix,
                      const igraph_integer_t max) {
  char *fname;
  FILE *f;
  igraph_integer_t count;
  igraph_integer_t countm;
  igraph_integer_t countp;
  igraph_integer_t i;

  assert(igraph_vector_size(MinusCount) == igraph_vector_size(PlusCount));
  asprintf(&fname, "%s-all.dat", fprefix);
  f = fopen(fname, "w");
  fprintf(f, "node,total change\n");
  for (i = 0; i < igraph_vector_size(MinusCount); i++) {
    countm = (igraph_integer_t)VECTOR(*MinusCount)[i];
    countp = (igraph_integer_t)VECTOR(*PlusCount)[i];
    count = countm + countp;
    if (count == max) {
      fprintf(f, "%ld,%ld\n", (long int)i, (long int)count);
    }
  }
  fclose(f);
  free(fname);
}

/* All nodes in the given edge set that have the greatest degree.
 *
 * INPUT:
 *
 * - Edge -- the edge set of a simple undirected graph.
 * - VTotal -- the nodes with the greatest degree will be stored here.  This
 *   should be initialized beforehand.
 * - maxdeg -- the maximum degree will be stored here.
 */
/* void greatest_degree(Set *Edge, */
/*                      igraph_vector_t *VTotal, */
/*                      igraph_integer_t *maxdeg) { */
/*   char *edge; */
/*   igraph_t G; */
/*   igraph_integer_t vid; */
/*   igraph_vector_t E; */
/*   igraph_vector_t D; */
/*   igraph_vit_t vit; */
/*   int u; */
/*   int v; */
/*   SetIterator iter; */

/*   /\* construct a simple undirected graph from the given edge set *\/ */
/*   igraph_vector_init(&E, 0); */
/*   set_iterate(Edge, &iter); */
/*   while (set_iter_has_more(&iter)) { */
/*     edge = set_iter_next(&iter); */
/*     sscanf(edge, "%d %d", &u, &v); */
/*     igraph_vector_push_back(&E, (igraph_integer_t)u); */
/*     igraph_vector_push_back(&E, (igraph_integer_t)v); */
/*   } */
/*   igraph_create(&G, &E, 0, IGRAPH_UNDIRECTED); */
/*   igraph_simplify(&G, /\*no multiple edges*\/ 1, /\*no self-loops*\/ 1, */
/*                   /\*edge_comb*\/ 0); */

/*   /\* maximum degree and all nodes with this degree *\/ */
/*   igraph_maxdegree(&G, maxdeg, igraph_vss_all(), IGRAPH_ALL, 0); */
/*   igraph_vit_create(&G, igraph_vss_all(), &vit); */
/*   while (!IGRAPH_VIT_END(vit)) { */
/*     vid = (igraph_integer_t)IGRAPH_VIT_GET(vit); */
/*     igraph_vector_init(&D, 0); */
/*     igraph_degree(&G, &D, igraph_vss_1(vid), IGRAPH_ALL, 0); */
/*     if ((igraph_integer_t)VECTOR(D)[0] == *maxdeg) { */
/*       igraph_vector_push_back(VTotal, vid); */
/*     } */
/*     igraph_vector_destroy(&D); */
/*     IGRAPH_VIT_NEXT(vit); */
/*   } */

/*   igraph_destroy(&G); */
/*   igraph_vector_destroy(&E); */
/* } */

/* The symmetric difference of two simple undirected graphs.  We only
 * consider the symmetric difference of their edge sets.
 *
 * INPUT:
 *
 * - G -- a graph.
 * - H -- another graph.
 * - Symdif -- the symmetric difference will be stored here.  This should be
 *   initialized beforehand.
 */
/* void symmetric_difference(const igraph_t *G, */
/*                           const igraph_t *H, */
/*                           Set *Symdif) { */
/*   char *edge; */
/*   igraph_eit_t eit; */
/*   igraph_integer_t eG; */
/*   igraph_integer_t eH; */
/*   igraph_integer_t u; */
/*   igraph_integer_t v; */

/*   set_register_free_function(Symdif, free); */

/*   /\* the set difference A - B, i.e. what's in A but not in B *\/ */
/*   igraph_eit_create(G, igraph_ess_all(IGRAPH_EDGEORDER_ID), &eit); */
/*   while (!IGRAPH_EIT_END(eit)) { */
/*     eG = (igraph_integer_t)IGRAPH_EIT_GET(eit); */
/*     igraph_edge(G, eG, &u, &v); */
/*     /\* edge is not in H *\/ */
/*     if ((igraph_vcount(H) <= u) */
/*         || (igraph_vcount(H) <= v) */
/*         || (igraph_get_eid(H, &eH, u, v, IGRAPH_UNDIRECTED, 0) == -1)) { /\* BUG alert *\/ */
/*       if (u <= v) { */
/*         asprintf(&edge, "%ld %ld", (long int)u, (long int)v); */
/*       } else { */
/*         asprintf(&edge, "%ld %ld", (long int)v, (long int)u); */
/*       } */
/*       set_insert(Symdif, edge); */
/*     } */
/*     IGRAPH_EIT_NEXT(eit); */
/*   } */
/*   igraph_eit_destroy(&eit); */

/*   /\* the set difference B - A, i.e. what's in B but not in A *\/ */
/*   igraph_eit_create(H, igraph_ess_all(IGRAPH_EDGEORDER_ID), &eit); */
/*   while (!IGRAPH_EIT_END(eit)) { */
/*     eH = (igraph_integer_t)IGRAPH_EIT_GET(eit); */
/*     igraph_edge(H, eH, &u, &v); */
/*     /\* edge is not in G *\/ */
/*     if ((igraph_vcount(G) <= u) */
/*         || (igraph_vcount(G) <= v) */
/*         || (igraph_get_eid(G, &eG, u, v, IGRAPH_UNDIRECTED, 0) == -1)) { /\* BUG alert *\/ */
/*       if (u <= v) { */
/*         asprintf(&edge, "%ld %ld", (long int)u, (long int)v); */
/*       } else { */
/*         asprintf(&edge, "%ld %ld", (long int)v, (long int)u); */
/*       } */
/*       set_insert(Symdif, edge); */
/*     } */
/*     IGRAPH_EIT_NEXT(eit); */
/*   } */
/*   igraph_eit_destroy(&eit); */
/* } */

/* Initialize a vector of vectors.  This is just a vector, each of whose
 * elements is a pointer to a vector.
 *
 * - V -- pointer to an initialized vector of vectors.  The result will be
 *   stored here.
 * - G -- a graph.
 * - H -- another graph.
 */
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_t *G,
                            const igraph_t *H) {
  igraph_integer_t i;
  igraph_integer_t size;
  igraph_vector_t *p;

  if (igraph_vcount(G) <= igraph_vcount(H)) {
    size = (igraph_integer_t)igraph_vcount(H);
  } else {
    size = (igraph_integer_t)igraph_vcount(G);
  }
  for (i = 0; i < size; i++) {
    p = igraph_Calloc(1, igraph_vector_t);
    igraph_vector_init(p, 0);
    igraph_vector_ptr_push_back(V, p);
  }
}

/* Locate the region of greatest changes.  Here, we consider the node level.
 */
int main(int argc,
         char **argv) {
  char *fname;
  FILE *f;
  igraph_t G;
  igraph_t H;
  igraph_integer_t max;
  igraph_integer_t size;
  igraph_vector_t MinusCount;
  igraph_vector_t PlusCount;

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_file *felistA = arg_file0(NULL, "felistA", NULL,
                               "file containing edge list, one edge per line");
  struct arg_file *felistB = arg_file0(NULL, "felistB", NULL,
                               "file containing edge list, one edge per line");
  struct arg_str *fchangedist = arg_strn(NULL, "fchangedist", "string", 0,
                                         argc + 2,
                       "write change distributions to files with this prefix");
  struct arg_str *fmaxchange = arg_strn(NULL, "fmaxchange", "string", 0,
                                         argc + 2,
                           "write greatest changes to files with this prefix");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, felistA, felistB, fchangedist, fmaxchange, end};
  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Locate the region of greatest changes at node level.\n");
    arg_print_glossary(stdout, argtable, " %-23s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((felistA->count < 1)
      || (felistB->count < 1)
      || (fchangedist->count < 1)
      || (fmaxchange->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Locate the region of greatest changes at node level.\n");
    arg_print_glossary(stdout, argtable, " %-23s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* reconstruct graphs from their edge lists */
  asprintf(&fname, "%s", felistA->filename[0]);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, f, 0, IGRAPH_UNDIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  asprintf(&fname, "%s", felistB->filename[0]);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&H, f, 0, IGRAPH_UNDIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&H, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);

  if (igraph_vcount(&G) <= igraph_vcount(&H)) {
    size = (igraph_integer_t)igraph_vcount(&H);
  } else {
    size = (igraph_integer_t)igraph_vcount(&G);
  }
  igraph_vector_init(&MinusCount, size);
  igraph_vector_init(&PlusCount, size);
  change_plus(&G, &H, fchangedist->sval[0], fmaxchange->sval[0], &PlusCount);
  change_minus(&G, &H, fchangedist->sval[0], fmaxchange->sval[0], &MinusCount);
  change_total(&MinusCount, &PlusCount, fchangedist->sval[0], &max);
  change_total_max(&MinusCount, &PlusCount, fmaxchange->sval[0], max);

  igraph_destroy(&G);
  igraph_destroy(&H);
  igraph_vector_destroy(&MinusCount);
  igraph_vector_destroy(&PlusCount);
  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
