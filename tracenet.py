###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script MUST be run from Python.
#
# Construct a digraph from data on community matches.  The digraph is to be
# used to determine the history of each community.  Each trace is a path from a
# community birth to death, or up to the latest snapshot year.  We refer to the
# digraph as a trace network.

import argparse
import os

###################################
# helper functions
###################################

def add_missing_matches(E, M, V):
    """
    Update the edge set with fronts missing observations.  As we go along,
    we also update the vertex set.

    INPUT:

    - E -- edge set given as a dictionary.
    - M -- fronts with missing observations, given as a dictionary of lists.
    - V -- a vertex set to update.
     """
    for u in M:
        # add one front with missing observation
        i = 1
        v = u + "," + str(i)
        update_edge_set(E, V, (u, v), M)
        # add the remaining fronts
        for _ in M[u][1:]:
            v = u + "," + str(i)
            w = u + "," + str(i + 1)
            update_edge_set(E, V, (v, w), M)
            i += 1
        # has indirect continuation after missing observations?
        # take care of resumption of observations
        for k in E[u].keys():
            # community at y has observation at y + i
            if not k.startswith(u):
                v = u + "," + str(i)
                w = k
                del E[u][k]
                update_edge_set(E, V, (v, w), M)

def birth(si, dirname):
    """
    Get all the birth of communities in the given snapshot index.  The birth
    or beginning of a community is a community in snapshot y without
    a matching to some community in previous years.

    INPUT:

    - si -- a snapshot index for which data are available.  If there are
      community births in the given snapshot, we return a list of all such
      communities.  Otherwise, we return the empty list.
    - dirname -- path to directory with match data.
    """
    f = open(os.path.join(dirname, "birth", "birth-%d.dat" % si), "r")
    n = int(f.readline().strip())  # first line is number of births
    # zero births
    if n < 1:
        f.close()
        return list()
    # at least one birth
    B = list()
    for line in f:
        yi = line.strip()  # snapshot/community index
        B.append(yi)
    f.close()
    return B

def extract_vertices(match):
    """
    Extract a pair of nodes from the given match data.  The match data follows
    the format:

        si1 ci1 si2 ci2 sim pchange

    The snapshot index si1 is assumed to be the same as or earlier than si2.
    The match data format above means that the community with index ci1 at
    snapshot index si1 matches the community with index ci2 at snapshot index
    ci2.  The key "sim" is the similarity score between the two communities.
    The percentage change in community size from si1 to si2 is given by
    pchange.  The first node is taken to be the string "si1 ci1" and the
    second node is the string "si2 ci2".

    INPUT:

    - match -- a line of match data.
    """
    D = match.split()
    u = " ".join([D[0], D[1]])
    v = " ".join([D[2], D[3]])
    return (u, v)

def first_sindex(sindex, dirname):
    """
    The first snapshot index with matching data.

    INPUT:

    - sindex -- list of snapshot indices in nondecreasing order.
    - dirname -- path to directory with match data.
    """
    i = 0
    while not os.path.isfile(os.path.join(dirname,"match-%d.dat" % sindex[i])):
        i += 1
        if i >= len(sindex):
            raise ValueError("Expected matching data, but received none")
    return i

def get_sindex(fname):
    """
    Get all the snapshot indices from the given file.  The resulting list of
    indices will be sorted in nondecreasing order.

    INPUT:

    - fname -- file with all the snapshot indices, one per line.

    OUTPUT:

    List of all snapshot indices, sorted in nondecreasing order.
    """
    Sindex = list()
    f = open(fname, "r")
    for line in f:
        Sindex.append(int(line.strip()))
    f.close()
    return sorted(Sindex)

def update_edge_set(E, V, match, M):
    """
    Update the given edge set with the provided match data.  Each line of
    match data follows the format:

    si1 ci1 si2 ci2 sim p

    where "si1 ci1" denotes community index "ci1" in snapshot index "si1"
    (and similarly for "si2 ci2"), "sim" denotes the measure of overlap
    between the given two communities, and p is the percentage change in
    community size.

    WARNING:  This function will modify its arguments.

    INPUT:

    - E -- an edge set to update.  The edge set is assumed to be implemented
      as a dictionary.
    - V -- a vertex set to update.
    - match -- a line of match data read from a match file; or a tuple of
      match data.  In the case of tuple, we assume that all elements are
      distinct.
    - M -- dictionary of matches denoting fronts with missing observations.
    """
    u = None
    v = None
    if isinstance(match, str):
        u, v = extract_vertices(match)
        # match denoting front with missing observation
        if u == v:
            if u in M:
                M[u].append(v)
            else:
                M.setdefault(u, [v])
            return
    elif isinstance(match, tuple):
        u, v = match
    else:
        raise ValueError("Invalid match data")
    # front with observation
    if u in E:
        E[u].setdefault(v, True)
    else:
        E.setdefault(u, {v: True})
        update_vertex_set(V, u)
    update_vertex_set(V, v)

def update_vertex_set(V, v):
    """
    Update the given vertex set with the provided vertex.

    WARNING:  This function will modify its arguments.

    INPUT:

    - V -- a vertex set to update.
    - v -- a node.
    """
    if v not in V:
        V.setdefault(v, len(V))

def write_edges(E, V, fname):
    """
    Write to a file the given edge list.

    INPUT:

    - E -- edge set.
    - V -- vertex set; this is the mapping from year/community index to
      vertex ID.
    - fname -- write the trace network to this file.
    """
    Node = set(V.keys())
    f = open(fname, "w")
    # output the regular edges, i.e. not self-loops
    for u in sorted(E.keys()):
        if u in Node:
            Node.remove(u)
        for v in sorted(E[u].keys()):
            f.write("%d %d\n" % (V[u], V[v]))
            if v in Node:
                Node.remove(v)
    # output the self-loops; we represent isolated nodes as self-loops
    for u in Node:
        f.write("%d %d\n" % (V[u], V[u]))
    f.flush()
    os.fsync(f.fileno())
    f.close()

def write_vertices(V, fname):
    """
    Write to a file the given mapping from snapshot/community index to vertex
    ID.

    INPUT:

    - V -- vertex set.
    - fname -- write the vertex set to this file.
    """
    f = open(fname, "w")
    for key in sorted(V.keys()):
        f.write("%s,%d\n" % (key, V[key]))
    f.flush()
    os.fsync(f.fileno())
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Construct a trace network.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="file with snapshot indices")
parser.add_argument("--matchdir", metavar="path", required=True,
                    help="directory with match files")
parser.add_argument("--edgelist", metavar="file", required=True,
                    help="write the trace network to this file")
parser.add_argument("--nodelist", metavar="file", required=True,
                    help="write the nodes to this file")
args = parser.parse_args()
fsindex = args.fsindex
matchdir = args.matchdir
edgelist = args.edgelist
nodelist = args.nodelist

# determine the first snapshot index having matching data
sindex = get_sindex(fsindex)
i = first_sindex(sindex, matchdir)
# mapping of year/community index to vertex ID
V = dict()
# Mapping of matching data to edge.  Each edge follows the format (u, v),
# where each of u and v is a string of year/community index.
E = dict()
# all community births without matches
B = list()
# matches denoting fronts with mising observations
M = dict()
# build vertex and edge sets
for si in range(i, len(sindex)):
    # process all match data
    f = open(os.path.join(matchdir, "match-%d.dat" % sindex[si]), "r")
    for line in f:
        match = line.strip()
        update_edge_set(E, V, match, M)
    f.close()
    # Process all community births.  In some cases, it is possible that a
    # new community is born at snapshot index y and subsequently has
    # no observations at all.  It is hence considered dead from y + 1
    # onwards.  A case is when delta = 0.
    for yi in birth(sindex[i - 1], matchdir):
        if yi not in V:
            B.append(yi)
            update_vertex_set(V, yi)
# Process all community births in the latest snapshot.  In some cases, it is
# possible that a new community is born at snapshot si and subsequently has no
# observations at all.  It is hence considered dead from y + 1 onwards.
# A case is when delta = 0.
for yi in birth(sindex[-1], matchdir):
    if yi not in V:
        B.append(yi)
        update_vertex_set(V, yi)
add_missing_matches(E, M, V)
write_edges(E, V, edgelist)
write_vertices(V, nodelist)
