###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile
#
# Count the number of parameter settings that result in a statistically
# significant distribution of lifespan.  By statistically significant, we
# mean that when fitting the empirical distribution to a power law, we find
# that with p-value > 0.1 the empirical distribution follows a power law.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
fname <- args[1]  # file having a table of parameter settings and p-values

p <- 0.1  # cut-off value for statistical significance
T <- read.csv(file=fname, header=TRUE, sep=",")
density <- nrow(T[T$pvalue > p,]) / nrow(T)
write(paste(density, sep=""), stdout())
