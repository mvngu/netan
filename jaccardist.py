###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Cumulative distribution of Jaccard coefficients.

import argparse
import os

###################################
# helper functions
###################################

def cumulative_frequency(entropydir, maxlifespan, theta):
    """
    The cumulative frequency of Jaccard coefficients.  For each Jaccard
    coefficient J, we count how many dynamic communities having a Jaccard
    coefficient *at most* J during their history.  We also count how many
    dynamic communities with a Jaccard coefficient *at least* J during their
    history.

    INPUT:

    - entropydir -- path to directory with files containing H values.
    - maxlifespan -- maximum lifespan of a dynamic community.
    - theta -- the match threshold.
    """
    JDB = []  # database of time series of Jaccard coefficies
    for i in range(2, maxlifespan + 1):
        jaccard_time_series(entropydir, i, JDB)
    ndycom = len(JDB)  # number of dynamic communities altogether
    delta = 0.00025    # increment
    jac = theta        # focal Jaccard coefficient
    maxjac = 1.0       # maximum Jaccard coefficient
    Fmax = []          # frequency table; at most J
    Fmin = []          # frequency table; at least J
    while jac <= maxjac:
        count_min = 0
        count_max = 0
        # inspect sequence of Jaccard coefficients of each dynamic community
        for Series in JDB:
            for jc in Series:
                if jc <= jac:
                    count_max += 1
                    break
            for jc in Series:
                if jc >= jac:
                    count_min += 1
                    break
        normcount = float(count_max) / float(ndycom)
        Fmax.append((jac, normcount))
        normcount = float(count_min) / float(ndycom)
        Fmin.append((jac, normcount))
        jac += delta
    return (Fmax, Fmin)

def jaccard_time_series(entropydir, lifespan, JDB):
    """
    All time series of Jaccard coefficients for dynamic communities with the
    given lifespan.  Each dynamic community has a sequence of Jaccard
    coefficients that represents its changes in size throughout its history.

    WARNING: This function modifies its arguments.

    INPUT:

    - entropydir -- path to directory with files containing H values.
    - lifespan -- the lifespan of all dynamic communities we want to consider.
    - JDB -- the database of time series of Jaccard coefficients.  The results
      will be added to this database.
    """
    fname = os.path.join(entropydir, "entropy-%d.dat" % lifespan)
    f = open(fname, "r")
    for line in f:
        J = line.strip().split(";")[1]  # ignore H value
        J = map(float, J.split(","))
        JDB.append(J)
    f.close()

def write_distribution(Distmax, Distmin, outdir):
    """
    Write under the given path the cumulative distributions of Jaccard
    coefficients.

    INPUT:

    - Distmax -- the cumulative distribution of maximum Jaccard coefficients.
    - Distmin -- the cumulative distribution of minimum Jaccard coefficients.
    - outdir -- the path under which to write results.
    """
    if not os.path.isdir(outdir):
        os.makedirs(outdir)
    # write the maximum distribution
    fname = os.path.join(outdir, "jaccard-cumdist-max.csv")
    f = open(fname, "w")
    f.write("maximum Jaccard coefficient,normalized count\n")  # file header
    for jac, count in Distmax:
        f.write("%.10f,%.10f\n" % (jac, count))
    f.close()
    # write the minimum distribution
    fname = os.path.join(outdir, "jaccard-cumdist-min.csv")
    f = open(fname, "w")
    f.write("minimum Jaccard coefficient,normalized count\n")  # file header
    for jac, count in Distmin:
        f.write("%.10f,%.10f\n" % (jac, count))
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Cumulative distribution of Jaccard coefficients.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--entropydir", metavar="path", required=True,
                    help="path to directory with files containing H values")
parser.add_argument("--maxlifespan", metavar="int", required=True,
                    help="maximum lifespan of a dynamic community")
parser.add_argument("--theta", metavar="float", required=True,
                    help="match threshold")
parser.add_argument("--outdir", metavar="path", required=True,
                    help="path under which we write results")
args = parser.parse_args()

# get the command line arguments
entropydir = args.entropydir
maxlifespan = int(args.maxlifespan)
theta = float(args.theta)
outdir = args.outdir

Distmax, Distmin = cumulative_frequency(entropydir, maxlifespan, theta)
write_distribution(Distmax, Distmin, outdir)
