def entropy(S):
    return -1 * (sum(map(lambda x: x * log(x), S)) / len(S))

def avg_jaccard(S):
    return mean(S)

def sample_jaccard(theta, n):
    S = []
    for _ in range(n):
        r = random()
        while r <= theta:
            r = random()
        S.append(r)
    return S

theta = 0.99
N = 1000
L = 62
D = []
Davg = []
for ell in range(2, L + 1):
    Havg = []
    for i in range(1, N + 1):
        S = sample_jaccard(theta, ell)
        H = RR(entropy(S))
        ## H = RR(avg_jaccard(S))
        Havg.append(H)
        D.append((ell, H))
    Davg.append((ell, RR(mean(Havg))))

ident = "H"  # "avgjaccard"
hed = "H"  # "avg Jaccard"
f = open("experiment-lifespan-vs-%s_theta-%.2f_scatter.csv" % (ident, theta),
         "w")
f.write("lifespan,%s\n" % hed)
for ell, H in D:
    f.write("%d,%.10f\n" % (ell, H))
f.close()

f = open("experiment-lifespan-vs-%s_theta-%.2f_avg.csv" % (ident, theta), "w")
f.write("lifespan,%s\n" % hed)
for ell, H in Davg:
    f.write("%d,%.10f\n" % (ell, H))
f.close()
