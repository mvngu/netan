/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* Locate the region with the greatest changes in a sequence of time ordered */
/* line graphs. */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <ctype.h>
#include <igraph.h>
#include <libcalg.h>
#include <stdio.h>

void change_minus(const igraph_t *G,
                  const igraph_t *H,
                  const igraph_t *lineG,
                  const igraph_t *lineH,
                  const char *fchangedist,
                  const char *fmaxchange,
                  Trie *T,
                  HashTable *RT,
                  igraph_vector_t *MinusCount);
void change_plus(const igraph_t *G,
                 const igraph_t *H,
                 const igraph_t *lineG,
                 const igraph_t *lineH,
                 const char *fchangedist,
                 const char *fmaxchange,
                 Trie *T,
                 HashTable *RT,
                 igraph_vector_t *PlusCount);
void change_total(HashTable *RT,
                  const igraph_vector_t *MinusCount,
                  const igraph_vector_t *PlusCount,
                  const char *fprefix,
                  igraph_integer_t *max);
void change_total_max(HashTable *RT,
                      const igraph_vector_t *MinusCount,
                      const igraph_vector_t *PlusCount,
                      const char *fprefix,
                      const igraph_integer_t max);
int has_neighbor(const igraph_integer_t u,
                 const igraph_integer_t v,
                 const igraph_vector_t *V);
void hash_node(Trie *T,
               HashTable *RT,
               const igraph_t *G,
               const igraph_t *H,
               const igraph_t *lineG,
               const igraph_t *lineH);
void linegraph_neighbors(const igraph_t *G,
                         const igraph_t *LG,
                         const igraph_integer_t vL,
                         igraph_vector_t *Nbr);
igraph_integer_t table_index(Trie *T,
                             const igraph_integer_t u,
                             const igraph_integer_t v);
void table_node(HashTable *RT,
                igraph_integer_t k,
                igraph_integer_t *u,
                igraph_integer_t *v);
/* void table_reverse_lookup(Trie *T, */
/*                           const igraph_integer_t n, */
/*                           const igraph_t *G, */
/*                           const igraph_t *H, */
/*                           const igraph_t *lineG, */
/*                           const igraph_t *lineH, */
/*                           igraph_integer_t *u, */
/*                           igraph_integer_t *v); */
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_integer_t size);

/* The total number of deletion of edges.  Let D be the set of all
 * neighbours of v in AdjA that are not in AdjB.  Then members of D are
 * neighbours that are deleted in the transformation from AdjA to AdjB.
 * The cardinality of D therefore counts the number of edges incident on
 * v that are deleted in AdjB.
 *
 * INPUT:
 *
 * - G -- a simple undirected graph.  This graph is assumed to be a snapshot of
 *   some network at time t.
 * - H -- another simple undirected graph.  This graph is assumed to be a
 *   snapshot of some network at time t + 1.
 * - lineG -- the line graph of G.
 * - lineH -- the line graph of H.
 * - fchangedist -- write change distributions to files with this prefix.
 * - fmaxchange -- write greatest changes to files with this prefix.
 * - T -- a mapping of nodes in the given line graphs to unique nonnegative
 *   integers.  This is the reverse map of RT.
 * - RT -- a reverse map from a unique nonnegative integer to a node (u,v).
 *   This is the reverse map of T.
 * - MinusCount -- count the number of deleted neighbours for each node.
 *   This should be initialized beforehand.  The results will be stored here.
 */
void change_minus(const igraph_t *G,
                  const igraph_t *H,
                  const igraph_t *lineG,
                  const igraph_t *lineH,
                  const char *fchangedist,
                  const char *fmaxchange,
                  Trie *T,
                  HashTable *RT,
                  igraph_vector_t *MinusCount) {
  char *fname;
  FILE *f;
  igraph_integer_t a;
  igraph_integer_t b;
  igraph_integer_t deg;
  igraph_integer_t eid;
  igraph_integer_t i;
  igraph_integer_t k;
  igraph_integer_t maxdegminus;  /* greatest number of neighbours deleted */
  igraph_integer_t size;
  igraph_integer_t u;           /* a node */
  igraph_integer_t v;           /* another node */
  igraph_integer_t vL;          /* node in line graph */
  igraph_vector_t *N;           /* all deleted neighbours of some node */
  igraph_vector_t NbrG;         /* neighbours in line graph of G */
  igraph_vector_t NbrH;         /* neighbours in line graph of H */
  /* All nodes with greatest number of neighbours deleted. */
  igraph_vector_t VMinus;
  /* The minus table records the total number of deletion of edges incident */
  /* on each node.  The "minus" refers to deletion.  Index of MinusTable is */
  /* node index. The element MinusTable[i] refers to a vector containing all */
  /* deleted neighbours of node i. */
  igraph_vector_ptr_t MinusTable;
  igraph_vit_t vit;

  size = (igraph_integer_t)trie_num_entries(T);
  igraph_vector_ptr_init(&MinusTable, 0);
  vector_of_vectors_init(&MinusTable, size);
  IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(&MinusTable, igraph_vector_destroy);
  assert((igraph_integer_t)igraph_vector_size(MinusCount)
         == (igraph_integer_t)igraph_vector_ptr_size(&MinusTable));

  maxdegminus = 0;
  igraph_vit_create(lineG, igraph_vss_all(), &vit);
  while (!IGRAPH_VIT_END(vit)) {
    vL = (igraph_integer_t)IGRAPH_VIT_GET(vit);
    igraph_edge(G, vL, &u, &v);  /* a node in L(G) is an edge ID in G */
    k = table_index(T, u, v);
    N = (igraph_vector_t *)VECTOR(MinusTable)[k];
    /* neighbours of vL in L(G); assumed to be sorted by node IDs */
    igraph_vector_init(&NbrG, 0);
    linegraph_neighbors(G, lineG, vL, &NbrG);
    /* L(H) has node (u,v) */
    if ((u < igraph_vcount(H))
        && (v < igraph_vcount(H))) {
      igraph_get_eid(H, &eid, u, v, IGRAPH_UNDIRECTED, /*error=*/ 0);
    }
    if ((u < igraph_vcount(H))
        && (v < igraph_vcount(H))
        && (eid != -1)) {
      /* neighbours of (u,v) in L(H); assumed to be sorted by node IDs */
      igraph_vector_init(&NbrH, 0);
      linegraph_neighbors(H, lineH, eid, &NbrH);
      for (i = 0; i < igraph_vector_size(&NbrG); i += 2) {
        a = (igraph_integer_t)VECTOR(NbrG)[i];
        b = (igraph_integer_t)VECTOR(NbrG)[i + 1];
        assert(a < b);
        /* In the line graph L(H), node (a,b) is not a neighbour of (u,v). */
        /* Thus in the graph L(H), an edge between (a,b) and (u,v) has been */
        /* deleted. */
        if (!has_neighbor(a, b, &NbrH)) {
          igraph_vector_push_back(N, a);
          igraph_vector_push_back(N, b);
        }
      }
      igraph_vector_destroy(&NbrH);
    }
    /* node (u,v) is not in L(H) */
    else {
      /* All the deleted neighbours of (u,v) are those neighbours of (u,v) */
      /* in the line graph L(G). */
      for (i = 0; i < igraph_vector_size(&NbrG); i += 2) {
        a = (igraph_integer_t)VECTOR(NbrG)[i];
        b = (igraph_integer_t)VECTOR(NbrG)[i + 1];
        assert(a < b);
        igraph_vector_push_back(N, a);
        igraph_vector_push_back(N, b);
      }
    }
    /* the running greatest number of neighbours deleted */
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    if (deg > maxdegminus) {
      maxdegminus = deg;
    }
    igraph_vector_destroy(&NbrG);
    VECTOR(*MinusCount)[k] = deg;
    IGRAPH_VIT_NEXT(vit);
  }
  igraph_vit_destroy(&vit);

  /* All nodes with the greatest number of neighbours deleted.  We just */
  /* store the IDs of the nodes, so that we could retrieve the actual nodes */
  /* later on. */
  igraph_vector_init(&VMinus, 0);
  for (i = 0; i < igraph_vector_ptr_size(&MinusTable); i++) {
    N = (igraph_vector_t *)VECTOR(MinusTable)[i];
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    if (deg == maxdegminus) {
      igraph_vector_push_back(&VMinus, i);
    }
  }
  assert(igraph_vector_size(&VMinus) > 0);
  /* output to file */
  asprintf(&fname, "%s-minus.dat", fmaxchange);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_size(&VMinus); i++) {
    vL = (igraph_integer_t)VECTOR(VMinus)[i];
    N = (igraph_vector_t *)VECTOR(MinusTable)[vL];
    table_node(RT, vL, &u, &v);
    fprintf(f, "%ld %ld:%ld:",
            (long int)u, (long int)v, (long int)maxdegminus);
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    if (deg < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 2); k += 2) {
      fprintf(f, "%ld %ld,",
              (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
    }
    k = igraph_vector_size(N) - 2;
    fprintf(f, "%ld %ld\n",
            (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
  }
  fclose(f);
  free(fname);
  igraph_vector_destroy(&VMinus);

  /* output the minus table to file */
  asprintf(&fname, "%s-minus.dat", fchangedist);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_ptr_size(&MinusTable); i++) {
    N = (igraph_vector_t *)VECTOR(MinusTable)[i];
    table_node(RT, i, &u, &v);
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    fprintf(f, "%ld %ld:%ld:", (long int)u, (long int)v, (long int)deg);
    if (deg < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 2); k += 2) {
      fprintf(f, "%ld %ld,",
              (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
    }
    k = igraph_vector_size(N) - 2;
    fprintf(f, "%ld %ld\n",
            (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
  }
  fclose(f);
  free(fname);

  igraph_vector_ptr_destroy_all(&MinusTable);
}

/* The total number of addition of edges.  Let A be the set of all neighbours
 * of v in AdjB that are not in AdjA.  Then members of A are new neighbours of
 * v in the transformation from AdjA to AdjB.  The cardinality of A therefore
 * counts the number of new edges incident on v.
 *
 * INPUT:
 *
 * - G -- a simple undirected graph.  This graph is assumed to be a snapshot of
 *   some network at time t.
 * - H -- another simple undirected graph.  This graph is assumed to be a
 *   snapshot of some network at time t + 1.
 * - lineG -- the line graph of G.
 * - lineH -- the line graph of H.
 * - fchangedist -- write change distributions to files with this prefix.
 * - fmaxchange -- write greatest changes to files with this prefix.
 * - T -- a mapping of nodes in the given line graphs to unique nonnegative
 *   integers.  This is the reverse map of RT.
 * - RT -- a reverse map from a unique nonnegative integer to a node (u,v).
 *   This is the reverse map of T.
 * - PlusCount -- count the number of new neighbours for each node.  This
 *   should be initialized beforehand.  The results will be stored here.
 */
void change_plus(const igraph_t *G,
                 const igraph_t *H,
                 const igraph_t *lineG,
                 const igraph_t *lineH,
                 const char *fchangedist,
                 const char *fmaxchange,
                 Trie *T,
                 HashTable *RT,
                 igraph_vector_t *PlusCount) {
  char *fname;
  FILE *f;
  igraph_integer_t a;
  igraph_integer_t b;
  igraph_integer_t deg;
  igraph_integer_t eid;
  igraph_integer_t i;
  igraph_integer_t k;
  igraph_integer_t maxdegplus;  /* greatest number of new neighbours */
  igraph_integer_t size;
  igraph_integer_t u;           /* a node */
  igraph_integer_t v;           /* another node */
  igraph_integer_t vL;          /* node in line graph */
  igraph_vector_t *N;           /* all new neighbours of some node */
  igraph_vector_t NbrG;         /* neighbours in line graph of G */
  igraph_vector_t NbrH;         /* neighbours in line graph of H */
  /* All nodes with greatest number of new neighbours. */
  igraph_vector_t VPlus;
  /* The plus table records the total number of addition of edges incident */
  /* on each node in a line graph.  The "plus" refers to addition.  Index */
  /* of PlusTable is node index in a line graph.  The element PlusTable[i] */
  /* refers to a vector containing all new neighbours of node i. */
  igraph_vector_ptr_t PlusTable;
  igraph_vit_t vit;

  size = (igraph_integer_t)trie_num_entries(T);
  igraph_vector_ptr_init(&PlusTable, 0);
  vector_of_vectors_init(&PlusTable, size);
  IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(&PlusTable, igraph_vector_destroy);
  assert((igraph_integer_t)igraph_vector_size(PlusCount)
         == (igraph_integer_t)igraph_vector_ptr_size(&PlusTable));

  maxdegplus = 0;
  igraph_vit_create(lineH, igraph_vss_all(), &vit);
  while (!IGRAPH_VIT_END(vit)) {
    vL = (igraph_integer_t)IGRAPH_VIT_GET(vit);
    igraph_edge(H, vL, &u, &v);  /* a node in L(H) is an edge ID in H */
    k = table_index(T, u, v);
    N = (igraph_vector_t *)VECTOR(PlusTable)[k];
    /* neighbours of vL in L(H); assumed to be sorted by node IDs */
    igraph_vector_init(&NbrH, 0);
    linegraph_neighbors(H, lineH, vL, &NbrH);
    /* L(G) has node (u,v) */
    if ((u < igraph_vcount(G))
        && (v < igraph_vcount(G))) {
      igraph_get_eid(G, &eid, u, v, IGRAPH_UNDIRECTED, /*error=*/ 0);
    }
    if ((u < igraph_vcount(G))
        && (v < igraph_vcount(G))
        && (eid != -1)) {
      /* neighbours of (u,v) in L(G); assumed to be sorted by node IDs */
      igraph_vector_init(&NbrG, 0);
      linegraph_neighbors(G, lineG, eid, &NbrG);
      for (i = 0; i < igraph_vector_size(&NbrH); i += 2) {
        a = (igraph_integer_t)VECTOR(NbrH)[i];
        b = (igraph_integer_t)VECTOR(NbrH)[i + 1];
        assert(a < b);
        /* In the line graph L(G), node (a,b) is not a neighbour of (u,v). */
        /* Thus in the graph L(H), node (a,b) is a new neighbour of (u,v). */
        if (!has_neighbor(a, b, &NbrG)) {
          igraph_vector_push_back(N, a);
          igraph_vector_push_back(N, b);
        }
      }
      igraph_vector_destroy(&NbrG);
    }
    /* node (u,v) is not in G */
    else {
      /* All the new neighbours of (u,v) are those neighbours of (u,v) in */
      /* the line graph L(H). */
      for (i = 0; i < igraph_vector_size(&NbrH); i += 2) {
        a = (igraph_integer_t)VECTOR(NbrH)[i];
        b = (igraph_integer_t)VECTOR(NbrH)[i + 1];
        assert(a < b);
        igraph_vector_push_back(N, a);
        igraph_vector_push_back(N, b);
      }
    }
    /* the running greatest number of new neighbours */
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    if (deg > maxdegplus) {
      maxdegplus = deg;
    }
    igraph_vector_destroy(&NbrH);
    VECTOR(*PlusCount)[k] = deg;
    IGRAPH_VIT_NEXT(vit);
  }
  igraph_vit_destroy(&vit);

  /* All nodes with the greatest number of new neighbours.  We just store */
  /* the IDs of the nodes, so that we could retrieve the actual nodes later. */
  igraph_vector_init(&VPlus, 0);
  for (i = 0; i < igraph_vector_ptr_size(&PlusTable); i++) {
    N = (igraph_vector_t *)VECTOR(PlusTable)[i];
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    if (deg == maxdegplus) {
      igraph_vector_push_back(&VPlus, i);
    }
  }
  assert(igraph_vector_size(&VPlus) > 0);
  /* output to file */
  asprintf(&fname, "%s-plus.dat", fmaxchange);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_size(&VPlus); i++) {
    vL = (igraph_integer_t)VECTOR(VPlus)[i];
    N = (igraph_vector_t *)VECTOR(PlusTable)[vL];
    table_node(RT, vL, &u, &v);
    fprintf(f, "%ld %ld:%ld:", (long int)u, (long int)v, (long int)maxdegplus);
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    if (deg < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 2); k += 2) {
      fprintf(f, "%ld %ld,",
              (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
    }
    k = igraph_vector_size(N) - 2;
    fprintf(f, "%ld %ld\n",
            (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
  }
  fclose(f);
  free(fname);
  igraph_vector_destroy(&VPlus);

  /* output the plus table to file */
  asprintf(&fname, "%s-plus.dat", fchangedist);
  f = fopen(fname, "w");
  fprintf(f, "node:total neighbours:list of neighbours\n");
  for (i = 0; i < igraph_vector_ptr_size(&PlusTable); i++) {
    N = (igraph_vector_t *)VECTOR(PlusTable)[i];
    table_node(RT, i, &u, &v);
    deg = (igraph_integer_t)igraph_vector_size(N) / 2;
    fprintf(f, "%ld %ld:%ld:", (long int)u, (long int)v, (long int)deg);
    if (deg < 1) {
      fprintf(f, "\n");
      continue;
    }
    for (k = 0; k < (igraph_vector_size(N) - 2); k += 2) {
      fprintf(f, "%ld %ld,",
              (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
    }
    k = igraph_vector_size(N) - 2;
    fprintf(f, "%ld %ld\n",
            (long int)VECTOR(*N)[k], (long int)VECTOR(*N)[k + 1]);
  }
  fclose(f);
  free(fname);

  igraph_vector_ptr_destroy_all(&PlusTable);
}

/* Write all changes to file.
 *
 * INPUT:
 *
 * - G -- a simple undirected graph.  This graph is assumed to be a snapshot of
 *   some network at time t.
 * - H -- another simple undirected graph.  This graph is assumed to be a
 *   snapshot of some network at time t + 1.
 * - lineG -- the line graph of G.
 * - lineH -- the line graph of H.
 * - RT -- a reverse map from a unique nonnegative integer to a node (u,v).
 * - MinusCount -- all the counts of number of deleted neighbours for each
 *   node.
 * - PlusCount -- all the counts of number of new neighbours for each node.
 * - fprefix -- write changes to a file with this prefix.
 * - max -- the maximum number of changes will be stored here.
 */
void change_total(HashTable *RT,
                  const igraph_vector_t *MinusCount,
                  const igraph_vector_t *PlusCount,
                  const char *fprefix,
                  igraph_integer_t *max) {
  char *fname;
  FILE *f;
  igraph_integer_t count;
  igraph_integer_t countm;
  igraph_integer_t countp;
  igraph_integer_t i;
  igraph_integer_t u;
  igraph_integer_t v;

  assert(igraph_vector_size(MinusCount) == igraph_vector_size(PlusCount));
  asprintf(&fname, "%s-all.dat", fprefix);
  f = fopen(fname, "w");
  fprintf(f, "node,total change\n");
  *max = 0;
  for (i = 0; i < (igraph_integer_t)hash_table_num_entries(RT); i++) {
    table_node(RT, i, &u, &v);
    countm = (igraph_integer_t)VECTOR(*MinusCount)[i];
    countp = (igraph_integer_t)VECTOR(*PlusCount)[i];
    count = countm + countp;
    fprintf(f, "%ld %ld,%ld\n", (long int)u, (long int)v, (long int)count);
    if (count > *max) {
      *max = count;
    }
  }
  fclose(f);
  free(fname);
}

/* Locate the region of greatest total changes.
 *
 * INPUT:
 *
 * - MinusCount -- all the counts of number of deleted neighbours for each
 *   node.
 * - PlusCount -- all the counts of number of new neighbours for each node.
 * - fprefix -- write greatest changes to a file with this prefix.
 * - max -- the greatest total change.
 */
void change_total_max(HashTable *RT,
                      const igraph_vector_t *MinusCount,
                      const igraph_vector_t *PlusCount,
                      const char *fprefix,
                      const igraph_integer_t max) {
  char *fname;
  FILE *f;
  igraph_integer_t count;
  igraph_integer_t countm;
  igraph_integer_t countp;
  igraph_integer_t i;
  igraph_integer_t u;
  igraph_integer_t v;

  assert(igraph_vector_size(MinusCount) == igraph_vector_size(PlusCount));
  asprintf(&fname, "%s-all.dat", fprefix);
  f = fopen(fname, "w");
  fprintf(f, "node,total change\n");
  for (i = 0; i < (igraph_integer_t)hash_table_num_entries(RT); i++) {
    table_node(RT, i, &u, &v);
    countm = (igraph_integer_t)VECTOR(*MinusCount)[i];
    countp = (igraph_integer_t)VECTOR(*PlusCount)[i];
    count = countm + countp;
    if (count == max) {
      fprintf(f, "%ld %ld,%ld\n", (long int)u, (long int)v, (long int)count);
    }
  }
  fclose(f);
  free(fname);
}

/* Whether the given node in a line graph is in the given vector of neighbours.
 *
 * INPUT:
 *
 * - u, v -- a node of a line graph L(G).  This means that u and v are nodes
 *   in the original graph G.
 * - V -- a vector of nodes belonging to some line graph.  This means that
 *   the first two elements form a node, the second pair of elements form
 *   another node, etc.
 */
int has_neighbor(const igraph_integer_t u,
                 const igraph_integer_t v,
                 const igraph_vector_t *V) {
  igraph_integer_t a;
  igraph_integer_t b;
  igraph_integer_t i;

  assert(u < v);
  for (i = 0; i < igraph_vector_size(V); i += 2) {
    a = (igraph_integer_t)VECTOR(*V)[i];
    b = (igraph_integer_t)VECTOR(*V)[i + 1];
    assert(a < b);
    if ((u == a)
        && (v == b)) {
      return 1;  /* true */
    }
  }
  return 0;      /* false */
}

/* Map each node in a line graph to a unique nonnegative integer.
 *
 * INPUT:
 *
 * - T -- a mapping of node (u,v) to a unique nonnegative integer.  This is
 *   the reverse map of RT.
 * - RT -- a reverse map from a unique nonnegative integer to a node (u,v).
 *   This is the reverse map of T.
 * - G -- a graph.
 * - H -- another graph.
 * - lineG -- the line graph of G.
 * - lineH -- the line graph of H.
 */
void hash_node(Trie *T,
               HashTable *RT,
               const igraph_t *G,
               const igraph_t *H,
               const igraph_t *lineG,
               const igraph_t *lineH) {
  char *edge;
  char *edgecpy;
  igraph_integer_t i;
  igraph_integer_t u;
  igraph_integer_t v;
  igraph_integer_t vL;
  igraph_vit_t vit;
  int *key;
  int *val;

  /* map node in L(G) to an index */
  i = 0;
  igraph_vit_create(lineG, igraph_vss_all(), &vit);
  while (!IGRAPH_VIT_END(vit)) {
    /* node in L(G) is edge ID in G */
    vL = (igraph_integer_t)IGRAPH_VIT_GET(vit);
    igraph_edge(G, vL, &u, &v);
    assert(u != v);
    if (u < v) {
      asprintf(&edge, "%ld %ld", (long int)u, (long int)v);
    } else {
      asprintf(&edge, "%ld %ld", (long int)v, (long int)u);
    }
    asprintf(&edgecpy, "%s", edge);
    val = (int *)malloc(sizeof(int));
    *val = (int)i;
    assert(trie_insert(T, edge, val) != 0);
    key = (int *)malloc(sizeof(int));
    *key = (int)i;
    assert(hash_table_insert(RT, key, edgecpy) != 0);
    /* free(edge); */
    /* free(edgecpy); */
    /* free(val); */
    /* free(key); */
    i++;
    IGRAPH_VIT_NEXT(vit);
  }
  igraph_vit_destroy(&vit);
  assert(trie_num_entries(T) == i);

  /* map node in L(H) to an index */
  igraph_vit_create(lineH, igraph_vss_all(), &vit);
  while (!IGRAPH_VIT_END(vit)) {
    /* node in L(H) is edge ID in H */
    vL = (igraph_integer_t)IGRAPH_VIT_GET(vit);
    igraph_edge(H, vL, &u, &v);
    assert(u != v);
    if (u < v) {
      asprintf(&edge, "%ld %ld", (long int)u, (long int)v);
    } else {
      asprintf(&edge, "%ld %ld", (long int)v, (long int)u);
    }
    asprintf(&edgecpy, "%s", edge);
    /* node (u,v) has not been indexed */
    if (trie_lookup(T, edge) == TRIE_NULL) {
      val = (int *)malloc(sizeof(int));
      *val = (int)i;
      assert(trie_insert(T, edge, val) != 0);
      key = (int *)malloc(sizeof(int));
      *key = (int)i;
      assert(hash_table_insert(RT, key, edgecpy) != 0);
      i++;
      /* free(val); */
      /* free(key); */
    }
    /* free(edge); */
    /* free(edgecpy); */
    IGRAPH_VIT_NEXT(vit);
  }
  igraph_vit_destroy(&vit);
  assert(trie_num_entries(T) == i);
  assert(trie_num_entries(T) == hash_table_num_entries(RT));
}

/* The neighbours of the given node in a line graph.
 *
 * INPUT:
 *
 * - G -- a simple undirected graph.
 * - LG -- the line graph of G.
 * - vL -- a node in the line graph L(G).
 * - Nbr -- the neighbours of vL will be stored here.  This should be
 *   initialized beforehand.  The first two elements is an edge in G, the
 *   next two elements is another edge in G, etc.
 */
void linegraph_neighbors(const igraph_t *G,
                         const igraph_t *LG,
                         const igraph_integer_t vL,
                         igraph_vector_t *Nbr) {
  igraph_integer_t i;
  igraph_integer_t u;
  igraph_integer_t v;
  igraph_vector_t N;  /* IDs of edges in G */

  assert(igraph_vector_size(Nbr) == 0);
  igraph_vector_init(&N, 0);
  igraph_neighbors(LG, &N, vL, IGRAPH_ALL);
  for (i = 0; i < igraph_vector_size(&N); i++) {
    igraph_edge(G, VECTOR(N)[i], &u, &v);
    assert(u != v);
    if (u < v) {
      igraph_vector_push_back(Nbr, u);
      igraph_vector_push_back(Nbr, v);
    } else {
      igraph_vector_push_back(Nbr, v);
      igraph_vector_push_back(Nbr, u);
    }
  }
  igraph_vector_destroy(&N);
}

/* The index of the given node.  This is the same as the value associated
 * with the key (u,v) in the given hash table.
 *
 * INPUT:
 *
 * - T -- a mapping of node (u,v) to a unique nonnegative integer.
 * - u, v -- a node in a line graph L(G) such that (u,v) is an edge in G.
 */
igraph_integer_t table_index(Trie *T,
                             const igraph_integer_t u,
                             const igraph_integer_t v) {
  char *edge;
  int *i;

  assert(u != v);
  if (u < v) {
    asprintf(&edge, "%ld %ld", (long int)u, (long int)v);
  } else {
    asprintf(&edge, "%ld %ld", (long int)v, (long int)u);
  }
  i = (int *)trie_lookup(T, edge);
  free(edge);
  return (igraph_integer_t)(*i);
}

/* The line graph node corresponding to the given index.
 *
 * INPUT:
 *
 * - RT -- a mapping of unique nonnegative integer to node (u,v).
 * - k -- an index.
 * - u, v -- a node in a line graph L(G).  The results will be stored here.
 */
void table_node(HashTable *RT,
                igraph_integer_t k,
                igraph_integer_t *u,
                igraph_integer_t *v) {
  char *edge;
  long int a;
  long int b;

  edge = hash_table_lookup(RT, &k);
  sscanf(edge, "%ld %ld", &a, &b);
  *u = (igraph_integer_t)a;
  *v = (igraph_integer_t)b;
}

/* Do a reverse lookup in the given hash table.  That is, given a value,
 * we want to find the key with that value.  We assume that all values in the
 * hash table are unique.
 *
 * INPUT:
 *
 * - T -- a mapping of node (u,v) to a unique nonnegative integer.
 * - n -- a value on which to do reverse lookup.
 * - G -- a simple undirected graph.
 * - H -- another simple undirected graph.
 * - lineG -- the line graph of G.
 * - lineH -- the line graph of H.
 * - u, v -- a node in a line graph L(G) such that (u,v) is an edge in G.  The
 *   key corresponding to the value n will be stored here.
 */
/* void table_reverse_lookup(Trie *T, */
/*                           const igraph_integer_t n, */
/*                           const igraph_t *G, */
/*                           const igraph_t *H, */
/*                           const igraph_t *lineG, */
/*                           const igraph_t *lineH, */
/*                           igraph_integer_t *u, */
/*                           igraph_integer_t *v) { */
/*   char *edge; */
/*   igraph_integer_t a; */
/*   igraph_integer_t b; */
/*   igraph_integer_t *k; */
/*   igraph_integer_t vL; */
/*   igraph_vit_t vit; */

/*   /\* search in G and its line graph L(G) *\/ */
/*   igraph_vit_create(lineG, igraph_vss_all(), &vit); */
/*   while (!IGRAPH_VIT_END(vit)) { */
/*     vL = (igraph_integer_t)IGRAPH_VIT_GET(vit); */
/*     igraph_edge(G, vL, &a, &b); */
/*     if (a < b) { */
/*       asprintf(&edge, "%ld %ld", (long int)a, (long int)b); */
/*     } else { */
/*       asprintf(&edge, "%ld %ld", (long int)b, (long int)a); */
/*     } */
/*     k = (igraph_integer_t *)trie_lookup(T, edge); */
/*     if (*k == n) { */
/*       if (a < b) { */
/*         *u = a; */
/*         *v = b; */
/*       } else { */
/*         *u = b; */
/*         *v = a; */
/*       } */
/*       free(edge); */
/*       igraph_vit_destroy(&vit); */
/*       return; */
/*     } */
/*     free(edge); */
/*     IGRAPH_VIT_NEXT(vit); */
/*   } */
/*   igraph_vit_destroy(&vit); */

/*   /\* search in H and its line graph L(H) *\/ */
/*   igraph_vit_create(lineH, igraph_vss_all(), &vit); */
/*   while (!IGRAPH_VIT_END(vit)) { */
/*     vL = (igraph_integer_t)IGRAPH_VIT_GET(vit); */
/*     igraph_edge(H, vL, &a, &b); */
/*     if (a < b) { */
/*       asprintf(&edge, "%ld %ld", (long int)a, (long int)b); */
/*     } else { */
/*       asprintf(&edge, "%ld %ld", (long int)b, (long int)a); */
/*     } */
/*     k = (igraph_integer_t *)trie_lookup(T, edge); */
/*     if (*k == n) { */
/*       if (a < b) { */
/*         *u = a; */
/*         *v = b; */
/*       } else { */
/*         *u = b; */
/*         *v = a; */
/*       } */
/*       free(edge); */
/*       igraph_vit_destroy(&vit); */
/*       return; */
/*     } */
/*     free(edge); */
/*     IGRAPH_VIT_NEXT(vit); */
/*   } */
/*   igraph_vit_destroy(&vit); */
/* } */

/* Initialize a vector of vectors.  This is just a vector, each of whose
 * elements is a pointer to a vector.
 *
 * INPUT:
 *
 * - size -- the number of elements.
 * - V -- pointer to an initialized vector of vectors.  The result will be
 *   stored here.
 */
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_integer_t size) {
  igraph_integer_t i;
  igraph_vector_t *p;

  for (i = 0; i < size; i++) {
    p = igraph_Calloc(1, igraph_vector_t);
    igraph_vector_init(p, 0);
    igraph_vector_ptr_push_back(V, p);
  }
}

/* Locate the region of greatest changes.  Here, we consider the level of
 * line graphs.
 */
int main(int argc,
         char **argv) {
  char *fname;
  FILE *f;
  igraph_t G;
  igraph_t H;
  igraph_t lineG;  /* line graph version of G */
  igraph_t lineH;  /* line graph version of H */
  igraph_bool_t simple;
  igraph_integer_t max;
  igraph_vector_t MinusCount;
  igraph_vector_t PlusCount;
  Trie *T;  /* mapping of node in line graph to unique nonnegative integer */
  HashTable *RT;  /* reverse map of T */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_file *felistA = arg_file0(NULL, "felistA", NULL,
                               "file containing edge list, one edge per line");
  struct arg_file *felistB = arg_file0(NULL, "felistB", NULL,
                               "file containing edge list, one edge per line");
  struct arg_str *fchangedist = arg_strn(NULL, "fchangedist", "string", 0,
                                         argc + 2,
                       "write change distributions to files with this prefix");
  struct arg_str *fmaxchange = arg_strn(NULL, "fmaxchange", "string", 0,
                                         argc + 2,
                           "write greatest changes to files with this prefix");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, felistA, felistB, fchangedist, fmaxchange, end};
  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Locate the region of greatest changes at level of line graph.\n");
    arg_print_glossary(stdout, argtable, " %-23s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((felistA->count < 1)
      || (felistB->count < 1)
      || (fchangedist->count < 1)
      || (fmaxchange->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Locate the region of greatest changes at level of line graph.\n");
    arg_print_glossary(stdout, argtable, " %-23s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  /* reconstruct graphs from their edge lists */
  asprintf(&fname, "%s", felistA->filename[0]);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&G, f, 0, IGRAPH_UNDIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  asprintf(&fname, "%s", felistB->filename[0]);
  f = fopen(fname, "r");
  igraph_read_graph_edgelist(&H, f, 0, IGRAPH_UNDIRECTED);
  fclose(f);
  free(fname);
  igraph_simplify(&H, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                  /*edge_comb*/ 0);
  /* construct line graph version */
  igraph_linegraph(&G, &lineG);
  igraph_is_simple(&lineG, &simple);
  assert(simple);
  igraph_linegraph(&H, &lineH);
  igraph_is_simple(&lineH, &simple);
  assert(simple);
  /* Map each node in both line graphs to a unique nonnegative integer. */
  /* The number of elements in the map counts the number of unique nodes */
  /* in both line graphs.  We identify each node in a line graph as a pair */
  /* of vertices in the original graph.  This is different from igraph, */
  /* which identifies each node in a line graph as a nonnegative integer. */
  T = trie_new();
  RT = hash_table_new(int_hash, int_equal);
  hash_table_register_free_functions(RT, free, free);
  hash_node(T, RT, &G, &H, &lineG, &lineH);

  igraph_vector_init(&MinusCount, (igraph_integer_t)trie_num_entries(T));
  igraph_vector_init(&PlusCount, (igraph_integer_t)trie_num_entries(T));
  change_plus(&G, &H, &lineG, &lineH, fchangedist->sval[0],
              fmaxchange->sval[0], T, RT, &PlusCount);
  change_minus(&G, &H, &lineG, &lineH, fchangedist->sval[0],
               fmaxchange->sval[0], T, RT, &MinusCount);
  change_total(RT, &MinusCount, &PlusCount, fchangedist->sval[0], &max);
  change_total_max(RT, &MinusCount, &PlusCount, fmaxchange->sval[0], max);

  igraph_destroy(&G);
  igraph_destroy(&H);
  igraph_destroy(&lineG);
  igraph_destroy(&lineH);
  igraph_vector_destroy(&MinusCount);
  igraph_vector_destroy(&PlusCount);
  trie_free(T);
  hash_table_free(RT);
  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
