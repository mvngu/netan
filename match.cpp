/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

// Match step communities in consecutive time steps.

#ifndef _GNU_SOURCE
  #define _GNU_SOURCE  // asprintf
#endif

// C library
#include <argtable2.h>
#include <assert.h>
#include <ctype.h>
#include <igraph.h>
#include <stdio.h>
#include <unistd.h>  // access, F_OK

// CPP library
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <tr1/unordered_map>

int count_communities(const int si,
                      const char *comdir);
int first_sindex(const igraph_vector_t *sindex,
                 const char *comdir);
void get_sindex(const char *infile,
                igraph_vector_t *sindex);
inline void jaccard(const igraph_vector_t *A,
                    const igraph_vector_t *B,
                    double *sim,
                    double *p);
void match(const char *fsindex,
           const char *matchdir,
           const char *comdir,
           const double theta,
           const int delta);
void read_all_communities(const igraph_vector_t *sindex,
                          const int firstsi,
                          const char *comdir,
                          igraph_vector_ptr_t *Com,
                          std::tr1::unordered_map<int, int> *revsi);
void read_communities(const int si,
                      const char *comdir,
                      igraph_vector_ptr_t *Com);
// void read_one_community(const int si,
//                         const int ci,
//                         const char *comdir,
//                         igraph_vector_t *C);
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const int size);

/* The number of communities in the given snapshot index.
 *
 * INPUT:
 *
 * - si -- the snapshot index.
 * - comdir -- name of the directory from which to read file storing
 *   communities.
 */
int count_communities(const int si,
                      const char *comdir) {
  char *fname;
  int ncom;
  std::ifstream f;

  asprintf(&fname, "%s/com-%d.dat", comdir, si);
  ncom = 0;
  // given file doesn't exist
  if (access(fname, F_OK) != 0) {
    ncom = 0;
  } else {
    f.open(fname);
    f >> ncom;
    f.close();
  }
  free(fname);
  return ncom;
}

/* The first snapshot index having communities.  Depending on the minimum
 * required size of each community, some snapshot indices might be neglected
 * because each of the communities in that snapshot has size less than the
 * required minimum.  We only return the index of that snapshot from within the
 * given list of snapshot indices.
 *
 * INPUT:
 *
 * - sindex -- list of all the snapshot indices, sorted in temporal order.
 * - comdir -- name of the directory from which to read file storing
 *   communities.
*/
int first_sindex(const igraph_vector_t *sindex,
                 const char *comdir) {
  int i;
  int ncom;

  i = 0;
  ncom = count_communities((int)VECTOR(*sindex)[i], comdir);
  while (ncom < 1) {
    i++;
    assert(i < (int)igraph_vector_size(sindex));
    ncom = count_communities((int)VECTOR(*sindex)[i], comdir);
  }
  return i;
}

/* Get all the snapshot indices from the given file.  The snapshot indices
 * as contained in the given file is assumed to be sorted in temporal order.
 *
 * - infile -- file containing all the snapshot indices.  These are assumed to
 *   be sorted in temporal order.
 * - sindex -- vector of snapshot indices.  The result will be stored here.
 *   This should be initialized beforehand.
 */
void get_sindex(const char *infile,
                igraph_vector_t *sindex) {
  std::ifstream f;
  int si;  // a snapshot index

  f.open(infile);
  if (!f) {
    printf("Unable to open file %s\n", infile);
    assert(0);
  }
  while (f >> si) {
    igraph_vector_push_back(sindex, si);
  }
  f.close();
}

/* The Jaccard coefficient between the given two communities.  The Jaccard
 * coefficient measures the overlap of two communities.
 *
 * INPUT:
 *
 * - A, B -- two communities.
 *
 * OUTPUT:
 *
 * The degree of overlap between two the given two communities.
 */
inline void jaccard(const igraph_vector_t *A,
                    const igraph_vector_t *B,
                    double *sim,
                    double *p) {
  long a;  // size of A
  long b;  // size of B
  long c;  // size of A intersect B
  igraph_vector_t intersect;

  igraph_vector_init(&intersect, 0);
  igraph_vector_intersect_sorted(A, B, &intersect);
  a = (long)igraph_vector_size(A);
  b = (long)igraph_vector_size(B);
  c = (long)igraph_vector_size(&intersect);
  *sim = (double)c / (double)(a + b - c);
  *p = (double)(b - a) / (double)a;

  igraph_vector_destroy(&intersect);
}

/* Match communities in consecutive time steps.
 *
 * INPUT:
 *
 * - fsindex -- file containing all snapshot indices, one per line.
 * - matchdir -- directory under which to write match data.
 * - comdir -- directory under which community structures are found.
 * - theta -- the matching threshold.
 * - delta -- maximum allowable number of missing observations.
 *
 * OUTPUT:
 *
 * All community matches, written to a file and recorded in the following
 * format:
 *
 *   y-1 i y a s p
 *
 * Here, "y-1 i" refers to the community indexed by i in the snapshot indexed
 * by y-1, "y a" refers to the community in the current snapshot indexed by a,
 * s is the similarity score, and p is the percentage change in community size.
 */
void match(const char *fsindex,
           const char *matchdir,
           const char *comdir,
           const double theta,
           const int delta) {
  FILE *f;
  char *fname;
  std::string key;
  int sidx;                    // a snapshot index
  int cidx;                    // a community index
  std::stringstream ss;
  double p;                    // percentage change in size
  double sim;                  // similarity or overlap score
  igraph_vector_t *Ca;         // a step community
  igraph_vector_t *Fi;         // a front community
  igraph_vector_t *Mi;         // a community with missing observations
  igraph_vector_t sindex;      // snapshot indices
  igraph_vector_ptr_t *C;      // all communities in a snapshot
  igraph_vector_ptr_t Com;     // all communities in all snapshots
  igraph_vector_ptr_t *Front;  // all front communities
  igraph_vector_ptr_t *Step;   // all step communities
  int a;
  int i;
  int j;
  int k;
  int m;  // is there a match?
  // Let the time step be t.  The communities at time t are referred to as the
  // step communities.  Those at time t - 1 are the front communities.  Any
  // communities at time < t - 1 without observations are called missing
  // communities.  Missing communities are lumped together with the fronts at
  // time t.  We allow communities to have missing observations for a number
  // of consecutive time steps.  If the number of missing observations exceed
  // a fixed missing threshold, then we consider the corresponding missing
  // community as dead.
  // key := "year community_index"
  // value := no. missing observations
  std::tr1::unordered_map<std::string, int> Missing;
  std::tr1::unordered_map<std::string, int>::iterator itr;
  // A reverse mapping from snapshot index to index of Com.
  std::tr1::unordered_map<int, int> revsi;

  igraph_vector_init(&sindex, 0);
  get_sindex(fsindex, &sindex);
  i = first_sindex(&sindex, comdir);
  j = i + 1;
  igraph_vector_ptr_init(&Com, 0);
  IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(&Com, igraph_vector_ptr_destroy_all);
  read_all_communities(&sindex, i, comdir, &Com, &revsi);

  for (k = 1; k < (int)igraph_vector_ptr_size(&Com); k++) {
    Front = (igraph_vector_ptr_t *)VECTOR(Com)[k - 1];
    // No front communities to work with.  Create empty file and move on to
    // the next snapshot index.
    if ((int)igraph_vector_ptr_size(Front) == 0) {
      asprintf(&fname, "%s/match-%d.dat", matchdir, (int)VECTOR(sindex)[j]);
      f = fopen(fname, "w");
      fclose(f);
      free(fname);
      j++;
      continue;
    }
    // we have some front communities to work with
    Step = (igraph_vector_ptr_t *)VECTOR(Com)[k];
    asprintf(&fname, "%s/match-%d.dat", matchdir, (int)VECTOR(sindex)[j]);
    f = fopen(fname, "w");
    // try to match each front community with each step community
    for (i = 0; i < (int)igraph_vector_ptr_size(Front); i++) {
      Fi = (igraph_vector_t *)VECTOR(*Front)[i];
      m = 0;  // assume front F_i has zero matches so far
      for (a = 0; a < (int)igraph_vector_ptr_size(Step); a++) {
        Ca = (igraph_vector_t *)VECTOR(*Step)[a];
        jaccard(Fi, Ca, &sim, &p);
        if (sim > theta) {
          m++;
          fprintf(f, "%d %d %d %d %.10lf %.10lf\n",
                  (int)VECTOR(sindex)[j - 1], i,
                  (int)VECTOR(sindex)[j], a,
                  sim, p);
        }
      }
      // keep track of fronts with missing observations
      if (m < 1) {
        // Start with zero missing count because we will re-match this
        // missing community with each of the step communities.
        ss << (int)VECTOR(sindex)[j - 1] << " " << i;
        key = ss.str();
        Missing[key] = 0;
        ss.str("");
      }
    }
    // try to match each missing community with each step community
    for (itr = Missing.begin(); itr != Missing.end(); ) {
      std::istringstream is(itr->first);
      is >> sidx;
      is >> cidx;
      C = (igraph_vector_ptr_t *)VECTOR(Com)[revsi[sidx]];
      Mi = (igraph_vector_t *)VECTOR(*C)[cidx];
      m = 0;
      for (a = 0; a < (int)igraph_vector_ptr_size(Step); a++) {
        Ca = (igraph_vector_t *)VECTOR(*Step)[a];
        jaccard(Mi, Ca, &sim, &p);
        if (sim > theta) {
          m++;
          fprintf(f, "%d %d %d %d %.10lf %.10lf\n",
                  sidx, cidx,
                  (int)VECTOR(sindex)[j], a,
                  sim, p);
        }
      }
      // keep track of missing communities not yet considered dead
      // Below we use the delete-while-iterating idiom from
      // http://stackoverflow.com/questions/485358/why-did-i-get-a-segmentation-fault-with-a-map-insert
      if (m < 1) {
        Missing[itr->first]++;
        // community still considered missing
        if (Missing[itr->first] <= delta) {
          sim = 1.0;  // perfect match
          p = 0.0;    // zero change in size
          fprintf(f, "%d %d %d %d %.10lf %.10lf\n",
                  sidx, cidx, sidx, cidx, sim, p);
          ++itr;
        }
        // remove a dead community
        else {
          Missing.erase(itr++);
        }
      }
      // missing community M_i matches some step community C_a
      // so M_i no longer has missing observations
      else {
        Missing.erase(itr++);
      }
    }
    j++;
    fclose(f);
    free(fname);
  }

  // clear-and-minimize idiom
  Missing.clear();
  std::tr1::unordered_map<std::string, int>().swap(Missing);
  igraph_vector_destroy(&sindex);
  igraph_vector_ptr_destroy_all(&Com);
}

/* Get all communities in all snapshots.
 *
 * INPUT:
 *
 * - sindex -- list of all snapshot indices.
 * - firstsi -- index of the first snapshot with communities.
 * - comdir -- read communities from this directory.
 * - Com -- save all the communities here.
 * - revsi -- a reverse mapping from snapshot index to index of Com.
 */
void read_all_communities(const igraph_vector_t *sindex,
                          const int firstsi,
                          const char *comdir,
                          igraph_vector_ptr_t *Com,
                          std::tr1::unordered_map<int, int> *revsi) {
  int i;
  int k;
  int key;
  igraph_vector_ptr_t *C;

  i = firstsi;
  k = 0;
  while (i < (int)igraph_vector_size(sindex)) {
    C = igraph_Calloc(1, igraph_vector_ptr_t);
    igraph_vector_ptr_init(C, 0);
    IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(C, igraph_vector_destroy);
    read_communities((int)VECTOR(*sindex)[i], comdir, C);
    igraph_vector_ptr_push_back(Com, C);
    key = (int)VECTOR(*sindex)[i];
    (*revsi)[key] = k;
    i++;
    k++;
  }
}

/* Read all communities from a snapshot with the given index.  The communities
 * of a snapshot are stored in a file.  The very first line of the file lists
 * the number of communities.  Each subsequent line lists the node IDs making
 * up a community.
 *
 * INPUT:
 *
 * - si -- the snapshot index.
 * - comdir -- the directory under which communities are stored.
 * - Com -- the communities will be stored here.  This should be initialized
 *   beforehand.
 */
void read_communities(const int si,
                      const char *comdir,
                      igraph_vector_ptr_t *Com) {
  FILE *f;
  char *fname;
  int c;
  int ncom;        // # of communities
  long int i;      // generic index
  long int node;   // a node belonging to a community
  igraph_vector_t *p;

  asprintf(&fname, "%s/com-%d.dat", comdir, si);
  f = fopen(fname, "r");
  // skip all white spaces
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);
  // The very first line of the given file contains only the number of
  // communities.  Read in this number and initialize the community vector
  // Com to have that many vectors.
  fscanf(f, "%d", &ncom);
  vector_of_vectors_init(Com, ncom);

  // skip all white spaces
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);

  // Index i is now the community index.  We start from community with
  // index 0.  Each newline character read indicates that we are to
  // increment the community index by one.
  i = 0;
  while (!feof(f)) {
    // get all the nodes belonging to a community
    fscanf(f, "%li", &node);
    p = (igraph_vector_t *)VECTOR(*Com)[i];
    igraph_vector_push_back(p, (igraph_integer_t)node);
    // skip all white spaces
    do {
      c = getc(f);
      // All the nodes belonging to a community are assumed to be on one
      // line.  If we encounter a newline character, then we know that
      // we have read all the nodes of a community.
      if (c == '\n') {
        i++;
      }
    } while (isspace(c));
    ungetc(c, f);
  }

  fclose(f);
  free(fname);
}

/* Read one community from a snapshot with the given index.  The communities
 * of a snapshot are stored in a file.  The very first line of the file lists
 * the number of communities.  Each subsequent line lists the node IDs making
 * up a community.  We only get one community with the specified community
 * index.
 *
 * INPUT:
 *
 * - si -- the snapshot index.
 * - ci -- the index of the community in the given snapshot.
 * - comdir -- the directory under which communities are stored.
 * - C -- the community will be stored here.  This should be initialized
 *   beforehand.
 */
// void read_one_community(const int si,
//                         const int ci,
//                         const char *comdir,
//                         igraph_vector_t *C) {
//   FILE *f;
//   char *fname;
//   int c;
//   int ncom;        // # of communities
//   long int i;      // generic index
//   long int node;   // a node belonging to a community
//
//   assert(ci >= 0);
//   asprintf(&fname, "%s/com-%d.dat", comdir, si);
//   f = fopen(fname, "r");
//   // skip all white spaces
//   do {
//     c = getc(f);
//   } while (isspace(c));
//   ungetc(c, f);
//   // The very first line of the given file contains only the number of
//   // communities.  Read in this number.
//   fscanf(f, "%d", &ncom);
//   assert(ci < ncom);
//
//   // skip all white spaces
//   do {
//     c = getc(f);
//   } while (isspace(c));
//   ungetc(c, f);
//
//   // Index i is now the community index.  We start from community with
//   // index 0.  Each newline character read indicates that we are to
//   // increment the community index by one.
//   // Ignore all communities with index != i.
//   i = 0;
//   while (!feof(f)) {
//     fscanf(f, "%li", &node);
//     do {
//       c = getc(f);
//       if (c == '\n') {
//         i++;
//       }
//     } while (isspace(c));
//     ungetc(c, f);
//     if (i == ci) {
//       break;
//     }
//   }
//   // get all the nodes belonging to the community with index ci
//   while (!feof(f)) {
//     fscanf(f, "%li", &node);
//     igraph_vector_push_back(C, (igraph_integer_t)node);
//     // skip all white spaces
//     do {
//       c = getc(f);
//       // All the nodes belonging to a community are assumed to be on one
//       // line.  If we encounter a newline character, then we know that
//       // we have read all the nodes of a community.
//       if (c == '\n') {
//         i++;
//       }
//     } while (isspace(c));
//     ungetc(c, f);
//     if (i > ci) {
//       break;
//     }
//   }
//
//   fclose(f);
//   free(fname);
// }

/* Initialize a vector of vectors.  This is just a vector, each of whose
 * elements is a pointer to a vector.
 *
 * INPUT:
 *
 * - V -- pointer to an initialized vector of vectors.  The result will be
 *   stored here.
 * - size -- the number of elements in V.
 */
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const int size) {
  int i;
  igraph_vector_t *p;

  for (i = 0; i < size; i++) {
    p = igraph_Calloc(1, igraph_vector_t);
    igraph_vector_init(p, 0);
    igraph_vector_ptr_push_back(V, p);
  }
}

/* Match step communities in consecutive time steps.
 */
int main(int argc,
         char **argv) {
  // setup the table of command line options
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_file *fsindex = arg_file0(NULL, "fsindex", NULL,
                         "file containing all snapshot indices, one per line");
  struct arg_str *matchdir = arg_strn(NULL, "matchdir", "path", 0, argc + 2,
                                  "directory under which to write match data");
  struct arg_str *comdir = arg_strn(NULL, "comdir", "path", 0, argc + 2,
                       "directory under which community structures are found");
  struct arg_str *theta = arg_strn(NULL, "theta", "float", 0, argc + 2,
                                   "matching threshold");
  struct arg_str *delta = arg_strn(NULL, "delta", "integer", 0, argc + 2,
                         "maximum allowable consecutive missing observations");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, fsindex, matchdir, comdir, theta, delta, end};
  // parse the command line as defined by argtable[]
  arg_parse(argc, argv, argtable);

  // print usage information when --help is passed in
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Match step communities in consecutive time steps.\n");
    arg_print_glossary(stdout, argtable, " %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  // special case: all command line arguments, except --help, must be used
  if ((fsindex->count < 1)
      || (matchdir->count < 1)
      || (comdir->count < 1)
      || (theta->count < 1)
      || (delta->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Match step communities in consecutive time steps.\n");
    arg_print_glossary(stdout, argtable, " %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  // special case: matching threshold must be nonnegative
  if (strtod(theta->sval[0], NULL) < 0.0) {
    printf("Error: invalid matching threshold %lf\n",
           strtod(theta->sval[0], NULL));
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Match step communities in consecutive time steps.\n");
    arg_print_glossary(stdout, argtable, " %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  // special case: delta must be nonnegative
  if (strtol(delta->sval[0], NULL, 0) < 0) {
    printf("Error: invalid delta %ld\n", strtol(delta->sval[0], NULL, 0));
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Match step communities in consecutive time steps.\n");
    arg_print_glossary(stdout, argtable, " %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  // WARNING: The paths matchdir and comdir are assumed to exist.

  match(fsindex->filename[0], matchdir->sval[0], comdir->sval[0],
        strtod(theta->sval[0], NULL), atoi(delta->sval[0]));

  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));

  return 0;
}
