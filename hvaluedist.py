###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Cumulative distribution of H-values.

import argparse
import os

###################################
# helper functions
###################################

def cumulative_frequency(entropydir, maxlifespan):
    """
    The cumulative frequency of H values.  For each H-value H, we count how
    many dynamic communities having a H-value at least H.  Each H-value
    corresponds to some dynamic community.  Two dynamic communities can have
    the same H-value, but each dynamic community has only one H-value.

    INPUT:

    - entropydir -- path to directory with files containing H values.
    - maxlifespan -- maximum lifespan of a dynamic community.
    """
    Hdb = []  # database of H-values
    for i in range(2, maxlifespan + 1):
        hvalues(entropydir, i, Hdb)
    ndycom = len(Hdb)  # number of dynamic communities altogether
    delta = 0.00001    # increment
    H = 0.0            # focal H-value
    maxH = max(Hdb)    # maximum H-value
    F = []             # frequency table
    while H <= maxH:
        count = 0
        for hval in Hdb:
            if hval >= H:
                count += 1
        normcount = float(count) / float(ndycom)
        F.append((H, normcount))
        H += delta
    return F

def hvalues(entropydir, lifespan, Hdb):
    """
    All the H-values of dynamic communities with the given lifespan.

    WARNING: This function modifies its arguments.

    INPUT:

    - entropydir -- path to directory with files containing H values.
    - lifespan -- the lifespan of all dynamic communities we want to consider.
    - Hdb -- the database of H-values.  The results will be added to this
      database.
    """
    fname = os.path.join(entropydir, "entropy-%d.dat" % lifespan)
    f = open(fname, "r")
    for line in f:
        H = line.strip().split(";")[0]  # ignore series of Jaccard coefficients
        Hdb.append(float(H))
    f.close()

def write_distribution(Dist, outfile):
    """
    Write to the given file the cumulative distributions of H-values.

    INPUT:

    - Dist -- the cumulative distribution of H-values.
    - outfile -- the file to which we write results.
    """
    dirname = os.path.dirname(outfile)
    if not os.path.isdir(dirname):
        os.makedirs(dirname)
    f = open(outfile, "w")
    f.write("minimum H value,normalized count\n")  # file header
    for H, count in Dist:
        f.write("%.10f,%.10f\n" % (H, count))
    f.close()

###################################
# script starts here
###################################

# setup parser for command line options
s = "Cumulative distribution of H-values.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--entropydir", metavar="path", required=True,
                    help="path to directory with files containing H values")
parser.add_argument("--maxlifespan", metavar="int", required=True,
                    help="maximum lifespan of a dynamic community")
parser.add_argument("--outfile", metavar="path", required=True,
                    help="file to which we write results")
args = parser.parse_args()

# get the command line arguments
entropydir = args.entropydir
maxlifespan = int(args.maxlifespan)
outfile = args.outfile

Dist = cumulative_frequency(entropydir, maxlifespan)
write_distribution(Dist, outfile)
