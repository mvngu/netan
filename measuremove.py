###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script MUST be run from Sage.
#
# Measure the amount of movement of nodes across dynamic communities.

from sage.all_cmdline import *
# Use optparse because Sage 4.8 has Python 2.6.  From Python 2.7 onwards,
# we could use argparse.
import optparse
import os

###################################
# helper functions
###################################

def add_missing_matches(E, M, V, R):
    """
    Update the edge set with fronts missing observations.  As we go along,
    we also update the vertex set.

    INPUT:

    - E -- edge set given as a dictionary of dictionaries.
    - M -- fronts with missing observations, given as a dictionary of lists.
    - V -- a vertex set to update.
    - R -- reverse mapping from vertex ID to snapshot/community index.
     """
    for u in M:
        # add one front with missing observation
        i = 1
        v = u + "," + str(i)
        sim = M[u][0][u][0]
        pchange = M[u][0][u][1]
        update_edge_set(E, V, R, (u, v, sim, pchange), M)
        # add the remaining fronts
        for _ in M[u][1:]:
            v = u + "," + str(i)
            w = u + "," + str(i + 1)
            update_edge_set(E, V, R, (v, w, sim, pchange), M)
            i += 1
        # has indirect continuation after missing observations?
        # take care of resumption of observations
        for k in E[u].keys():
            # community at si has observation at si + i
            if not k.startswith(u):
                v = u + "," + str(i)
                w = k
                sim = E[u][k][0]
                pchange = E[u][k][1]
                del E[u][k]
                update_edge_set(E, V, R, (v, w, sim, pchange), M)

def birth(si, dirname):
    """
    Get all the birth of dynamic communities in the given snapshot index.
    The birth or beginning of a dynamic community is a community in snapshot
    index si without a matching to some community in previous snapshots.

    INPUT:

    - si -- a snapshot index for which data are available.  If there are
      community births in the given snapshot, we return a list of all such
      communities.  Otherwise, we return the empty list.
    - dirname -- path to directory with match data.
    """
    f = open(os.path.join(dirname, "birth", "birth-%d.dat" % si), "r")
    n = int(f.readline().strip())  # first line is number of births
    # zero births
    if n < 1:
        f.close()
        return []
    # at least one birth
    B = []
    for line in f:
        yi = line.strip()  # snapshot/community index
        B.append(yi)
    f.close()
    return B

def clean_key(cname):
    """
    Clean the given community label.

    INPUT:

    - cname -- a label for a community.  This community is assumed to be a
      node in the digraph representation of all traces of dynamic communities.
    """
    return cname.split(",")[0]

def count_communities(si, comdir):
    """
    The number of communities in the given network snapshot.

    INPUT:

    - si -- a snapshot index.
    - comdir -- directory from which to read communities with a given minimum
      number of nodes (smin).
    """
    fname = os.path.join(comdir, "comm-%d.dat" % si)
    if not os.path.isfile(fname):
        return 0
    f = open(fname, "r")
    ncom = int(f.readline().strip())  # first line is # communities in snapshot
    f.close()
    return ncom

def count_source_sink(G):
    """
    Count the number of nodes that act as source of community split or
    sink of community merge.  Let v be a vertex in a digraph G, where G is
    assumed to be a network derived from community matching.  If the
    out-degree of v is > 1, then v is a source for community split.  If the
    in-degree of v is > 1, then v is a sink for community merge.

    INPUT:

    - G -- a directed graph.
    """
    assert isinstance(G, DiGraph)
    nss = 0  # number of nodes that are source or sink
    for v in G.vertex_iterator():
        if (G.out_degree(v) > 1) or (G.in_degree(v) > 1):
            nss += 1
    return nss

def density_exchange_communities(G, smin, theta, delta, fname):
    """
    The density of communities that act as source of split or destination
    for merge.

    INPUT:

    - G -- a digraph representing the evolution of all dynamic communities.
      This contains all the traces of all dynamic communities.
    - smin -- minimum community size.
    - theta -- matching threshold.
    - delta -- maximum allowable consecutive missing observations.  How many
      consecutive time steps with missing observations are we prepared to
      tolerate before we decide that a dynamic community is dead.
    - fname -- append results to this file.
    """
    # get the properties we are interested in
    nss_observed = count_source_sink(G)
    nnode = G.order()
    # write results to file
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write("smin,theta,delta,NSS observed,#communities,density\n")
        f.close()
    # Write the density to the given file.  We do not overwrite the file, if
    # it exists.  Rather, we append our results to the file.
    R = RR(nss_observed)  # nss in real or observed network
    N = RR(nnode)         # possible number of nss, i.e. # nodes in digraph
    density = R / N
    f = open(fname, "a")
    f.write("%s,%s,%s,%d,%d,%.10f\n" %
            (str(smin), str(theta), str(delta), nss_observed, nnode, density))
    f.close()

def density_exchange_dycom(G, smin, theta, delta, fsindex, tracedir, fname):
    """
    The density of dynamic communities that have participated in a community
    split or merge.  In other words, we compute the density of dynamic
    communities that have exchanged nodes.

    INPUT:

    - G -- a digraph representing the evolution of all dynamic communities.
      This contains all the traces of all dynamic communities.
    - smin -- minimum community size.
    - theta -- matching threshold.
    - delta -- maximum allowable consecutive missing observations.  How many
      consecutive time steps with missing observations are we prepared to
      tolerate before we decide that a dynamic community is dead.
    - fsindex -- file with all the snapshot indices, one per line.  The
      indices are assumed to be sorted in temporal order.
    - tracedir -- where we store the traces of all dynamic communities.
    - fname -- append results to this file.
    """
    assert isinstance(G, DiGraph)
    Sindex = read_sindex(fsindex)
    Dycom = read_dynamic_community(Sindex, tracedir)
    XD = set()  # all dycoms that have participated in a split or merge
    for v in G.vertex_iterator():
        # dycoms that have participated in a split or merge
        if (G.out_degree(v) > 1) or (G.in_degree(v) > 1):
            for dc in Dycom:
                trace = Dycom[dc]
                if v in trace:
                    XD = XD.union(set([dc]))
    # write results to file
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write("smin,theta,delta,#dycom split merge,#dycom,density\n")
        f.close()
    # Write the density to the given file.  We do not overwrite the file, if
    # it exists.  Rather, we append our results to the file.
    NSM = len(XD)   # no. dycom that participated in split or merge
    N = len(Dycom)  # no. dycom in all
    density = RR(NSM) / RR(N)
    f = open(fname, "a")
    f.write("%s,%s,%s,%d,%d,%.10f\n" %
            (str(smin), str(theta), str(delta), NSM, N, density))
    f.close()

def density_exchange_nodes(G, smin, theta, delta, fsindex, comdir, fname):
    """
    The density of nodes that have participated in a community split or merge.
    In other words, we compute the density of nodes that have straddled
    between different dynamic communities.

    INPUT:

    - G -- a digraph representing the evolution of all dynamic communities.
      This contains all the traces of all dynamic communities.
    - smin -- minimum community size.
    - theta -- matching threshold.
    - delta -- maximum allowable consecutive missing observations.  How many
      consecutive time steps with missing observations are we prepared to
      tolerate before we decide that a dynamic community is dead.
    - fsindex -- file with all the snapshot indices, one per line.  The
      indices are assumed to be sorted in temporal order.
    - comdir -- directory where communities are stored.  This should be where
      we store communities that were used in the matching process.  The given
      directory is not necessarily the same as the directory where we store
      all communities extracted from all network snapshots.
    - fname -- append results to this file.
    """
    assert isinstance(G, DiGraph)
    Sindex = read_sindex(fsindex)
    Com = read_community(Sindex, comdir)
    XN = set()  # all unique nodes that have participated in a split or merge
    XV = set()  # all unique nodes
    for v in G.vertex_iterator():
        CV = Com[clean_key(v)]  # all nodes in community v
        XV = XV.union(CV)
        N = set()               # nodes that participate in a split or merge
        # nodes that have participated in a split
        if G.out_degree(v) > 1:
            for u in G.neighbors_out(v):
                CU = Com[clean_key(u)]  # all nodes in community u
                N = N.union(CV.intersection(CU))
        # nodes that have participated in a merge
        if G.in_degree(v) > 1:
            for u in G.neighbors_in(v):
                CU = Com[clean_key(u)]  # all nodes in community u
                N = N.union(CV.intersection(CU))
        XN = XN.union(N)
    # write results to file
    if not os.path.isfile(fname):
        f = open(fname, "w")
        f.write("smin,theta,delta,#nodes split merge,#nodes,density\n")
        f.close()
    # Write the density to the given file.  We do not overwrite the file, if
    # it exists.  Rather, we append our results to the file.
    NSM = len(XN)  # no. nodes that participated in split or merge
    N = len(XV)    # no. nodes in all
    density = RR(NSM) / RR(N)
    f = open(fname, "a")
    f.write("%s,%s,%s,%d,%d,%.10f\n" %
            (str(smin), str(theta), str(delta), NSM, N, density))
    f.close()

def extract_vertices_pchange(match):
    """
    Extract a pair of nodes from the given match data and the corresponding
    percentage change in community sizes.  The match data follows the format:

        si1 community_index1 si2 community_index2 sim pchange

    The snapshot index si1 is assumed to be the same as or earlier than si2.
    The match data format above means that the community with index
    community_index1 at snapshot si1 matches the community with index
    community_index2 at snapshot si2.  The key "sim" is the similarity
    score between the two communities.  The percentage change in community
    size from si1 to si2 is given by pchange.  The first node is taken to
    be the string "si1 community_index1" and the second node is the string
    "si2 community_index2".

    INPUT:

    - match -- a line of match data.
    """
    D = match.split()
    u = " ".join([D[0], D[1]])
    v = " ".join([D[2], D[3]])
    sim = D[4]
    p = D[5]
    return (u, v, sim, p)

def first_snapshot(Sindex, dirname):
    """
    The first snapshot with matching data.  We only return the index of that
    snapshot from within the given list of snapshot indices.

    INPUT:

    - Sindex -- list of snapshot indices in nondecreasing order.
    - dirname -- path to directory with match data.
    """
    i = 0
    while not os.path.isfile(os.path.join(dirname,
                                          "match-%d.dat" % Sindex[i])):
        i += 1
        if i >= len(Sindex):
            raise ValueError("Expected matching data, but received none")
    return i

def get_sindices(fname):
    """
    Get all the snapshot indices from the given file.  The resulting list of
    indices will be sorted in nondecreasing order.

    INPUT:

    - fname -- file with all the snapshot indices, one per line.
    """
    Sindex = []
    f = open(fname, "r")
    for line in f:
        Sindex.append(int(line.strip()))
    f.close()
    return sorted(Sindex)

def ground_truth_properties(Sindex, comdir, eventdir):
    """
    Properties of the ground truth networks.  The networks we use as ground
    truths are:

    * network of insular dynamic communities -- each dynamic community lives
      in exactly one snapshot.  This is a case with no movement of nodes at
      all across dynamic communities.
    * network of purely subsistent dynamic communities -- no movement of nodes
      at all across dynamic communities.  Each dynamic community has enough
      nodes for it to persist from one snapshot to the next.  Think of
      subsistence: just enough to survive.
    * network of frenetic dynamic communities -- hyper movement of nodes
      across dynamic communities.

    Each of the above networks represents an extreme case of movement of
    nodes across dynamic communities.  Either we have no movement at all or
    all possible directions of movement occur.  The properties we are
    interested in are:

    * the number of snapshots
    * the number of communities in each snapshot

    From these two properties, we compute the total number of edges in each
    ground truth network.

    INPUT:

    - Sindex -- list of all possible snapshot indices.
    - comdir -- directory from which to read communities with a given minimum
      number of nodes (smin).
    - eventdir -- directory with data on community events.
    """
    # Determine the first snapshot index i having match data.  The very first
    # snapshot is therefore k = i - 1.  If N is the index of the last
    # snapshot, then the number of snapshots from k to N is
    # N - k + 1 = N - i + 2.
    i = first_snapshot(Sindex, eventdir)
    # map the snapshot index to number of communities in snapshot
    SC = dict()
    for k in range(i - 1, len(Sindex)):
        si = Sindex[k]
        ncom = count_communities(si, comdir)
        if ncom == 0:
            previous_si = Sindex[k - 1]
            previous_ncom = SC[previous_si]
            SC.setdefault(si, previous_ncom)
        else:
            assert ncom > 0
            SC.setdefault(si, ncom)
    # number of edges in the purely subsistent and frenetic networks
    nedge_subsistent = 0
    nedge_frenetic = 0
    # In a frenetic network, this is the number of nodes that act as
    # source for community split or sink for community merge.
    nvert_frenetic = 0
    for k in range(i - 1, len(Sindex) - 1):
        siA = Sindex[k]
        siB = Sindex[k + 1]
        ncomA = SC[siA]
        ncomB = SC[siB]
        nedge_subsistent += min([ncomA, ncomB])
        nedge_frenetic += ncomA * ncomB
        nvert_frenetic += ncomA
    nvert_frenetic += SC[Sindex[-1]]
    return (nedge_subsistent, nedge_frenetic, nvert_frenetic)

def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline of a dynamic
    community.  Each step community is represented as

        si com

    where "si" refers to the snapshot index and "com" refers to the index
    of the community in the given snapshot.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    return trace.split("->")

def read_community(Sindex, comdir):
    """
    Read in all the communities in each network snapshot.  For a network
    snapshot y, each community in the snapshot is assigned a unique
    nonnegative integer, starting from zero.  Let i be the unique label
    assigned to some community.  We use the key "y i" to refer to community i
    in snapshot y.

    INPUT:

    - Sindex -- a list of all the snapshot indices.
    - comdir -- directory where communities are stored.
    """
    # Dictionary of all communities.  Each key is a string of the form "y i".
    # The value corresponding to the key is the set of all nodes belonging to
    # community "y i".
    D = dict()
    for si in Sindex:
        i = 0
        fname = os.path.join(comdir, "comm-%d.dat" % si)
        if not os.path.isfile(fname):
            continue
        f = open(fname, "r")
        ncom = int(f.readline().strip())  # first line is #communities
        if ncom < 1:
            f.close()
            continue
        # get the set of nodes belonging to community "y i"
        for line in f:
            S = line.strip().split()
            Node = set(map(int, S))
            assert len(S) == len(Node)
            key = "%d %d" % (si, i)
            D.setdefault(key, Node)
            i += 1
        f.close()
    return D

def read_dynamic_community(Sindex, tracedir):
    """
    Read in all the dynamic communities.

    INPUT:

    - Sindex -- a list of all the snapshot indices.
    - tracedir -- where we store the traces of all dynamic communities.
    """
    LD = dict()  # all dynamic communities
    # iterate over each lifespan ell
    for ell in range(1, len(Sindex) + 1):
        fname = os.path.join(tracedir, "trace-%d.dat" % ell)
        f = open(fname, "r")
        ndycom = int(f.readline().strip())  # first line is #communities
        if ndycom < 1:
            f.close()
            continue
        # Get all traces of all dynamic communities with lifespan ell.
        # Each line is the trace of a dynamic community.
        for line in f:
            dycom = dict()
            trace = parse_trace(line.strip())
            [dycom.setdefault(tr, True) for tr in trace]
            LD.setdefault(line.strip(), dycom)
        f.close()
    return LD

def read_sindex(fname):
    """
    Get all the snapshot indices from the given file.

    INPUT:

    - fname -- file with all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    """
    Sindex = []
    f = open(fname, "r")
    for line in f:
        Sindex.append(int(line.strip()))
    f.close()
    return Sindex

def update_edge_set(E, V, R, match, M):
    """
    Update the given edge set with the provided match data.

    WARNING:  This function will modify its arguments.

    INPUT:

    - E -- an edge set to update.  The edge set is assumed to be implemented
      as a dictionary of dictionaries.
    - V -- a vertex set to update.
    - R -- reverse mapping from vertex ID to snapshot/community index.
    - match -- a line of match data read from a match file; or a tuple of
      match data.  In the case of tuple, we assume that all elements are
      distinct.
    - M -- dictionary of matches denoting fronts with missing observations.
    """
    u = None
    v = None
    sim = None
    p = None
    if isinstance(match, str):
        u, v, sim, p = extract_vertices_pchange(match)
        # match denoting front with missing observation
        if u == v:
            if u in M:
                M[u].append({v: [sim, p]})
            else:
                M.setdefault(u, [{v: [sim, p]}])
            return
    elif isinstance(match, tuple):
        u, v, sim, p = match
    else:
        raise ValueError("Invalid match data")
    # front with observation
    if u in E:
        E[u].setdefault(v, [sim, p])
    else:
        E.setdefault(u, {v: [sim, p]})
        update_vertex_set(V, R, u)
    update_vertex_set(V, R, v)

def update_vertex_set(V, R, v):
    """
    Update the given vertex set with the provided vertex.

    WARNING:  This function will modify its arguments.

    INPUT:

    - V -- a vertex set to update.
    - R -- reverse mapping from vertex ID to snapshot/community index.
    - v -- a node.
    """
    if v not in V:
        n = len(V)
        V.setdefault(v, n)
        R.setdefault(n, v)

###################################
# script starts here
###################################

# setup parser for command line options
s = "Measure the exchange of nodes across dynamic communities.\n"
s += "Usage: %prog arg1 arg2 ..."
parser = optparse.OptionParser(usage=s)
parser.add_option("--measure", metavar="string",
                  help="what to measure: dycom, node, nss")
parser.add_option("--smin", metavar="integer", type=int,
                  help="minimum community size")
parser.add_option("--theta", metavar="float", type=float,
                  help="matching threshold")
parser.add_option("--delta", metavar="integer", type=int,
                  help="maximum allowable consecutive missing observations")
parser.add_option("--sindex", metavar="file",
                  help="file containing all snapshot indices, one per line")
parser.add_option("--eventdir", metavar="path",
                  help="directory to read event data")
parser.add_option("--outfile", metavar="file",
                  help="append results here")
options, _ = parser.parse_args()

# get command line arguments & sanity checks
if ((options.measure is None)
    or (options.smin is None)
    or (options.theta is None)
    or (options.delta is None)
    or (options.sindex is None)
    or (options.eventdir is None)
    or (options.outfile is None)):
    raise optparse.OptionValueError(
        "All options must be used. Use -h for help.")
mtype = options.measure
smin = options.smin
theta = options.theta
delta = options.delta
sindexfname = options.sindex
eventdir = options.eventdir
fname = options.outfile
if mtype not in ("dycom", "node", "nss"):
    raise ValueError("Can't work out what measurement you want")
if smin < 1:
    raise ValueError("Invalid minimum size")
if theta < 0.0:
    raise ValueError("Invalid theta")
if delta < 0:
    raise ValueError("Invalid delta")

comdir = os.path.join(eventdir, "smin-%d" % smin)
dirname = os.path.join(eventdir, "smin-%d" % smin,
                       "theta-%s_delta-%d" % (str(theta), delta))
if not os.path.isdir(dirname):
    raise ValueError("No such directory: %s" % dirname)
Sindex = get_sindices(sindexfname)

# determine the first snapshot index having match data
i = first_snapshot(Sindex, dirname)
# mapping of snapshot/community index to vertex ID
V = {}
# reverse mapping from vertex ID to snapshot/community index
R = {}
# Mapping of matching data to edge.  Each edge follows the format (u,v;p),
# where each of u and v is a string of snapshot/community index and p is the
# label for the edge and denotes the percentage change in community size.
E = {}
# matches denoting fronts with missing observations
M = {}
# all community births without matches
B = []
# build vertex and edge sets
for si in range(i, len(Sindex)):
    # process all match data
    f = open(os.path.join(dirname, "match-%d.dat" % Sindex[si]), "r")
    for line in f:
        match = line.strip()
        update_edge_set(E, V, R, match, M)
    f.close()
    # Process all community births.  In some cases, it is possible that a
    # new dynamic community is born at snapshot year y and subsequently has
    # no observations at all.  It is hence considered dead from y + 1
    # onwards.  A case is when delta = 0.
    for yi in birth(Sindex[si - 1], dirname):
        if yi not in V:
            B.append(yi)
            update_vertex_set(V, R, yi)
# Process all community births in the latest snapshot.  In some cases, it is
# possible that a new dynamic community is born at snapshot si and
# subsequently has no observations at all.  It is hence considered dead from
# si + 1 onwards.  A case is when delta = 0.
for yi in birth(Sindex[-1], dirname):
    if yi not in V:
        B.append(yi)
        update_vertex_set(V, R, yi)
add_missing_matches(E, M, V, R)
# construct graph
G = DiGraph(E, multiedges=False)
for yi in B:
    if yi not in G:
        G.add_vertex(yi)
# get the required measurement
if mtype == "dycom":
    tracedir = os.path.join(dirname, "trace")
    density_exchange_dycom(G, smin, theta, delta, sindexfname, tracedir, fname)
elif mtype == "node":
    density_exchange_nodes(G, smin, theta, delta, sindexfname, comdir, fname)
elif mtype == "nss":
    density_exchange_communities(G, smin, theta, delta, fname)
