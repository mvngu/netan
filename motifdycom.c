/**************************************************************************
* Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* Find all motifs of a given size in each community.  We only consider motifs
 * of sizes 3 and 4 here.
 */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <ctype.h>
#include <igraph.h>
#include <stdlib.h>
#include <string.h>

/* WARNING:  Global variable.  I know I shouldn't do this.  But I really
 * need a global variable in order to keep track of the motif profile and to
 * record a representative from each isomorphism class.
 */
igraph_vector_t mprofile;

/* function prototypes
 */
igraph_bool_t motif_profile(const igraph_t *graph,
                            igraph_vector_t *vids,
                            int ic,
                            void* extra);
void mprofile_init(igraph_vector_t *V,
                   const igraph_integer_t size);
void read_dycom_snapshots(FILE *f,
                          igraph_vector_ptr_t *C);
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_integer_t size);

/* Update the motif profile as we find each motif.  This function is meant to
 * be used as a callback function.
 *
 * WARNING:  This function uses a global variable.
 */
igraph_bool_t motif_profile(const igraph_t *graph,
                            igraph_vector_t *vids,
                            int ic,  /* isomorphism class */
                            void* extra) {
  igraph_integer_t i;
  igraph_integer_t k;
  igraph_integer_t eid;
  igraph_integer_t u;
  igraph_integer_t v;
  /* record a representative from each isomorphism class */
  /* Record the subgraph induced by the nodes in the representative. */
  if (VECTOR(mprofile)[ic] == 0) {
    printf("%d", ic);
    for (i = 0; i < igraph_vector_size(vids) - 1; i++) {
      for (k = i + 1; k < igraph_vector_size(vids); k++) {
        u = (igraph_integer_t)VECTOR(*vids)[i];
        v = (igraph_integer_t)VECTOR(*vids)[k];
        igraph_get_eid(graph, &eid, u, v, IGRAPH_UNDIRECTED, /*error=*/0);
        if (eid >= 0) {
          printf(",%15ld %15ld", (long int)u, (long int)v);
        }
      }
    }
    printf("\n");
  }
  /* update motif profile */
  VECTOR(mprofile)[ic] += 1L;

  return 0;
}

/* Initialize a vector with the given size.
 *
 * - V -- pointer to an initialized vector.  The result will be stored here.
 *   This is the vector for the motif profile.
 * - size -- the motif size.
 */
void mprofile_init(igraph_vector_t *V,
                   const igraph_integer_t size) {
  igraph_integer_t i;

  if (size == 3) {
    /* 4 isomorphism classes for motifs on 3 nodes */
    for (i = 0; i < 4; i++) {
      igraph_vector_push_back(V, 0L);
    }
  } else if (size == 4) {
    /* 11 isomorphism classes for motifs on 4 nodes */
    for (i = 0; i < 11; i++) {
      igraph_vector_push_back(V, 0L);
    }
  }
}

/* Read in each snapshot of a dynamic community.
 *
 * - f -- a stream from which to read the communities.
 * - C -- an initialized vector of vectors.  The communities for a snapshot
 *   will be stored here.  Each community will be stored as a vector of
 *   integers, where each integer denotes a vertex ID.  The very first element
 *   of the vector of nodes is the ID of the network snapshot in which the
 *   snapshot community occurs.
 */
void read_dycom_snapshots(FILE *f,
                          igraph_vector_ptr_t *C) {
  long int i;      /* generic index */
  long int ncomm;  /* # of step communities */
  long int node;   /* a node belonging to a community */
  int c;
  igraph_vector_t *p;

  /* skip all white spaces */
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);
  /* The very first line of the given file contains only the number of */
  /* step communities.  Read in this number and initialize the community
   * vector C to have that many vectors. */
  fscanf(f, "%li", &ncomm);
  vector_of_vectors_init(C, (igraph_integer_t)ncomm);

  /* skip all white spaces */
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);

  /* Index i is now the community index.  We start from community with */
  /* index 0.  Each newline character read indicates that we are to */
  /* increment the community index by one. */
  i = 0;
  while (!feof(f)) {
    /* Get all the nodes belonging to a community.  The very first integer */
    /* if a line is the ID of the network snapshot in which this community */
    /* occurs. */
    fscanf(f, "%li", &node);
    p = (igraph_vector_t *)VECTOR(*C)[i];
    igraph_vector_push_back(p, (igraph_integer_t)node);
    /* skip all white spaces */
    do {
      c = getc(f);
      /* All the nodes belonging to a community are assumed to be on one */
      /* line.  If we encounter a newline character, then we know that */
      /* we have read all the nodes of a community, including the ID of the */
      /* network snapshot in which the step community occurs. */
      if (c == '\n') {
        i++;
      }
    } while (isspace(c));
    ungetc(c, f);
  }
}

/* Initialize a vector of vectors.  This is just a vector, each of whose
 * elements is a pointer to a vector.
 *
 * - V -- pointer to an initialized vector of vectors.  The result will be
 *   stored here.
 * - size -- the number of elements in V.
 */
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_integer_t size) {
  igraph_integer_t i;
  igraph_vector_t *p;

  for (i = 0; i < size; i++) {
    p = igraph_Calloc(1, igraph_vector_t);
    igraph_vector_init(p, 0);
    igraph_vector_ptr_push_back(V, p);
  }
}

/* Find all motifs of a given size in each dynamic community.  Currently, we
 * only support motifs of sizes 3 and 4.  The algorithm for motif detection
 * is from:
 *
 * S. Wernicke and F. Rasche. FANMOD: a tool for fast network motif
 * detection. Bioinformatics, 22:1152--1153, 2006.
 *
 * WARNING:  This function uses a global variable.
 */
int main(int argc,
         char **argv) {
  FILE *file;
  char *fname;
  int i;
  int j;
  int n;  /* size of motif */
  igraph_t G;                   /* network snapshot */
  igraph_t H;                   /* subgraph of G */
  igraph_integer_t snapshotid;  /* ID of network snapshot */
  igraph_vector_t cp;         /* cut-off probabilities */
  igraph_vector_t *Stepcom;   /* vector of nodes in a step community */
  igraph_vector_ptr_t Dycom;  /* vector of snapshots of a dynamic community */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_str *edgelistdir = arg_strn(NULL, "edgelistdir", "path", 0,
                                    argc + 2,
                                    "directory from which to read edge lists");
  struct arg_str *fdycom = arg_strn(NULL, "fdycom", "path", 0, argc + 2,
           "file containing a dynamic community, one step community per line");
  struct arg_str *size = arg_strn(NULL, "size", "string", 0, argc + 2,
                                  "size of motif; only support 3 and 4");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, edgelistdir, fdycom, size, end};

  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Find all motifs of a given size in a dynamic community.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((edgelistdir->count < 1)
      || (fdycom->count < 1)
      || (size->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Find all motifs of a given size in a dynamic community.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: must use a supported motif size */
  if ((strcmp(size->sval[0], "3") != 0)
      && (strcmp(size->sval[0], "4") != 0)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Find all motifs of a given size in a dynamic community.\n");
    arg_print_glossary(stdout, argtable, "  %-20s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  n = atoi(size->sval[0]);  /* motif size */
  igraph_vector_init_real(&cp, 8, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);

  /* read in each snapshot of a dynamic community */
  igraph_vector_ptr_init(&Dycom, 0);
  IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(&Dycom, igraph_vector_destroy);
  asprintf(&fname, "%s", fdycom->sval[0]);
  file = fopen(fname, "r");
  read_dycom_snapshots(file, &Dycom);
  fclose(file);
  free(fname);

  /* For each step community C, let G be the graph in which C is embedded. */
  /* Let H be the subgraph of G induced by nodes in C.  Find all motifs */
  /* in H. */
  for (i = 0; i < igraph_vector_ptr_size(&Dycom); i++) {
    Stepcom = (igraph_vector_t *)VECTOR(Dycom)[i];
    snapshotid = (igraph_integer_t)VECTOR(*Stepcom)[0];
    /* Remove the ID of the network snapshot.  The remaining elements in */
    /* the vector should be all nodes of a step community. */
    igraph_vector_remove(Stepcom, 0);
    /* get the graph representing the network snapshot */
    /* reconstruct graph from its edge list: simple undirected graph */
    asprintf(&fname, "%s/edgelist-%ld.dat", edgelistdir->sval[0],
             (long int)snapshotid);
    file = fopen(fname, "r");
    igraph_read_graph_edgelist(&G, file, /*nvert*/ 0, IGRAPH_UNDIRECTED);
    fclose(file);
    free(fname);
    igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                    /*edge_comb*/ 0);
    /* H := subgraph induced by nodes in the step community */
    igraph_induced_subgraph(&G, &H, igraph_vss_vector(Stepcom),
                            IGRAPH_SUBGRAPH_AUTO);

    /* profile for motifs of size n */
    igraph_vector_init(&mprofile, 0);
    mprofile_init(&mprofile, n);
    igraph_motifs_randesu_callback(&H, n, &cp, &motif_profile, /*extra=*/0);
    /* print motif profile */
    for (j = 0; j < igraph_vector_size(&mprofile); j++) {
      printf("%20ld ", (long int)VECTOR(mprofile)[j]);
    }
    printf("\n\n");

    igraph_destroy(&G);
    igraph_destroy(&H);
    igraph_vector_destroy(&mprofile);
  }

  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
  igraph_vector_ptr_destroy_all(&Dycom);
  igraph_vector_destroy(&cp);

  return 0;
}
