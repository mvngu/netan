###########################################################################
# Copyright (C) 2011--2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# Count the number of communities at a given snapshot index.

import argparse
import os

###################################
# helper functions
###################################

def count_com(tracedir, maxell, output):
    """
    Count the number of communities at the given snapshot index.

    INPUT:

    - tracedir -- where the trace files are stored.
    - maxell -- the maximum lifespan of any dynamic community.
    - output -- write results to this file.
    """
    Com = set()
    for ell in range(1, maxell + 1):
        fname = os.path.join(tracedir, "trace-%d.dat" % ell)
        f = open(fname, "r")
        # first line is #dycom with given lifespan
        line = f.readline()
        ndycom = int(line.strip())
        if ndycom < 1:
            f.close()
            continue
        # we have at least one dynamic community to consider
        # update the set of community indices
        for line in f:
            parse_trace(line.strip(), Com)
        f.close()
    f = open(output, "w")
    f.write("%d\n" % len(Com))
    f.close()

def parse_trace(trace, Com):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline of a dynamic
    community.  Each step community is represented as

        si com

    where "si" refers to the snapshot index and "com" refers to the index
    of the community in the given snapshot.  We are only interested in the
    snapshot indices.

    WARNING:  This function modifies its arguments.

    INPUT:

    - trace -- the trace of a dynamic community.
    - Com -- this should contain all the step communities so far.  We update
      this as we go along.
    """
    T = trace.split("->")
    # transform the pattern "si1 com1,n" into "si1 com1"
    for i in range(len(T)):
        if "," in T[i]:
            sicom, _ = T[i].split(",")
            T[i] = sicom
    # update the set of community indices
    for sicom in T:
        if sicom not in Com:
            Com.add(sicom)

###################################
# script starts here
###################################

# setup parser for command line options
s = "Count the number of communities.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--tracedir", metavar="path", required=True,
                    help="directory with trace files")
parser.add_argument("--maxell", metavar="integer", required=True, type=int,
                    help="the maximum lifespan")
parser.add_argument("--output", metavar="file", required=True,
                    help="write results to this file")
args = parser.parse_args()

# get command line arguments & sanity checks
tracedir = args.tracedir
maxell = args.maxell
output = args.output

count_com(tracedir, maxell, output)
