###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile
#
# A lifespan distribution that follows an exponential-law.
# Count the number of parameter settings that result in a statistically
# significant distribution of lifespan.  By statistically significant, we
# mean that when fitting the empirical distribution to an exponential-law,
# we find that a linear regression on a linear-log scale resulted in a
# coefficient of determination r^2 >= 0.9 and p-value < 0.05.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
fname <- args[1]  # file having a table of parameter settings,
                  # r-squared values, and p-values

T <- read.csv(file=fname, header=TRUE, sep=",")
colnames(T) <- c("smin", "theta", "delta", "rsquared", "pvalue")
proportion <- nrow(T[T$pvalue == 1 & T$rsquared >= 0.9,]) / nrow(T)
write(proportion, stdout())
