###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script MUST be run from Sage.
#
# Various measures of entropy of dynamic communities.

# third party library
from sage.all_cmdline import *
# standard library
# Use optparse because Sage 4.8 has Python 2.6.  From Python 2.7 onwards,
# we could use argparse.
import optparse
import os

###################################
# helper functions
###################################

def entropy(S):
    """
    The entropy of a dynamic community given as a timeline.

    INPUT:

    - Match -- a dictionary of all match data.
    - S -- a sequence of similarity scores.
    """
    H = map(lambda x: x * log(x), S)  # we want natural logarithm
    H = -1 * sum(H)
    if H == 0.0:
        H = 0.0
    H = H / len(S)
    return H

def first_match_year(Year, dirname):
    """
    The first snapshot year having match data.  Depending on the minimum
    required size of each community, a snapshot year might be neglected
    because each of the communities in that year has size less than the
    required minimum.  We only return the index of that year from within the
    given list of years.

    INPUT:

    - Year -- list of all the snapshot years, sorted in increasing order.
    - dirname -- name of the directory from which to read file storing
      match data.
    """
    i = 0
    fname = os.path.join(dirname, "match-%d.dat" % Year[i])
    while not os.path.isfile(fname):
        i += 1
        if i >= len(Year):
            raise ValueError("Expected match data, but received none")
        fname = os.path.join(dirname, "match-%d.dat" % Year[i])
    return i

def full_match_dict(yearfname, matchdir):
    """
    Return a dictionary of all match data.  This is a dictionary of
    dictionaries.  Each key is a snapshot year and each value corresponding to
    the key is a dictionary of match data for that year.  We use the
    following representation:

        Match := {1951: {...}, ..., 2011: {...}}

    The key/value pair is represented as:

        2011: {"2010 i 2011 j": S("2010 i", "2011 j"), ...}

    The part "2010 i" refers to the community in snapshot 2010 with index i.
    Similarly, the part "2011 j" means the community in snapshot 2011 with
    index j.  The value S("2010 i", "2011 j") is the similarity measure between
    the communities "2010 i" and "2011 j".

    INPUT:

    - yearfname -- file containing all snapshot years, one per line.
    - matchdir -- directory containing the match data.
    """
    Year = get_years(yearfname)
    i = first_match_year(Year, matchdir)
    Match = {}
    for y in Year[i:]:
        D = match_dict(y, matchdir)
        Match.setdefault(y, D)
    return Match

# this function is stolen from dycom
def get_years(fname):
    """
    Get all the snapshot years from the given file.  The resulting list of
    years will be sorted in nondecreasing order.

    INPUT:

    - fname -- file with all the snapshot years, one per line.

    OUTPUT:

    List of all snapshot years, sorted in nondecreasing order.
    """
    Year = []
    f = open(fname, "r")
    for line in f:
        Year.append(int(line.strip()))
    f.close()
    return sorted(Year)

def match_dict(year, matchdir):
    """
    The match dictionary for the given year.  The key/value pair of the
    dictionary follows the format:

        2011: {"2010 i 2011 j": S("2010 i", "2011 j"), ...}

    The part "2010 i" refers to the community in snapshot 2010 with index i.
    Similarly, the part "2011 j" means the community in snapshot 2011 with
    index j.  The value S("2010 i", "2011 j") is the similarity measure between
    the communities "2010 i" and "2011 j".  The file containing match data is
    assumed to have each line in the following format:

        year1 i year2 j sim pchange

    where "year1 i" refers to the community in year year1 with index i,
    "year2 j" denotes the community in year year2 with index j, "sim" is the
    similarity score between the given two communities, and "pchange" is the
    percentage change in community size.

    INPUT:

    - year -- the snapshot year.
    - matchdir -- directory containing the match data.
    """
    f = None
    fname = os.path.join(matchdir, "match-%d.dat" % year)
    try:
        f = open(fname, "r")
    except IOError:
        raise IOError("File does not exist: %s" % fname)
    D = {}
    for line in f:
        match = line.strip()
        year1, com1, year2, com2, sim, _ = year_index_pchange(match)
        key = "%d %d %d %d" % (year1, com1, year2, com2)
        if key in D:
            raise ValueError("Unexpected duplicate key")
        D.setdefault(key, sim)
    f.close()
    return D

def max_lifespan(yearfname, tracedir):
    """
    The maximum lifespan for the given parameter settings.  This upper value
    is usually distinct from the global maximum lifespan for the entirety of
    the dataset.

    INPUT:

    - yearfname -- file containing all snapshot years, one per line.
    - tracedir -- path to directory with timeline data for each dynamic
      community.
    """
    Year = get_years(yearfname)
    m = len(Year)  # global maximum lifespan for the dataset
    fname = os.path.join(tracedir, "trace-%d.dat" % m)
    while not os.path.isfile(fname):
        m -= 1
        if m <= 0:
            raise ValueError("Expected trace data, but received none")
        fname = os.path.join(tracedir, "trace-%d.dat" % m)
    return m

def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline a dynamic
    community.  Each step community is represented as

        year com

    where "year" refers to the snapshot year and "com" refers to the index
    of the community in the given year.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    return trace.split("->")

def similarity_sequence(Match, trace):
    """
    The sequence of similarity scores for a dynamic community.  The
    sequence is arranged in temporal order from the birth of the dynamic
    community to its death.

    INPUT:

    - Match -- a dictionary of all match data.
    - trace -- the trace of a dynamic community.
    """
    Trace = parse_trace(trace)
    S = []
    for i in range(len(Trace) - 1):
        A = Trace[i]
        B = Trace[i + 1]
        yearA, indA = A.split()
        yearB, indB = B.split()
        if "," in indB:
            # missing observation, so similarity score = 1.0
            assert(yearA == yearB)
            S.append(float(1.0))
            continue
        if "," in indA:
            indA = indA.split(",")[0]
        key = "%s %s %s %s" % (yearA, indA, yearB, indB)
        Dict = Match[int(yearB)]
        sim = Dict[key]
        S.append(float(sim))
    return S

def write_entropy(yearfname, Match, tracedir, outdir):
    """
    Write to file the entropy of each dynamic community.

    INPUT:

    - yearfname -- file containing all snapshot years, one per line.
    - Match -- a dictionary of all match data.
    - tracedir -- path to directory with timeline data for each dynamic
      community.
    - outdir -- path to directory under which to write results.
    """
    m = max_lifespan(yearfname, tracedir)
    if m < 2:
        raise ValueError("Maximum lifespan must be >= 2")
    for i in range(2, m + 1):
        fname = os.path.join(tracedir, "trace-%d.dat" % i)
        f = open(fname, "r")
        f.readline()  # ignore first line, which counts # dynamic communities
        St = ""       # should be in CSV format
        for line in f:
            S = similarity_sequence(Match, line.strip())
            H = entropy(S)
            St += "%.10f;" % H
            St += ",".join(map(str, S)) + "\n"
        f.close()
        fname = os.path.join(outdir, "entropy-%d.dat" % i)
        f = open(fname, "w")
        f.write(St)
        f.close()

# this function is stolen from dycom
def year_index_pchange(match):
    """
    Extract the years, community indices, similarity score, and percentage
    change from the given match.  The match follows this format:

        year1 comm_index1 year2 comm_index2 sim pchange

    The key "year1" is the snapshot year for a front community and
    "comm_index1" is a community index.  Similarly, "year2" is the snapshot
    year for a step community and "comm_index2" is a community index.  The
    key "sim" is the similarity score between the two communities.  The key
    "pchange" is the percentage change in community size.

    INPUT:

    - match -- a community match between a front and a step.
    """
    M = match.split()
    s = int(M[0])   # year of front community
    Cs = int(M[1])  # index of front community
    t = int(M[2])   # year of step community
    Ct = int(M[3])  # index of step community
    sim = M[4]      # similarity score between two communities
    p = M[5]        # percentage change in community size
    return (s, Cs, t, Ct, sim, p)

###################################
# script starts here
###################################

# setup parser for command line options
s = "Various measures of entropy of dynamic communities.\n"
s += "Usage: %prog arg1 arg2 ..."
parser = optparse.OptionParser(usage=s)
parser.add_option("--year", metavar="file",
                  help="file containing all snapshot years, one per line")
parser.add_option("--matchdir", metavar="path",
                  help="path to directory with match data")
parser.add_option("--tracedir", metavar="path",
                  help="path to directory with timeline data")
parser.add_option("--outdir", metavar="path",
                  help="path to directory under which to output results")
options, _ = parser.parse_args()

# get command line arguments & sanity checks
if ((options.year is None) or (options.matchdir is None)
    or (options.tracedir is None) or (options.outdir is None)):
    raise optparse.OptionValueError(
        "All options must be used. Use -h for help.")
yearfname = options.year
matchdir = options.matchdir
tracedir = options.tracedir
outdir = options.outdir

Match = full_match_dict(yearfname, matchdir)
write_entropy(yearfname, Match, tracedir, outdir)
