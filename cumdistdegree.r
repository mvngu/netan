###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile output
#
# The cumulative distribution of degree.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
infile <- args[1]   # file with a sequence of degrees
outfile <- args[2]  # write the cumulative distribution to this file

npoint <- 1000
Dseq <- read.csv(file=infile, header=TRUE, sep=",")
colnames(Dseq) <- c("degree")

# no dynamic communities at all
if (length(Dseq$degree) == 0) {
  outcon <- file(outfile, open="w")
  writeLines("degree,cumulative frequency", outcon)
  quit()
}

# we have at least one dynamic community
delta <- max(Dseq$degree) / npoint
D <- c()
for (i in 0:npoint) {
  Deg <- i*delta
  Pr <- length(Dseq[Dseq$degree >= i*delta,]) / length(Dseq$degree)
  D <- rbind(D, c(Deg, Pr))
}
D <- data.frame(D)
colnames(D) <- c("degree", "cumulative frequency")
write.table(D, file=outfile, quote=FALSE, sep=",", row.names=FALSE)
