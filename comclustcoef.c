/**************************************************************************
 * Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

/* Evolution of clustering coefficients in communities. */

#define _GNU_SOURCE  /* asprintf */

#include <argtable2.h>
#include <assert.h>
#include <ctype.h>
#include <igraph.h>
#include <stdio.h>

void clustering_coefficients(const igraph_t *G,
                             const igraph_vector_ptr_t *C,
                             const igraph_integer_t minsize,
                             igraph_vector_t *Gclust,
                             igraph_vector_t *Avglclust);
void read_communities(FILE *f,
                      igraph_vector_ptr_t *C);
void snapshot_index(const char *infile,
                    igraph_vector_t *sindex);
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_integer_t size);
void write_header(const char *fname, const int islocal);
void write_results(const char *fglobal,
                   const char *flocal,
                   const igraph_integer_t sindex,
                   const igraph_vector_t *Gclust,
                   const igraph_vector_t *Avglclust);

/* The clustering coefficients of each community.  We consider two types of
 * clustering coefficients.
 *
 * - The global clustering coefficient of each community.
 * - The average local clustering coefficient of each community.
 *
 * - G -- a network snapshot.
 * - C -- collection of all communities in G.
 * - minsize -- the minimum community size.  We only consider all communities
 *   with this minimum size.
 * - Gclust -- global clustering coefficient of each community.  Store results
 *   here.  This vector should be initialized beforehand.  It will be resized
 *   as required.
 * - Avglclust -- average local clustering coefficient of each community.
 *   Store results here.  This vector should be initialized beforehand.  It
 *   will be resized as required.
 */
void clustering_coefficients(const igraph_t *G,
                             const igraph_vector_ptr_t *C,
                             const igraph_integer_t minsize,
                             igraph_vector_t *Gclust,
                             igraph_vector_t *Avglclust) {
  igraph_t H;            /* subgraph induced by a bunch of nodes */
  igraph_integer_t i;
  igraph_real_t gclust;  /* global clustering coefficient */
  igraph_real_t lclust;  /* average local clustering coefficient */
  igraph_vector_t *V;    /* nodes in a community */

  for (i = 0; i < igraph_vector_ptr_size(C); i++) {
    V = (igraph_vector_t *)VECTOR(*C)[i];
    if (igraph_vector_size(V) < minsize) {
      continue;
    }
    igraph_induced_subgraph(G, &H, igraph_vss_vector(V), IGRAPH_SUBGRAPH_AUTO);
    /* Average local clustering coefficient following */
    /* (Watts & Strogatz 1998).  For a vertex with degree < 2, we set its */
    /* average local clustering coefficient to zero. */
    igraph_transitivity_avglocal_undirected(&H, &lclust,
                                            IGRAPH_TRANSITIVITY_ZERO);
    igraph_vector_push_back(Avglclust, lclust);
    /* The global clustering coefficient as defined in */
    /* (Wasserman & Faust 1994).  If a graph has no connected triples */
    /* (i.e. triangles), we set its global clustering coefficient to zero. */
    igraph_transitivity_undirected(&H, &gclust, IGRAPH_TRANSITIVITY_ZERO);
    igraph_vector_push_back(Gclust, gclust);

    igraph_destroy(&H);
  }
}

/* Read in the collection of communities for a network snapshot.
 *
 * - f -- a stream from which to read the communities.
 * - C -- an initialized vector of vectors.  The communities for a snapshot
 *   will be stored here.  Each community will be stored as a vector of
 *   integers, where each integer denotes a vertex ID.
 */
void read_communities(FILE *f,
                      igraph_vector_ptr_t *C) {
  long int i;     /* generic index */
  long int ncom;  /* # communities */
  long int node;  /* a node belonging to a community */
  int c;
  igraph_vector_t *p;

  /* skip all white spaces */
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);
  /* The very first line of the given file contains only the number of */
  /* communities.  Read in this number and initialize the community vector */
  /* C to have that many vectors. */
  fscanf(f, "%li", &ncom);
  vector_of_vectors_init(C, (igraph_integer_t)ncom);

  /* skip all white spaces */
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);

  /* Index i is now the community index.  We start from community with */
  /* index 0.  Each newline character read indicates that we are to */
  /* increment the community index by one. */
  i = 0;
  while (!feof(f)) {
    /* get all the nodes belonging to a community */
    fscanf(f, "%li", &node);
    p = (igraph_vector_t *)VECTOR(*C)[i];
    igraph_vector_push_back(p, (igraph_integer_t)node);
    /* skip all white spaces */
    do {
      c = getc(f);
      /* All the nodes belonging to a community are assumed to be on one */
      /* line.  If we encounter a newline character, then we know that */
      /* we have read all the nodes of a community. */
      if (c == '\n') {
        i++;
      }
    } while (isspace(c));
    ungetc(c, f);
  }
}

/* Get all the snapshot indices from the given file.  The file is assumed to
 * contain snapshot indices, one per line.  All the snapshot indices are
 * assumed to be in temporal order, from the birth of the network to its
 * last observation.
 *
 * - infile -- file containing all the snapshot years.
 * - sindex -- vector of snapshot indices.  The result will be stored here.
 *   This should be initialized beforehand.
 */
void snapshot_index(const char *infile,
                    igraph_vector_t *sindex) {
  FILE *f;
  long int si;  /* a snapshot index */
  int c;        /* a character */

  f = fopen(infile, "r");

  /* skip all white spaces */
  do {
    c = getc(f);
  } while (isspace(c));
  ungetc(c, f);

  /* read all snapshot indices, one per line */
  while (!feof(f)) {
    fscanf(f, "%li", &si);
    igraph_vector_push_back(sindex, (igraph_integer_t)si);
    /* skip all white spaces */
    do {
      c = getc(f);
    } while (isspace(c));
    ungetc(c, f);
  }

  fclose(f);
}

/* Initialize a vector of vectors.  This is just a vector, each of whose
 * elements is a pointer to a vector.
 *
 * - V -- pointer to an initialized vector of vectors.  The result will be
 *   stored here.
 * - size -- the number of elements in V.
 */
void vector_of_vectors_init(igraph_vector_ptr_t *V,
                            const igraph_integer_t size) {
  igraph_integer_t i;
  igraph_vector_t *p;

  for (i = 0; i < size; i++) {
    p = igraph_Calloc(1, igraph_vector_t);
    igraph_vector_init(p, 0);
    igraph_vector_ptr_push_back(V, p);
  }
}

/* Write header for the results file.
 *
 * - fname -- the name of the file to which we write the header.
 * - islocal -- boolean; whether we are considering the local or global
 *   clustering coefficient.
 */
void write_header(const char *fname,
                  const int islocal) {
  FILE *file;

  file = fopen(fname, "w");
  if (islocal) {
    fprintf(file, "snapshot index,avg local clust coefs\n");
  } else {
    fprintf(file, "snapshot index,global clust coefs\n");
  }
  fclose(file);
}

/* Write results to a file.  Results are written according to the following
 * CSV file format:
 *
 * snapshot index,list of (local/global) clustering coefficients
 *
 * - fglobal -- file to which we write global clustering coefficients of all
 *   communities.
 * - flocal -- file to which we write average local clustering coefficients
 *   of all communities.
 * - sindex -- the snapshot index.
 * - Gclust -- vector with global clustering coefficient for each community.
 * - Avglclust -- vector with average local clustering coefficient for each
 *   community.
 */
void write_results(const char *fglobal,
                   const char *flocal,
                   const igraph_integer_t sindex,
                   const igraph_vector_t *Gclust,
                   const igraph_vector_t *Avglclust) {
  FILE *fileglobal;
  FILE *filelocal;
  igraph_integer_t i;

  fileglobal = fopen(fglobal, "a");
  filelocal = fopen(flocal, "a");
  fprintf(fileglobal, "%li", (long int)sindex);
  fprintf(filelocal, "%li", (long int)sindex);
  assert(igraph_vector_size(Gclust) == igraph_vector_size(Avglclust));
  for (i = 0; i < igraph_vector_size(Gclust); i++) {
    fprintf(fileglobal, ",%.10lf", (double)VECTOR(*Gclust)[i]);
    fprintf(filelocal, ",%.10lf", (double)VECTOR(*Avglclust)[i]);
  }
  fprintf(fileglobal, "\n");
  fprintf(filelocal, "\n");
  fclose(fileglobal);
  fclose(filelocal);
}

/* Evolution of clustering coefficients in communities.  We consider two types
 * of evolution:
 *
 * - For each network snapshot G, compute the global clustering coefficient of
 *   each community in G.
 * - Let C be a collection of communities in a network snapshot G.  Compute
 *   the average local clustering coefficient of each community.
 */
int main(int argc,
         char **argv) {
  FILE *infile;                  /* file to read from */
  char *fname;
  char *fgclust;                 /* file to write global clust. coeff. */
  char *favglclust;              /* file to write avg local clust. coeff. */
  igraph_vector_t sindex;        /* all the snapshot indices */
  igraph_t G;                    /* a graph */
  igraph_integer_t i;            /* generic index */
  igraph_integer_t min_comsize;  /* minimum #nodes for each community */
  igraph_vector_t Gclust;        /* global clustering coefficients */
  igraph_vector_t Avglclust;     /* avg local clustering coefficients */
  igraph_vector_ptr_t C;         /* vector of communities */

  /* setup the table of command line options */
  struct arg_lit *help = arg_lit0(NULL, "help", "print this help and exit");
  struct arg_str *minsize = arg_strn(NULL, "minsize", "integer", 0,
                                     argc + 2, "minimum community size");
  struct arg_file *fsindex = arg_file0(NULL, "file", NULL,
                         "file containing all snapshot indices, one per line");
  struct arg_str *elistdir = arg_strn(NULL, "elistdir", "path", 0, argc + 2,
                                    "directory from which to read edge lists");
  struct arg_str *comdir = arg_strn(NULL, "comdir", "path", 0, argc + 2,
                               "directory with files of community structures");
  struct arg_str *statdir = arg_strn(NULL, "statdir", "path", 0, argc + 2,
                                     "directory to which we write statistics");
  struct arg_end *end = arg_end(20);
  void *argtable[] = {help, minsize, fsindex, elistdir, comdir, statdir, end};

  /* parse the command line as defined by argtable[] */
  arg_parse(argc, argv, argtable);

  /* print usage information when --help is passed in */
  if (help->count > 0) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Evolution of clustering coefficients in communities.\n");
    arg_print_glossary(stdout, argtable, "  %-25s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }
  /* special case: all command line arguments, except --help, must be used */
  if ((minsize->count < 1)
      || (fsindex->count < 1)
      || (elistdir->count < 1)
      || (comdir->count < 1)
      || (statdir->count < 1)) {
    printf("Usage: %s", argv[0]);
    arg_print_syntax(stdout, argtable, "\n");
    printf("Evolution of clustering coefficients in communities.\n");
    arg_print_glossary(stdout, argtable, "  %-25s %s\n");
    arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
    exit(EXIT_FAILURE);
  }

  min_comsize = (igraph_integer_t)atoi(minsize->sval[0]);
  asprintf(&fgclust, "%s/global/clustcoef-global-smin-%ld.csv",
           statdir->sval[0], (long int)min_comsize);
  asprintf(&favglclust, "%s/local/clustcoef-avglocal-smin-%ld.csv",
           statdir->sval[0], (long int)min_comsize);
  write_header(fgclust, /*islocal=*/ 0);
  write_header(favglclust, /*islocal=*/ 1);
  igraph_vector_init(&sindex, 0);
  snapshot_index(fsindex->filename[0], &sindex);

  /* iterate over each snapshot & get local/global clustering coefficients */
  for (i = 0; i < igraph_vector_size(&sindex); i++) {
    /* Construct the snapshot graph from its edge list.  The graph is */
    /* a simple undirected graph, i.e. no self-loops nor multiple edges. */
    asprintf(&fname, "%s/edgelist-%li.dat", elistdir->sval[0],
             (long int)VECTOR(sindex)[i]);
    infile = fopen(fname, "r");
    igraph_read_graph_edgelist(&G, infile, 0, IGRAPH_UNDIRECTED);
    fclose(infile);
    free(fname);
    igraph_simplify(&G, /*no multiple edges*/ 1, /*no self-loops*/ 1,
                    /*edge_comb*/ 0);

    /* read in all the communities of a network snapshot */
    igraph_vector_ptr_init(&C, 0);
    IGRAPH_VECTOR_PTR_SET_ITEM_DESTRUCTOR(&C, igraph_vector_destroy);
    asprintf(&fname, "%s/comm-%li.dat", comdir->sval[0],
             (long int)VECTOR(sindex)[i]);
    infile = fopen(fname, "r");
    read_communities(infile, &C);
    fclose(infile);
    free(fname);

    igraph_vector_init(&Gclust, 0);
    igraph_vector_init(&Avglclust, 0);
    clustering_coefficients(&G, &C, min_comsize, &Gclust, &Avglclust);
    write_results(fgclust, favglclust, (igraph_integer_t)VECTOR(sindex)[i],
                  &Gclust, &Avglclust);

    /* clean up */
    igraph_destroy(&G);
    igraph_vector_ptr_destroy_all(&C);
    igraph_vector_destroy(&Gclust);
    igraph_vector_destroy(&Avglclust);
  }

  arg_freetable(argtable, sizeof(argtable) / sizeof(argtable[0]));
  free(fgclust);
  free(favglclust);
  igraph_vector_destroy(&sindex);

  return 0;
}
