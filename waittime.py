###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# NOTE: This script should be run from Python.
#
# The waiting time between successive occurrence of an event.

import argparse
import os

###################################
# helper functions
###################################

def extract_wait_time(fsindex, eventdir, outdir, etype):
    """
    Get the time between successive occurrences of an event.

    INPUT:

    - fsindex -- a file listing all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    - eventdir -- directory where event data are stored.  Data for each event
      are stored under a separate directory.
    - outdir -- directory where we will write waiting time.
    - etype -- the type of event.
    """
    # sanity check
    # WARNING:  We assume that data for the following events are represented
    # in a common format.
    Event = ("contract", "expand", "merge", "split")
    if etype not in Event:
        raise ValueError("Unsupported event: %s" % etype)
    # get waiting time
    Sindex = read_sindex(fsindex)
    D = read_event(Sindex, eventdir, etype)
    # for year, event in D.iteritems():
    #     print year, event
    Waittime = []
    for ell in range(2, len(Sindex) + 1):
        T = read_trace(eventdir, ell)
        for trace in T.itervalues():
            t = 1
            for tail, head in trace:
                si, com = head.split(" ")
                if (tail, head) in D[int(si)]:
                    Waittime.append(t)
                    t = 1
                    continue
                t += 1
    fname = os.path.join(outdir, "%s.dat" % etype)
    f = open(fname, "w")
    for t in sorted(Waittime):
        f.write("%d\n" % t)
    f.close()

def parse_event(line):
    """
    Parse the event as represented by the given line.  The event is assumed
    to be represented in the format:

        sindex1 com1<-sindex2 com2,sindex3 com3,...

    After the event is parsed, we represent the event in the format:

        (sindex2 com2, sindex1 com1), (sindex3 com3, sindex1 com1), ...

    The token "sindex1 com1" is called the head.  The other tokens are tails.
    """
    Event = []
    head, tails = line.split("<-")
    for t in tails.split(","):
        Event.append((t, head))
    return Event

def parse_trace(trace):
    """
    Parse the trace of a dynamic community.  The trace records in temporal
    order all step communities that constitute the timeline of a dynamic
    community.  Each step community is represented as

        year com

    where "year" refers to the snapshot year and "com" refers to the index
    of the community in the given year.

    INPUT:

    - trace -- the trace of a dynamic community.
    """
    T = trace.split("->")
    i = 1
    while i < len(T):
        # collapse the pattern "year1 com1->year1 com1,n" into "year1 com1"
        if "," in T[i]:
            del T[i]
            continue
        i += 1
    return T

def read_event(Sindex, eventdir, etype):
    """
    Read in the event data for a given type of event.

    INPUT:

    - Sindex -- a list of all the snapshot indices.
    - eventdir -- directory where event data are stored.
    - etype -- the type of event.
    """
    Event = ("contract", "expand", "merge", "split")
    if etype not in Event:
        raise ValueError("Unsupported event: %s" % etype)
    D = dict()
    for i in Sindex:
        # length(D[i]) == 0 means that snapshot i has no occurrence of the
        # event
        D.setdefault(i, dict())
        fname = os.path.join(eventdir, etype, "%s-%d.dat" % (etype, i))
        f = open(fname, "r")
        nevent = int(f.readline().strip())
        if nevent > 0:
            for line in f:
                for tail, head in parse_event(line.strip()):
                    assert (tail, head) not in D[i]
                    D[i].setdefault((tail, head), True)
        f.close()
    return D

def read_sindex(fname):
    """
    Get all the snapshot indices from the given file.

    INPUT:

    - fname -- file with all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    """
    Sindex = []
    f = open(fname, "r")
    for line in f:
        Sindex.append(int(line.strip()))
    f.close()
    return Sindex

def read_trace(eventdir, ell):
    """
    Read in the trace of all dynamic communities with the given lifespan.
    The trace of a dynamic community is represented as:

        y1 n1->y2 n2->y3 n3-> ... ->yk nk

    After reading in the trace, we represent it as:

        (y1 n1, y2 n2), (y2 n2, y3 n3), ..., (yk-1 nk-1, yk nk)

    INPUT:

    - eventdir -- directory where event data are stored.  Data for each event
      are stored under a separate directory.
    - ell -- the lifespan of a dynamic community.  Assumed to be > 1.
    """
    if ell < 2:
        raise ValueError("Lifespan must be > 1")
    T = dict()
    fname = os.path.join(eventdir, "trace", "trace-%d.dat" % ell)
    f = open(fname, "r")
    ncom = int(f.readline().strip())  # how many dynamic communities
    if ncom > 0:
        for line in f:
            n = len(T)
            T.setdefault(n, list())
            trace = parse_trace(line.strip())
            for i in range(len(trace) - 1):
                T[n].append((trace[i], trace[i + 1]))
    f.close()
    return T

def wait_time(fsindex, eventdir, outdir):
    """
    The waiting time between successive occurrence of an event.

    INPUT:

    - fsindex -- a file listing all the snapshot indices, one per line.  The
      indices are assumed to be in temporal order.
    - eventdir -- directory where event data are stored.  Data for each event
      are stored under a separate directory.
    - outdir -- directory where we will write waiting time.  The waiting time
      for each event will be written to a separate file.
    """
    extract_wait_time(fsindex, eventdir, outdir, "contract")
    extract_wait_time(fsindex, eventdir, outdir, "expand")
    extract_wait_time(fsindex, eventdir, outdir, "merge")
    extract_wait_time(fsindex, eventdir, outdir, "split")

###################################
# script starts here
###################################

# setup parser for command line options
s = "Waiting time between occurrence of an event.\n"
parser = argparse.ArgumentParser(description=s)
parser.add_argument("--fsindex", metavar="file", required=True,
                    help="listing of all snapshot indices")
parser.add_argument("--eventdir", metavar="path", required=True,
                    help="directory where event data are stored")
parser.add_argument("--outdir", metavar="path", required=True,
                    help="directory where we will write waiting time")
args = parser.parse_args()
# get command line arguments & sanity checks
fsindex = args.fsindex
eventdir = args.eventdir
outdir = args.outdir

if not os.path.isdir(outdir):
    os.makedirs(outdir)
wait_time(fsindex, eventdir, outdir)
