###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> predir h changetype minsi maxsi
#
# Evaluate the accuracy and effectiveness of our prediction method.  We
# evaluate as follows:
#
# * The relative accuracy is the number of predictions that turn out to be
#   true divided by the number of predictions.
# * The absolute accuracy is the number of predictions that turn out to be
#   true divided by the number of dynamic communities that actually
#   experienced the change in question.
# * The ratio of prediction-actual is the number of predictions divided by the
#   number of dynamic communities that actually experienced the change under
#   consideration.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
predir <- args[1]      # path to where results of our predictions are stored
h <- args[2]           # threshold for H value
changetype <- args[3]  # the type of change we consider
minsi <- args[4]       # the minimum snapshot index
maxsi <- args[5]       # the maximum snapshot index

Eval <- c()
for (si in minsi:maxsi) {
  fname <- paste(predir,"/","h-",h,"/",changetype,"-A-",si,".dat", sep="")
  D <- read.csv(file=fname, header=TRUE, sep=",")
  D <- D[1, c(2,3,4)]
  colnames(D) <- c("actual", "prediction", "positive")
  # relative accuracy
  rc <- 0
  if (D$prediction > 0) {
    rc <- D$positive / D$prediction
  }
  # absolute accuracy
  ac <- 0
  if (D$actual > 0) {
    ac <- D$positive / D$actual
  }
  # ratio of prediction-actual
  pa <- 0
  if (D$actual > 0) {
    pa <- D$prediction / D$actual
  }
  Eval <- rbind(Eval, c(si, rc, ac, pa))
}
colnames(Eval) <- c("snapshot", "RelAc", "AbsAc", "PredAct")
fname <- paste(predir,"/",changetype,"-h-",h,".dat", sep="")
write.table(Eval, file=fname, quote=FALSE, sep=",", row.names=FALSE)
