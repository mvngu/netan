###########################################################################
# Copyright (C) 2012 Minh Van Nguyen <mvngu.name@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# http://www.gnu.org/licenses/
###########################################################################

# Usage: Rscript <script-name> infile keepzero prefout
#
# Test for the presence of an exponential law.

# get command line arguments
args <- commandArgs(trailingOnly=TRUE)
fname <- args[1]                 # CSV file with a table of (quantity, count)
keepzero <- as.numeric(args[2])  # boolean; 1 := true, 0 := false
                                 # whether we want to keep count of zeros
prefout <- args[3]               # prefix of files to which we write results

D <- read.csv(file=fname, header=TRUE, sep=",")
colnames(D) <- c("quantity", "count")
if (keepzero == 0) {
  D <- D[D$count > 0,]
}
# only two lines of raw data; assume not exponential
if (length(D$count) < 3) {
  # write results to files
  fname <- paste(prefout,".rs", sep="")  # rs := r-squared
  outcon <- file(fname, open="w")
  write("0", outcon)
  close(outcon)
  fname <- paste(prefout,".pvt", sep="")  # pvt := p-value test
  outcon <- file(fname, open="w")
  write("0", outcon)
  close(outcon)
  quit()
}

# at least two lines of raw data
D <- cbind(D, D$count / sum(D$count))
colnames(D) <- c("quantity", "count", "proportion")
D <- cbind(D, log(D$proportion))
colnames(D) <- c("quantity", "count", "proportion", "logprop")
f <- lm(D$logprop ~ D$quantity)
rsquared <- summary(f)$r.squared
pval <- anova(f)$"Pr(>F)"[1]
lt <- 1  # by default, assume that p-value < 0.05
if (pval >= 0.05) {
  lt <- 0
}
# write results to files
fname <- paste(prefout,".rs", sep="")  # rs := r-squared
outcon <- file(fname, open="w")
write(rsquared, outcon)
close(outcon)
fname <- paste(prefout,".pvt", sep="")  # pvt := p-value test
outcon <- file(fname, open="w")
write(lt, outcon)
close(outcon)
